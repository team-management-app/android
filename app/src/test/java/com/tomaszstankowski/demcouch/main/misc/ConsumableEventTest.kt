package com.tomaszstankowski.demcouch.main.misc

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class ConsumableEventTest {

    @Test
    fun `consume whenConsumedForTheFirstTime returnDataAndSetIsConsumedFlagToTrue`() {
        val event = ConsumableEvent("test")

        val result = event.consume()

        assertEquals("test", result)
        assertTrue(event.isConsumed)
    }

    @Test(expected = IllegalStateException::class)
    fun `consume whenEventIsAlreadyConsumed throwException`() {
        val event = ConsumableEvent(3)

        event.consume()
        event.consume()
    }
}