package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import org.junit.Test

class DescriptionValidatorTest {
    private val descriptionValidator = DescriptionValidator()

    @Test
    fun `validate whenDescriptionTooLong emitTooLongError`() {
        val tooLongDesc = "a".repeat(DescriptionValidator.MAX_LENGTH + 1)

        descriptionValidator.validate(tooLongDesc)
                .test()
                .assertResult(FieldStatus.error(
                        ValidationError.MAX_LENGTH, DescriptionValidator.MAX_LENGTH))
    }

    @Test
    fun `validate whenDescriptionHasMaxLength doNotEmitError`() {
        val maxLengthDesc = "a".repeat(DescriptionValidator.MAX_LENGTH)

        descriptionValidator.validate(maxLengthDesc)
                .test()
                .assertResult(FieldStatus.correct())
    }

    @Test
    fun `validate whenDescriptionEmpty doNotEmitError`() {
        descriptionValidator.validate("")
                .test()
                .assertResult(FieldStatus.correct())
    }
}