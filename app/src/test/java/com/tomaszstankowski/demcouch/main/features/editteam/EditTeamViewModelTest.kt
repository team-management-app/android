package com.tomaszstankowski.demcouch.main.features.editteam

import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.misc.LocationService
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.team.TeamRepository
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.DescriptionValidator
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.TeamNameValidator
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit

class EditTeamViewModelTest {
    private val teamRepository: TeamRepository = mockk()
    private val teamNameValidator: TeamNameValidator = mockk()
    private val descriptionValidator: DescriptionValidator = mockk()
    private val locationService: LocationService = mockk()
    private lateinit var viewModel: EditTeamViewModel
    private val initialTeam = Team(
            1,
            "KS Tęcza",
            "Klub Sportowy",
            Location("Warszawa, Polska", 0.0, 0.0),
            Discipline.FOOTBALL,
            null)

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Before
    fun before() {
        viewModel = EditTeamViewModel(
                teamRepository,
                teamNameValidator,
                descriptionValidator,
                locationService)
        every { teamRepository.getTeam(1) } returns
                Flowable.just(Resource(FetchStatus.SUCCESS, initialTeam))
        viewModel.getTeam(1).test()
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
    }

    @Test
    fun `getTeam whenCalledMultiplesTimes askRepositoryOnce`() {
        viewModel = EditTeamViewModel(
                teamRepository,
                teamNameValidator,
                descriptionValidator,
                locationService)
        every { teamRepository.getTeam(3) } returns
                Flowable.never()

        viewModel.getTeam(3).test()
        viewModel.getTeam(3).test()
        viewModel.getTeam(4).test()
        viewModel.getTeam(5).test()

        verify(exactly = 1) { teamRepository.getTeam(3) }
        verify(exactly = 0) { teamRepository.getTeam(4) }
        verify(exactly = 0) { teamRepository.getTeam(5) }
    }

    @Test
    fun `getTeam whenSubscribedToSecondCallAfterFirstCallEnded teamReturned`() {
        viewModel = EditTeamViewModel(
                teamRepository,
                teamNameValidator,
                descriptionValidator,
                locationService)
        every { teamRepository.getTeam(3) } returns
                Flowable.just(Resource(FetchStatus.SUCCESS, initialTeam))

        viewModel.getTeam(3).test()
        testSchedulerRule.testScheduler.advanceTimeBy(400, TimeUnit.MILLISECONDS)
        viewModel.getTeam(3).test()
                .assertValue(initialTeam)
    }


    @Test
    fun `updateTeam whenCalledAfterChangesMade changedTeamPassed`() {
        val team = Team(initialTeam.id,
                "Handball team",
                "Interesting description",
                Location("Paris, France", 1.0, 3.0),
                Discipline.HANDBALL,
                initialTeam.thumbnailUrl)
        viewModel.onLocationFromAutoCompleteSelected(team.location)
        viewModel.name.postValue(team.name)
        viewModel.description.postValue(team.desc!!)
        viewModel.onDisciplineSelected(team.discipline)
        every { teamRepository.updateTeam(team) } returns Completable.never()

        viewModel.onSaveChangesClicked()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        verify { teamRepository.updateTeam(team) }
    }

    @Test
    fun `onLocationFromAutocompleteSelected whenCalled locationEmitted`() {
        val location = Location("City, Country", 1.0, 0.1)

        viewModel.onLocationFromAutoCompleteSelected(location)

        viewModel.location
                .test()
                .assertValuesOnly(location)
    }

    @Test
    fun `pickCurrentLocation whenSuccess emitLocation`() {
        val location = Location("Somewhere", 99.0, 100.0)
        every { locationService.getCurrentLocation() } returns Single.just(location)

        viewModel.onPickCurrentLocationClicked()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)


        viewModel.location
                .test()
                .assertValuesOnly(location)
    }
}