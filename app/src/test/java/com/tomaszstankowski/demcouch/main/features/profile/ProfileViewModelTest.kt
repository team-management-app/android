package com.tomaszstankowski.demcouch.main.features.profile

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.repository.user.UserRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test

class ProfileViewModelTest {

    private val userRepo: UserRepository = mockk()
    private val authService: AuthenticationService = mockk()
    private lateinit var viewModel: ProfileViewModel

    @Before
    fun before() {
        viewModel = ProfileViewModel(userRepo, authService)
        every { userRepo.getUser(1) } returns Flowable.never()
        every { userRepo.getUserTeams(1) } returns Flowable.never()
    }

    @Test
    fun `init givenIdEqualToCurrentUserId editableTrueEmitted`() {
        every { authService.userId } returns 1

        viewModel.init(1)

        viewModel.isEditable.test()
                .assertValue(true)
    }

    @Test
    fun `init givenIdNotEqualToCurrentUserId editableFalseEmitted`() {
        every { authService.userId } returns 2

        viewModel.init(1)

        viewModel.isEditable.test()
                .assertValue(false)
    }

    @Test
    fun `init calledMultipleTimes logicDoneOnce`() {
        every { authService.userId } returns 5

        viewModel.init(1)
        viewModel.init(1)
        viewModel.init(2)
        viewModel.init(3)

        verify(exactly = 1) { userRepo.getUser(1) }
        verify(exactly = 0) { userRepo.getUser(2) }
        verify(exactly = 0) { userRepo.getUser(3) }
    }

}