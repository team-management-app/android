package com.tomaszstankowski.demcouch.main.viewmodel

import com.tomaszstankowski.demcouch.TestSchedulerRule
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.util.concurrent.TimeUnit

class ActionTest {
    private lateinit var action: Action

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Before
    fun before() {
        action = Action()
    }

    @Test
    fun `invokeSingle whenSubscribed emitLoadingTrue`() {
        action.invoke(Single.never<Any>()).test()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        action.loading.test()
                .assertValuesOnly(true)
    }

    @Test
    fun `invokeSingle whenFailed emitFailureThrowableAndLoadingFalse`() {
        val failureSub = action.failure.test()
        val error = IOException("Error")

        action.invoke(Single.error<Any>(error)).test()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        failureSub.assertValue(error)
        action.loading.test()
                .assertValuesOnly(false)
    }

    @Test
    fun `invokeSingle whenFailedAndSubscribedFailureAfterInvoke failureThrowableNotConsumed`() {
        val error = IOException("Error")

        action.invoke(Single.error<Any>(error)).test()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        action.failure.test()
                .assertNoValues()
    }

    @Test
    fun `invokeSingle whenSuccess emitSuccessAndLoadingFalse`() {
        action.invoke(Single.just(Any())).test()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        action.success.test()
                .assertValueCount(1)
        action.loading.test()
                .assertValuesOnly(false)
    }

    @Test
    fun `invokeCompletable whenSubscribed emitLoadingTrue`() {
        action.invoke(Completable.never()).test()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        action.loading.test()
                .assertValuesOnly(true)
    }

    @Test
    fun `invokeCompletable whenFailed emitFailureThrowableAndLoadingFalse`() {
        val failureSub = action.failure.test()
        val error = IOException("Error")

        action.invoke(Completable.error(error)).test()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        failureSub.assertValue(error)
        action.loading.test()
                .assertValuesOnly(false)
    }

    @Test
    fun `invokeCompletable whenFailedAndSubscribedFailureAfterInvoke failureThrowableNotConsumed`() {
        val error = IOException("Error")

        action.invoke(Completable.error(error)).test()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        action.failure.test()
                .assertNoValues()
    }

    @Test
    fun `invokeCompletable whenSuccess emitSuccessAndLoadingFalse`() {
        action.invoke(Completable.complete()).test()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        action.success.test()
                .assertValueCount(1)
        action.loading.test()
                .assertValuesOnly(false)
    }
}