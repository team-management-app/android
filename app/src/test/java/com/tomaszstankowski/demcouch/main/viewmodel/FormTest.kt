package com.tomaszstankowski.demcouch.main.viewmodel

import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.Form
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidatedField
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit

class FormTest {
    private lateinit var alwaysValidField: ValidatedField<String>
    private lateinit var alwaysInvalidField: ValidatedField<String>
    private lateinit var booleanField: ValidatedField<Boolean>

    @Before
    fun before() {
        alwaysValidField = ValidatedField({ Single.just(FieldStatus.correct()) }, 0)
        alwaysInvalidField = ValidatedField({ Single.just(FieldStatus.error(ValidationError.CONFLICT)) }, 0)
        booleanField = ValidatedField({
            if (it) Single.just(FieldStatus.correct())
            else Single.just(FieldStatus.error(ValidationError.CONFLICT))
        }, 0)
    }

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Test
    fun `whenRequiredFieldNotTouched invalid`() {
        val form = Form(
                Form.Field(alwaysValidField, true),
                Form.Field(alwaysInvalidField, true))

        alwaysValidField.postValue("whatever")
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        form.valid.test()
                .assertValue(false)
    }

    @Test
    fun `whenOneFieldInvalid invalid`() {
        val form = Form(
                Form.Field(alwaysValidField, true),
                Form.Field(alwaysInvalidField, true),
                Form.Field(booleanField, true))

        alwaysValidField.postValue("whatever")
        alwaysInvalidField.postValue("wrong")
        booleanField.postValue(true)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        form.valid.test()
                .assertValue(false)
    }

    @Test
    fun `whenNotRequiredFieldSkipped valid`() {
        val form = Form(
                Form.Field(alwaysValidField),
                Form.Field(alwaysInvalidField),
                Form.Field(booleanField))

        alwaysValidField.postValue("Good")
        booleanField.postValue(true)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        form.valid.test()
                .assertValue(true)
    }

    @Test
    fun `whenNotRequiredFieldInvalid invalid`() {
        val form = Form(
                Form.Field(alwaysValidField, true),
                Form.Field(alwaysInvalidField),
                Form.Field(booleanField, true))

        alwaysValidField.postValue("Good")
        alwaysInvalidField.postValue("Wrong")
        booleanField.postValue(true)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        form.valid.test()
                .assertValue(false)
    }
}