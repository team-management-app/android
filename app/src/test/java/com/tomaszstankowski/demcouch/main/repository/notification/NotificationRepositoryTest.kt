package com.tomaszstankowski.demcouch.main.repository.notification

import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationDao
import com.tomaszstankowski.demcouch.main.domain.notification.NotificationType
import com.tomaszstankowski.demcouch.main.repository.notification.model.NotificationMapper
import com.tomaszstankowski.demcouch.main.web.notifications.NotificationResponse
import com.tomaszstankowski.demcouch.main.web.notifications.NotificationService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit.SECONDS

class NotificationRepositoryTest {
    private val notificationDao: NotificationDao = mockk(relaxed = true)
    private val notificationService: NotificationService = mockk()
    private val notificationMapper: NotificationMapper = mockk()
    private val authenticationService: AuthenticationService = mockk()
    private val config = NotificationRepository.Config(10)

    private val repository = NotificationRepository(
            notificationDao,
            notificationService,
            notificationMapper,
            authenticationService,
            config)

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Test
    fun `listenForNotifications whenCurrentUserIdEqualToZero doNotFetchNotificationsFromApi`() {
        every { authenticationService.userId } returns 0

        repository.startListeningForNotifications()
        testSchedulerRule.testScheduler.advanceTimeBy(15, SECONDS)

        verify(exactly = 0) { notificationService.getUserNotifications(any()) }
    }

    @Test
    fun `listenForNotifications whenFetchFromApiFailed retryAfterInterval`() {
        every { authenticationService.userId } returns 1
        every { notificationService.getUserNotifications(1) } returns Single.error(IOException())

        repository.startListeningForNotifications()
        testSchedulerRule.testScheduler.advanceTimeBy(15, SECONDS)
        every { notificationService.getUserNotifications(1) } returns Single.never()
        testSchedulerRule.testScheduler.advanceTimeBy(10, SECONDS)

        verify(exactly = 2) { notificationService.getUserNotifications(1) }
    }

    @Test
    fun `listenForNotifications whenFetchFromApiSucceeded saveResponseInCache`() {
        val apiResponse = NotificationResponse(3, Date(), "test", NotificationType.INFO)
        val cachedNotification = apiResponse.run {
            NotificationCache(id, 1, createDate, "test", type.toString(), false)
        }
        every { authenticationService.userId } returns 1
        every { notificationService.getUserNotifications(1) } returns
                Single.just(listOf(apiResponse))
        every { notificationMapper.mapApiResponseToCache(apiResponse, 1) } returns cachedNotification

        repository.startListeningForNotifications()
        testSchedulerRule.testScheduler.advanceTimeBy(15, SECONDS)

        verify { notificationDao.insertAll(listOf(cachedNotification)) }
    }

    @Test
    fun `markNotificationAsRead whenRequestToApiFailed doNotModifyCache`() {
        every { notificationService.delete(2) } returns Completable.error(IOException())

        repository.markNotificationAsRead(2).test()

        verify(exactly = 0) { notificationDao.insert(any()) }
    }

    @Test
    fun `markNotificationAsRead whenRequestToApiSucceeded updateNotificationInCache`() {
        val notification = NotificationCache(2, 1, Date(), "test", "INFO", false)
        val updatedNotification = notification.run {
            NotificationCache(id, userId, createDate, data, type, true)
        }
        every { notificationService.delete(2) } returns Completable.complete()
        every { notificationDao.findById(2) } returns Flowable.just(notification)

        repository.markNotificationAsRead(2).test()

        verify { notificationDao.insert(updatedNotification) }
    }
}