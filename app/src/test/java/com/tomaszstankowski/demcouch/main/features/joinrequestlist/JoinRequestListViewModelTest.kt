package com.tomaszstankowski.demcouch.main.features.joinrequestlist

import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

class JoinRequestListViewModelTest {
    private val repository: TeamMembershipRepository = mockk()
    private lateinit var viewModel: JoinRequestListViewModel

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Before
    fun before() {
        viewModel = JoinRequestListViewModel(repository)
    }

    @Test
    fun `init whenCalledMultipleTimes invokeLogicOnce`() {
        every { repository.getRequestedMembershipsForTeam(5) } returns Flowable.never()

        viewModel.init(5)
        viewModel.init(5)
        viewModel.init(6)

        verify(exactly = 1) { repository.getRequestedMembershipsForTeam(5) }
        verify(exactly = 0) { repository.getRequestedMembershipsForTeam(6) }
    }

    @Test
    fun `onConfirmMembershipClicked whenFailed doNotEmitUpdatedMemberships`() {
        val memberships = listOf(
                TeamMembership(1, Date(), false, User(11, "test", "test")),
                TeamMembership(2, Date(), false, User(12, "test2", "test2")))
        every { repository.getRequestedMembershipsForTeam(5) } returns
                Flowable.just(Resource(FetchStatus.SUCCESS, memberships))
        every { repository.confirmMembership(1) } returns Single.error(IOException())

        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        viewModel.onConfirmMembershipClicked(1)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)

        viewModel.requestedMemberships.test()
                .assertValuesOnly(Resource(FetchStatus.SUCCESS, memberships))
    }

    @Test
    fun `onRejectMembershipClicked whenFailed doNotEmitUpdatedMemberships`() {
        val memberships = listOf(
                TeamMembership(1, Date(), false, User(11, "test", "test")),
                TeamMembership(2, Date(), false, User(12, "test2", "test2")))
        every { repository.getRequestedMembershipsForTeam(5) } returns
                Flowable.just(Resource(FetchStatus.SUCCESS, memberships))
        every { repository.deleteMembership(1) } returns Completable.error(IOException())

        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        viewModel.onRejectMembershipClicked(1)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)

        viewModel.requestedMemberships.test()
                .assertValuesOnly(Resource(FetchStatus.SUCCESS, memberships))
    }

    @Test
    fun `onConfirmMembershipClicked whenSucceeded emitUpdatedMemberships`() {
        val memberships = listOf(
                TeamMembership(1, Date(), false, User(11, "test", "test")),
                TeamMembership(2, Date(), false, User(12, "test2", "test2")))
        every { repository.getRequestedMembershipsForTeam(5) } returns
                Flowable.just(Resource(FetchStatus.SUCCESS, memberships))
        every { repository.confirmMembership(1) } returns
                Single.just(TeamMembership(1, Date(), true, User(1, "test", "test")))

        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        viewModel.onConfirmMembershipClicked(1)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)

        viewModel.requestedMemberships.test()
                .assertValuesOnly(Resource(FetchStatus.SUCCESS, listOf(memberships[1])))
    }

    @Test
    fun `onRejectMembershipClicked whenSucceeded emitUpdatedMemberships`() {
        val memberships = listOf(
                TeamMembership(1, Date(), false, User(11, "test", "test")),
                TeamMembership(2, Date(), false, User(12, "test2", "test2")))
        every { repository.getRequestedMembershipsForTeam(5) } returns
                Flowable.just(Resource(FetchStatus.SUCCESS, memberships))
        every { repository.deleteMembership(1) } returns
                Completable.complete()

        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        viewModel.onRejectMembershipClicked(1)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)

        viewModel.requestedMemberships.test()
                .assertValuesOnly(Resource(FetchStatus.SUCCESS, listOf(memberships[1])))
    }
}