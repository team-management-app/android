package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import org.junit.Test

class PasswordValidatorTest {

    private val passwordValidator = PasswordValidator()

    @Test
    fun `validate whenPasswordTooShort emitStatusError`() {
        passwordValidator.validate("short")
                .test()
                .assertResult(FieldStatus.error(ValidationError.MIN_LENGTH, 6))
    }

    @Test
    fun `validate whenPasswordTooLong emitStatusError`() {
        passwordValidator.validate("s".repeat(31))
                .test()
                .assertResult(FieldStatus.error(ValidationError.MAX_LENGTH, 30))
    }

    @Test
    fun `validate whenPasswordCorrect emitStatusCorrect`() {
        passwordValidator.validate("GoodPw")
                .test()
                .assertResult(FieldStatus.correct())
    }
}