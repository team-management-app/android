package com.tomaszstankowski.demcouch.main.features.managememberships

import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

class ManageMembershipsViewModelTest {
    private val repository: TeamMembershipRepository = mockk()
    private lateinit var viewModel: ManageMembershipsViewModel

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Before
    fun before() {
        viewModel = ManageMembershipsViewModel(repository)
    }

    @Test
    fun `init whenCalledMultipleTimes invokeLogicOnce`() {
        every { repository.getConfirmedMembershipsForTeam(5) } returns Flowable.never()

        viewModel.init(5)
        viewModel.init(5)
        viewModel.init(6)

        verify(exactly = 1) { repository.getConfirmedMembershipsForTeam(5) }
        verify(exactly = 0) { repository.getConfirmedMembershipsForTeam(6) }
    }

    @Test
    fun `onDeleteMembershipClicked whenFailed doNotEmitUpdatedMemberships`() {
        val memberships = listOf(
                TeamMembership(1, Date(), true, User(11, "test", "test")),
                TeamMembership(2, Date(), true, User(12, "test2", "test2")))
        every { repository.getConfirmedMembershipsForTeam(5) } returns
                Flowable.just(Resource(FetchStatus.SUCCESS, memberships))
        every { repository.deleteMembership(1) } returns Completable.error(IOException())

        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        viewModel.onDeleteMembershipClicked(memberships[0])
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.memberships.test()
                .assertValuesOnly(Resource(FetchStatus.SUCCESS, memberships))
    }

    @Test
    fun `onDeleteMembershipClicked whenSucceeded emitUpdatedMemberships`() {
        val memberships = listOf(
                TeamMembership(1, Date(), true, User(11, "test", "test")),
                TeamMembership(2, Date(), true, User(12, "test2", "test2")))
        every { repository.getConfirmedMembershipsForTeam(5) } returns
                Flowable.just(Resource(FetchStatus.SUCCESS, memberships))
        every { repository.deleteMembership(1) } returns Completable.complete()

        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        viewModel.onDeleteMembershipClicked(memberships[0])
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.memberships.test()
                .assertValuesOnly(Resource(FetchStatus.SUCCESS, listOf(memberships[1])))
    }
}