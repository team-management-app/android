package com.tomaszstankowski.demcouch.main.features.createteam

import android.net.Uri
import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.repository.team.TeamRepository
import com.tomaszstankowski.demcouch.main.web.team.TeamRequest
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit

class CreateTeamViewModelTest {

    private val repository: TeamRepository = mockk()
    private val authService: AuthenticationService = mockk()
    private lateinit var viewModel: CreateTeamViewModel
    private val teamRequest = TeamRequest(
            "Schaby",
            "",
            Location("Schabowo", 1.0, 1.0),
            Discipline.BASKETBALL)
    private val team = Team(
            1,
            teamRequest.name,
            teamRequest.description,
            teamRequest.location,
            teamRequest.discipline,
            null)

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Before
    fun before() {
        viewModel = CreateTeamViewModel(repository, authService)
    }

    @Test
    fun `createTeam whenTeamCreatedButUploadingThumbnailFailed successEmitted`() {
        val error = IllegalStateException("Fail!")
        val thumbnail: Uri = mockk()
        viewModel.thumbnail = thumbnail
        every {
            repository.createTeam(
                    teamRequest.name,
                    teamRequest.description,
                    teamRequest.location,
                    teamRequest.discipline,
                    5)
        } returns Single.just(team)
        every { repository.updateThumbnail(team.id, thumbnail) } returns Completable.error(error)
        every { authService.userId } returns 5
        viewModel.name = teamRequest.name
        viewModel.description = teamRequest.description
        viewModel.discipline = teamRequest.discipline
        viewModel.location = teamRequest.location
        val failureSub = viewModel.createTeam.failure.test()

        viewModel.createTeam()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        failureSub.assertNoValues()
        viewModel.createTeam.success
                .test()
                .assertValueCount(1)
    }

    @Test
    fun `currentStep whenLastStepReached emitReady`() {
        viewModel.currentStep = 4

        viewModel.ready.test().assertValuesOnly(true)
    }

    @Test
    fun `currentStep whenSteppedBackFromLastStep emitFalse`() {
        viewModel.currentStep = 4
        viewModel.currentStep = 3

        viewModel.ready
                .test()
                .assertValuesOnly(false)
    }
}