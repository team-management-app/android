package com.tomaszstankowski.demcouch.main.features.createteam.steps.chooselocation

import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.misc.LocationService
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.util.concurrent.TimeUnit

class ChooseLocationViewModelTest {
    private val locationService: LocationService = mockk()
    private lateinit var viewModel: ChooseLocationViewModel
    private val testLocation = Location("Somewhere", 50.0, 100.0)

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Before
    fun before() {
        viewModel = ChooseLocationViewModel(locationService)
    }

    @Test
    fun `onLocationFromAutocompleteSelected whenCalled successEmittedAndSnapshotNotNull`() {
        viewModel.onLocationFromAutocompleteSelected(testLocation)

        viewModel.location
                .test()
                .assertValuesOnly(Resource(FetchStatus.SUCCESS, testLocation))
        assertEquals(testLocation, viewModel.locationSnapshot)
    }

    @Test
    fun `pickCurrentLocation whenCalled loadingEmitted`() {
        val error = IOException("Location unavailable")
        every { locationService.getCurrentLocation() } returns Single.error(error)

        viewModel.pickCurrentLocation()

        viewModel.location.test()
                .assertValuesOnly(Resource(FetchStatus.LOADING, null))
                .assertNotComplete()
    }


    @Test
    fun `pickCurrentLocation whenFailed resourceFailEmittedAndSnapshotNull`() {
        val error = IOException("Location unavailable")
        every { locationService.getCurrentLocation() } returns Single.error(error)

        viewModel.pickCurrentLocation()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        assertNull(viewModel.locationSnapshot)
        viewModel.location.test()
                .assertValuesOnly(
                        Resource(FetchStatus.FAIL, null)
                )
                .assertNotComplete()
    }

    @Test
    fun `pickCurrentLocation whenSuccess successEmittedAndSnapshotNotNull`() {
        every { locationService.getCurrentLocation() } returns Single.just(testLocation)

        viewModel.pickCurrentLocation()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        assertEquals(testLocation, viewModel.locationSnapshot)
        viewModel.location
                .test()
                .assertValues(
                        Resource(FetchStatus.SUCCESS, testLocation)
                )
                .assertNotComplete()
    }
}