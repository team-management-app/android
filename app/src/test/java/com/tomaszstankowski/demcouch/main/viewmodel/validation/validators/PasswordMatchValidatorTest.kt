package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import org.junit.Test

class PasswordMatchValidatorTest {

    private val validator = PasswordMatchValidator()

    @Test
    fun `validate whenPasswordsMatch emitStatusCorrect`() {
        validator.validate("pass", "pass")
                .test()
                .assertResult(FieldStatus.correct())
    }

    @Test
    fun `validate whenPasswordDoNotMatch emitStatusError`() {
        validator.validate("password", "pass")
                .test()
                .assertResult(FieldStatus.error(ValidationError.NOT_MATCH, null))
    }
}