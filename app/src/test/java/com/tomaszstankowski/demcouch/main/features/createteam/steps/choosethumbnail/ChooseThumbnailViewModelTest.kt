package com.tomaszstankowski.demcouch.main.features.createteam.steps.choosethumbnail

import android.net.Uri
import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.ThumbnailValidator
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.util.concurrent.TimeUnit

class ChooseThumbnailViewModelTest {

    private val thumbnailValidator: ThumbnailValidator = mockk()
    private lateinit var viewModel: ChooseThumbnailViewModel

    @Rule
    @JvmField
    val testScheduler = TestSchedulerRule()

    @Before
    fun before() {
        viewModel = ChooseThumbnailViewModel(thumbnailValidator)
    }

    @Test
    fun `onThumbnailSelected whenThumbnailTooLarge emitRejected`() {
        val uri: Uri = mockk()
        every { thumbnailValidator.validate(uri) } returns
                Single.just(FieldStatus.error(ValidationError.SIZE_EXCEEDED, 1))
        val subscriber = viewModel.thumbnailSelection.test()

        viewModel.onThumbnailSelected(uri)
        testScheduler.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        subscriber.assertValue(
                ThumbnailSelection.rejected(
                        uri,
                        FieldStatus.error(ValidationError.SIZE_EXCEEDED, 1)))
        verify(exactly = 1) { thumbnailValidator.validate(uri) }
    }

    @Test
    fun `onThumbnailSelected whenThumbnailOk emitAccepted`() {
        val uri: Uri = mockk()
        every { thumbnailValidator.validate(uri) } returns
                Single.just(FieldStatus.correct())
        val subscriber = viewModel.thumbnailSelection.test()

        viewModel.onThumbnailSelected(uri)
        testScheduler.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        subscriber.assertValue(ThumbnailSelection.accepted(uri))
        verify(exactly = 1) { thumbnailValidator.validate(uri) }
    }


    @Test
    fun `onThumbnailSelected whenErrorDuringValidationOccurred emitEmpty`() {
        val uri: Uri = mockk()
        val error = IOException()
        every { thumbnailValidator.validate(uri) } returns Single.error(error)
        val subscriber = viewModel.thumbnailSelection.test()

        viewModel.onThumbnailSelected(uri)
        testScheduler.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        subscriber.assertValue(ThumbnailSelection.empty())
        verify(exactly = 1) { thumbnailValidator.validate(uri) }
    }

    @Test
    fun `onThumbnailRemoveClicked emitEmpty`() {
        val subscriber = viewModel.thumbnailSelection.test()

        viewModel.onThumbnailRemoveClicked()

        subscriber.assertValue(ThumbnailSelection.empty())
    }
}