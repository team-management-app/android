package com.tomaszstankowski.demcouch.main.repository

import android.net.Uri
import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.cache.team.TeamCache
import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.cache.user.UserDao
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.misc.ThumbnailService
import com.tomaszstankowski.demcouch.main.repository.team.TeamMapper
import com.tomaszstankowski.demcouch.main.repository.team.TeamRepository
import com.tomaszstankowski.demcouch.main.repository.user.UserMapper
import com.tomaszstankowski.demcouch.main.web.core.model.ThumbnailResponse
import com.tomaszstankowski.demcouch.main.web.team.TeamRequest
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import com.tomaszstankowski.demcouch.main.web.team.TeamService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import okhttp3.MultipartBody
import org.junit.Rule
import org.junit.Test
import java.io.IOException

class TeamRepositoryTest {
    private val service: TeamService = mockk()
    private val teamDao: TeamDao = mockk(relaxed = true)
    private val userDao: UserDao = mockk()
    private val userTeamJoinDao: UserTeamJoinDao = mockk(relaxed = true)
    private val thumbnailService: ThumbnailService = mockk()
    private val teamMapper: TeamMapper = mockk()
    private val userMapper: UserMapper = mockk()
    private val fetchMemory: ResourceFetchMemory = mockk()
    private val repository = TeamRepository(service, teamDao, userDao, userTeamJoinDao,
            thumbnailService, teamMapper, userMapper, fetchMemory)

    private val teamRequest = TeamRequest(
            "Kaszubia",
            "",
            Location("Kościerzyna, Polska", 0.0, 0.0),
            Discipline.FOOTBALL)
    private val team = Team(
            5,
            teamRequest.name,
            teamRequest.description,
            teamRequest.location,
            teamRequest.discipline,
            null)
    private val teamResponse = TeamResponse(
            team.id,
            team.name,
            team.desc,
            team.location,
            team.discipline,
            team.thumbnailUrl)
    private val teamCache = TeamCache(
            team.id,
            team.name,
            team.desc,
            team.location,
            team.discipline.toString(),
            team.thumbnailUrl)


    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Test
    fun `addTeam whenApiRespondsOk saveTeamInCache`() {
        every { service.createTeam(teamRequest) } returns Single.just(teamResponse)
        every { teamMapper.mapApiResponseToCache(teamResponse) } returns teamCache
        every { teamMapper.mapCacheToDomain(teamCache) } returns team

        repository.createTeam(
                teamRequest.name,
                teamRequest.description,
                teamRequest.location,
                teamRequest.discipline,
                5)
                .test()
                .assertComplete()

        verify(exactly = 1) { teamDao.insert(teamCache) }
        verify(exactly = 1) { userTeamJoinDao.insert(UserTeamJoin(5, teamResponse.id)) }
    }

    @Test
    fun `addTeam whenCallToApiFails doNotSaveTeamInCache`() {
        val error = IOException("Error")
        every { service.createTeam(teamRequest) } returns Single.error(error)

        repository.createTeam(
                teamRequest.name,
                teamRequest.description,
                teamRequest.location,
                teamRequest.discipline,
                5)
                .test()
                .assertError(error)

        verify(exactly = 0) { teamDao.insert(allAny()) }
    }

    @Test
    fun `updateTeam whenApiRespondsOk updateCache`() {
        every { service.updateTeam(team.id.toString(), teamRequest) } returns Single.just(teamResponse)
        every { teamMapper.mapApiResponseToCache(teamResponse) } returns teamCache

        repository.updateTeam(team)
                .test()
                .assertComplete()

        verify(exactly = 1) { teamDao.insert(teamCache) }
    }

    @Test
    fun `updateTeam whenCallToApiFails doNotUpdateCache`() {
        val error = IOException("Error!")
        every { service.updateTeam(team.id.toString(), teamRequest) } returns Single.error(error)

        repository.updateTeam(team)
                .test()
                .assertError(error)

        verify(exactly = 0) { teamDao.insert(allAny()) }
    }

    @Test
    fun `deleteTeam whenApiCallFails doNotWipeTeamFromCache`() {
        val error = IOException("No network")
        every { service.deleteTeam("1") } returns Completable.error(error)

        repository.deleteTeam(1)
                .test()
                .assertError(error)

        verify(exactly = 0) { teamDao.delete(1) }
    }

    @Test
    fun `deleteTeam whenApiCallSucceeds wipeTeamFromCache`() {
        every { service.deleteTeam("1") } returns Completable.complete()

        repository.deleteTeam(1)
                .test()
                .assertComplete()

        verify { teamDao.delete(1) }
        verify { userTeamJoinDao.deleteByTeam(1) }
    }

    @Test
    fun `uploadThumbnail whenApiCallFails doNoUpdateCache`() {
        val thumbnail: Uri = mockk()
        val body: MultipartBody.Part = mockk()
        val error = IOException("saveError")
        every { thumbnailService.prepareMultipartBody(thumbnail) } returns body
        every { service.uploadThumbnail(team.id.toString(), body) } returns Single.error(error)
        every { teamDao.findById(team.id) } returns Flowable.just(teamCache)

        repository.updateThumbnail(team.id, thumbnail)
                .test()
                .assertError(error)

        verify(exactly = 0) { teamDao.insert(any()) }
    }

    @Test
    fun `uploadThumbnail whenFetchingTeamFromCacheFailed doNoUpdateCache`() {
        val thumbnail: Uri = mockk()
        val body: MultipartBody.Part = mockk()
        val error = IOException("saveError")
        every { thumbnailService.prepareMultipartBody(thumbnail) } returns body
        every { service.uploadThumbnail(team.id.toString(), body) } returns Single.just(ThumbnailResponse("thumb.jpg"))
        every { teamDao.findById(team.id) } returns Flowable.error(error)

        repository.updateThumbnail(team.id, thumbnail)
                .test()
                .assertError(error)

        verify(exactly = 0) { teamDao.insert(any()) }
    }

    @Test
    fun `uploadThumbnail whenAllGood updateCache`() {
        val thumbnail: Uri = mockk()
        val body: MultipartBody.Part = mockk()
        every { thumbnailService.prepareMultipartBody(thumbnail) } returns body
        every { service.uploadThumbnail(team.id.toString(), body) } returns Single.just(ThumbnailResponse("thumb.jpg"))
        every { thumbnailService.normalizeThumbnailUrl("thumb.jpg") } returns "http://api:8080/static/thumb.jpg"
        every { teamDao.findById(team.id) } returns Flowable.just(teamCache)

        repository.updateThumbnail(team.id, thumbnail)
                .test()
                .assertComplete()

        verify {
            teamDao.insert(TeamCache(
                    teamCache.id,
                    teamCache.name,
                    teamCache.desc,
                    teamCache.location,
                    teamCache.discipline,
                    "http://api:8080/static/thumb.jpg"
            ))
        }
    }

    @Test
    fun `deleteThumbnail whenApiCallFails doNotUpdateCache`() {
        val error = IOException("Veto")
        every { service.deleteThumbnail(team.id.toString()) } returns Completable.error(error)
        every { teamDao.findById(team.id) } returns Flowable.just(teamCache)

        repository.deleteThumbnail(team.id)
                .test()
                .assertError(error)
        verify(exactly = 0) { teamDao.insert(any()) }
    }

    @Test
    fun `deleteThumbnail whenFetchFromCacheFails doNotUpdateCache`() {
        val error = IOException("Veto")
        every { service.deleteThumbnail(team.id.toString()) } returns Completable.complete()
        every { teamDao.findById(team.id) } returns Flowable.error(error)

        repository.deleteThumbnail(team.id)
                .test()
                .assertError(error)
        verify(exactly = 0) { teamDao.insert(any()) }
    }

    @Test
    fun `deleteThumbnail whenAllGood updateCache`() {
        every { service.deleteThumbnail(team.id.toString()) } returns Completable.complete()
        every { teamDao.findById(team.id) } returns Flowable.just(teamCache)

        repository.deleteThumbnail(team.id)
                .test()
                .assertComplete()
        verify {
            teamDao.insert(
                    TeamCache(
                            teamCache.id,
                            teamCache.name,
                            teamCache.desc,
                            teamCache.location,
                            teamCache.discipline,
                            null
                    )
            )
        }
    }
}