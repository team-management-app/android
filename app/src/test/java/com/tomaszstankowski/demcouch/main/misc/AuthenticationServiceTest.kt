package com.tomaszstankowski.demcouch.main.misc

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.auth.LogOutEventEmitter
import com.tomaszstankowski.demcouch.main.auth.UserCacheService
import com.tomaszstankowski.demcouch.main.web.login.LoginService
import com.tomaszstankowski.demcouch.main.web.user.UserService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.assertEquals
import org.junit.Test

class AuthenticationServiceTest {
    private val userCacheService: UserCacheService = mockk(relaxed = true)
    private val loginService: LoginService = mockk()
    private val userService: UserService = mockk()
    private val logOutEventEmitter: LogOutEventEmitter = mockk(relaxed = true)
    private val authService = AuthenticationService(
            userCacheService, loginService, userService, logOutEventEmitter)

    @Test
    fun `getUserId whenUserIdNotPresentInCache performLogOut`() {
        every { userCacheService.userId } returns null

        val result = authService.userId

        assertEquals(0, result)
        verify { userCacheService.invalidateCurrentUser() }
        verify { logOutEventEmitter.emit() }
    }

    @Test
    fun `getUserId whenUserIdPresentInCache justReturnIt`() {
        every { userCacheService.userId } returns 5

        val result = authService.userId

        assertEquals(5, result)
        verify(exactly = 0) { userCacheService.invalidateCurrentUser() }
        verify(exactly = 0) { logOutEventEmitter.emit() }
    }
}