package com.tomaszstankowski.demcouch.main.repository

import android.content.SharedPreferences
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class ResourceFetchMemoryTest {
    private val sharedPreferences: SharedPreferences = mockk()
    private val config = ResourceFetchMemory.Config(10, "test_")
    private val clock: Clock = mockk()
    private val fetchMemory = ResourceFetchMemory(sharedPreferences, config, clock)

    @Test
    fun `shouldFetch whenCalledBeforeFreshTimeExpired returnFalse`() {
        every { sharedPreferences.getLong("test_resource", 0) } returns 0
        every { clock.currentTimeMs } returns 10

        val result = fetchMemory.shouldFetch("resource")

        assertFalse(result)
    }

    @Test
    fun `shouldFetch_whenCalledAfterFreshTimeExpired returnTrue`() {
        every { sharedPreferences.getLong("test_resource", 0) } returns 100
        every { clock.currentTimeMs } returns 111

        val result = fetchMemory.shouldFetch("resource")

        assertTrue(result)
    }
}