package com.tomaszstankowski.demcouch.main.repository

import com.tomaszstankowski.demcouch.TestSchedulerRule
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.util.concurrent.TimeUnit

class RepositoryUtilsTest {

    private data class ApiResponseData(val id: Long)

    private data class ResultData(val id: Long)

    private data class CacheData(val id: Long)

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    private val cacheSubject: Subject<CacheData> = PublishSubject.create()

    private val apiSubject: Subject<ApiResponseData> = PublishSubject.create()

    private val saveInCacheSpy = spyk<(ApiResponseData) -> Unit>(objToCopy = { })

    private val onNotFoundInApiSpy = spyk<() -> Unit> { }

    private val onFetchCompletedSpy = spyk<() -> Unit> { }

    private lateinit var resourceFlowable: Flowable<Resource<ResultData>>

    @Before
    fun before() {
        resourceFlowable = combineCacheWithApi(
                cacheSubject.toFlowable(BackpressureStrategy.LATEST),
                apiSubject.firstOrError(),
                saveInCacheSpy,
                { cache: CacheData -> ResultData(cache.id) },
                onNotFoundInApiSpy,
                { true },
                onFetchCompletedSpy)
    }

    @Test
    fun `combineCacheWithApi whenFetchFromApiTakesLong emitLoading`() {
        val subscriber = resourceFlowable.test()

        cacheSubject.onNext(CacheData(5))

        subscriber.assertNoValues()
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        subscriber.assertValues(Resource(FetchStatus.LOADING, ResultData(5)))
        subscriber.assertNotComplete()
    }

    @Test
    fun `combineCacheWithApi whenFetchFromApiSucceedsAfterDebounceTime emitLoadingAndThenSuccess`() {
        val subscriber = resourceFlowable.test()

        cacheSubject.onNext(CacheData(5))
        subscriber.assertNoValues()

        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        subscriber.assertValueAt(0, Resource(FetchStatus.LOADING, ResultData(5)))

        apiSubject.onNext(ApiResponseData(5))
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        subscriber.assertValueAt(1, Resource(FetchStatus.SUCCESS, ResultData(5)))
        subscriber.assertNotComplete()
    }

    @Test
    fun `combineCacheWithApi whenFetchFromApiSucceedsBeforeDebounceTime emitSuccess`() {
        val subscriber = resourceFlowable.test()

        cacheSubject.onNext(CacheData(5))
        subscriber.assertNoValues()

        testSchedulerRule.testScheduler.advanceTimeBy(200, TimeUnit.MILLISECONDS)
        subscriber.assertNoValues()

        apiSubject.onNext(ApiResponseData(5))
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        subscriber.assertValues(Resource(FetchStatus.SUCCESS, ResultData(5)))
        subscriber.assertNotComplete()
    }

    @Test
    fun `combineCacheWithApi whenFetchFromApiFailedBeforeDebounceTime emitFailure`() {
        val subscriber = resourceFlowable.test()
        val error = IOException("Error")

        cacheSubject.onNext(CacheData(5))
        subscriber.assertNoValues()

        testSchedulerRule.testScheduler.advanceTimeBy(200, TimeUnit.MILLISECONDS)
        subscriber.assertNoValues()

        apiSubject.onError(error)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        subscriber.assertValues(Resource(FetchStatus.FAIL, ResultData(5)))
        subscriber.assertNotComplete()
    }

    @Test
    fun `combineCacheWithApi whenFetchFromApiFailedAfterDebounceTime emitLoadingAndThenFailure`() {
        val subscriber = resourceFlowable.test()
        val error = IOException("Error")

        cacheSubject.onNext(CacheData(5))
        subscriber.assertNoValues()

        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        subscriber.assertValueAt(0, Resource(FetchStatus.LOADING, ResultData(5)))
        apiSubject.onError(error)

        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        subscriber.assertValueAt(1, Resource(FetchStatus.FAIL, ResultData(5)))
        subscriber.assertNotComplete()
    }

    @Test
    fun `combineCacheWithApi whenFetchFromApiSucceeded cacheSourceStillEmits`() {
        val subscriber = resourceFlowable.test()

        cacheSubject.onNext(CacheData(5))

        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)
        apiSubject.onNext(ApiResponseData(5))

        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        subscriber.assertValueAt(0, Resource(FetchStatus.SUCCESS, ResultData(5)))

        cacheSubject.onNext(CacheData(6))
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        subscriber.assertValueAt(1, Resource(FetchStatus.SUCCESS, ResultData(6)))
        subscriber.assertNotComplete()
    }

    @Test
    fun `combineCacheWithApi whenFetchSucceeded saveResponseInCache`() {
        resourceFlowable.test()

        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)
        apiSubject.onNext(ApiResponseData(3))
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)

        verify { saveInCacheSpy(ApiResponseData(3)) }
    }

    @Test
    fun `combineCacheWithApi whenFetchFailed nothingSavedInCache`() {
        resourceFlowable.test()

        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)
        apiSubject.onError(IOException())
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)

        verify(exactly = 0) { saveInCacheSpy(any()) }
    }

    @Test
    fun `combineCacheWithApi whenApiResponseIs404 invokeOnNotFoundInApi`() {
        val error = HttpException(Response.error<Any>(404, mockk()))
        resourceFlowable.test()

        apiSubject.onError(error)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)

        verify { onNotFoundInApiSpy() }
    }

    @Test
    fun `combineCacheWithApi whenApiResponseIsNot404 doNotInvokeOnNotFoundInApi`() {
        val error = HttpException(Response.error<Any>(401, mockk()))
        resourceFlowable.test()

        apiSubject.onError(error)
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)

        verify(exactly = 0) { onNotFoundInApiSpy() }
    }

    @Test
    fun `combineCacheWithApi whenShouldNotFetch emitResourceSuccess`() {
        val sub = combineCacheWithApi(
                cacheSubject.toFlowable(BackpressureStrategy.LATEST),
                apiSubject.firstOrError(),
                saveInCacheSpy,
                { cache: CacheData -> ResultData(cache.id) },
                onNotFoundInApiSpy,
                { false },
                onFetchCompletedSpy)
                .test()

        cacheSubject.onNext(CacheData(5))
        testSchedulerRule.testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)

        sub.assertValuesOnly(Resource(FetchStatus.SUCCESS, ResultData(5)))
    }
}