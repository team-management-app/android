package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import com.tomaszstankowski.demcouch.main.web.core.model.PageResponse
import com.tomaszstankowski.demcouch.main.web.team.TeamService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Rule
import org.junit.Test

class TeamNameValidatorTest {

    private val teamService: TeamService = mockk()
    private val teamNameValidationService = TeamNameValidator(teamService)
    private val emptyPage = PageResponse<FindTeamsResponse>(emptyList(), PageResponse.Pagination(1, 1, 0))
    private val existingTeam = FindTeamsResponse(
            3,
            "Arka Gdynia",
            null,
            Location("Gdynia, Polska", 0.0, 0.0),
            Discipline.FOOTBALL,
            null)
    private val notEmptyPage = PageResponse(listOf(existingTeam), PageResponse.Pagination(1, 1, 1))

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    @Test
    fun `validate whenNameValid emitNoErrors`() {
        every { teamService.getTeams(name = "Chelsea London") } returns Single.just(emptyPage)
        teamNameValidationService.validate("Chelsea London")
                .test()
                .assertValue(FieldStatus.correct())
        verify(exactly = 1) { teamService.getTeams(name = "Chelsea London") }
    }

    @Test
    fun `validate whenNameInValidNames emitNoError`() {
        teamNameValidationService.validate("Ddd", "Ddd")
                .test()
                .assertValue(FieldStatus.correct())
        verify(exactly = 0) { teamService.getTeams(name = allAny()) }
    }

    @Test
    fun `validate whenNameTooShort emitMinLengthValidationError`() {
        teamNameValidationService.validate("Arka")
                .test()
                .assertValue(FieldStatus.error(ValidationError.MIN_LENGTH, TeamNameValidator.MIN_LENGTH))
        verify(exactly = 0) { teamService.getTeams(name = allAny()) }
    }

    @Test
    fun `validate whenNameTooLong emitMaxLengthValidationError`() {
        teamNameValidationService.validate("d".repeat(31))
                .test()
                .assertValue(FieldStatus.error(ValidationError.MAX_LENGTH, TeamNameValidator.MAX_LENGTH))
        verify(exactly = 0) { teamService.getTeams(name = allAny()) }
    }

    @Test
    fun `validate whenNameAlreadyExists emitConflictValidationError`() {
        every { teamService.getTeams(name = existingTeam.name) } returns Single.just(notEmptyPage)

        teamNameValidationService.validate(existingTeam.name)
                .test()
                .assertValue(FieldStatus.error(ValidationError.CONFLICT, existingTeam.name))
        verify(exactly = 1) { teamService.getTeams(name = existingTeam.name) }
    }
}