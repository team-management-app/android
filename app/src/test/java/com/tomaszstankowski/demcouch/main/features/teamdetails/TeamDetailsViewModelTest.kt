package com.tomaszstankowski.demcouch.main.features.teamdetails

import android.net.Uri
import com.tomaszstankowski.demcouch.TestSchedulerRule
import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.team.TeamRepository
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

class TeamDetailsViewModelTest {

    private val teamRepository: TeamRepository = mockk()
    private val authService: AuthenticationService = mockk()
    private val teamMembershipRepository: TeamMembershipRepository = mockk()
    private lateinit var viewModel: TeamDetailsViewModel

    @Rule
    @JvmField
    val testSchedulerRule = TestSchedulerRule()

    private val users = mapOf(
            Pair(1, User(1, "Janusz", "Tracz")),
            Pair(2, User(2, "Grzegorz", "Brzęczyk")))

    @Test
    fun `changeThumbnail whenRepositoryFailed emitError`() {
        viewModel = TeamDetailsViewModel(teamRepository, authService, teamMembershipRepository)
        every { teamRepository.getTeam(9999) } returns Flowable.never()
        every { teamRepository.getTeamMemberships(9999) } returns Flowable.never()
        every { teamMembershipRepository.getConfirmedMembershipsForTeam(9999) } returns Flowable.never()
        every { teamMembershipRepository.getRequestedMembershipsForTeam(9999) } returns Flowable.never()
        viewModel.init(9999)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        val uri: Uri = mockk()
        val error = IOException("Error!")
        every { teamRepository.updateThumbnail(9999, uri) } returns Completable.error(error)
        val errorSub = viewModel.error.test()

        viewModel.onThumbnailSelected(uri)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        errorSub.assertValuesOnly(error)
    }

    @Test
    fun `removeThumbnail whenRepositoryFailed emitError`() {
        viewModel = TeamDetailsViewModel(teamRepository, authService, teamMembershipRepository)
        every { teamRepository.getTeam(9999) } returns Flowable.never()
        every { teamRepository.getTeamMemberships(9999) } returns Flowable.never()
        every { teamMembershipRepository.getConfirmedMembershipsForTeam(9999) } returns Flowable.never()
        every { teamMembershipRepository.getRequestedMembershipsForTeam(9999) } returns Flowable.never()
        viewModel.init(9999)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        val error = IllegalStateException("Bo tak")
        val errorSub = viewModel.error.test()
        every { teamRepository.deleteThumbnail(9999) } returns Completable.error(error)

        viewModel.onThumbnailRemoveClicked()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        errorSub.assertValuesOnly(error)
    }

    @Test
    fun `init whenCurrentUseMembershipNotConfirmed emitMembershipStatusPending`() {
        viewModel = TeamDetailsViewModel(teamRepository, authService, teamMembershipRepository)
        every { teamRepository.getTeam(5) } returns Flowable.never()
        every { teamRepository.getTeamMemberships(5) } returns Flowable.never()
        every { teamMembershipRepository.getRequestedMembershipsForTeam(5) } returns Flowable.never()
        every { teamMembershipRepository.getConfirmedMembershipsForTeam(5) } returns Flowable.just(
                Resource(FetchStatus.SUCCESS, listOf(
                        TeamMembership(6, Date(), false, users[1]!!),
                        TeamMembership(7, Date(), true, users[2]!!))))
        every { authService.userId } returns 1

        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.membershipStatus.test()
                .assertValue(MembershipStatus.PENDING)
    }

    @Test
    fun `init whenCurrentUserMembershipConfirmed emitMembershipStatusActive`() {
        viewModel = TeamDetailsViewModel(teamRepository, authService, teamMembershipRepository)
        every { teamRepository.getTeam(5) } returns Flowable.never()
        every { teamRepository.getTeamMemberships(5) } returns Flowable.never()
        every { teamMembershipRepository.getRequestedMembershipsForTeam(5) } returns Flowable.never()
        every { teamMembershipRepository.getConfirmedMembershipsForTeam(5) } returns Flowable.just(
                Resource(FetchStatus.SUCCESS, listOf(
                        TeamMembership(6, Date(), true, users[1]!!),
                        TeamMembership(7, Date(), false, users[2]!!))))
        every { authService.userId } returns 1

        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.membershipStatus.test()
                .assertValue(MembershipStatus.PLAYER)
    }

    @Test
    fun `init whenCurrentUserNotInMembers emitMembershipStatusNone`() {
        viewModel = TeamDetailsViewModel(teamRepository, authService, teamMembershipRepository)
        every { teamRepository.getTeam(5) } returns Flowable.never()
        every { teamRepository.getTeamMemberships(5) } returns Flowable.never()
        every { teamMembershipRepository.getRequestedMembershipsForTeam(5) } returns Flowable.never()
        every { teamMembershipRepository.getConfirmedMembershipsForTeam(5) } returns Flowable.just(
                Resource(FetchStatus.SUCCESS, listOf(
                        TeamMembership(6, Date(), true, users[1]!!),
                        TeamMembership(7, Date(), true, users[2]!!))))
        every { authService.userId } returns 3

        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.membershipStatus.test()
                .assertValue(MembershipStatus.NONE)
    }

    @Test
    fun `onJoinTeamClicked whenFailed doNotEmitNewMembershipStatus`() {
        viewModel = TeamDetailsViewModel(teamRepository, authService, teamMembershipRepository)
        every { teamRepository.getTeam(5) } returns Flowable.never()
        every { teamRepository.getTeamMemberships(5) } returns Flowable.never()
        every { teamMembershipRepository.getRequestedMembershipsForTeam(5) } returns Flowable.never()
        every { teamMembershipRepository.getConfirmedMembershipsForTeam(5) } returns Flowable.just(
                Resource(FetchStatus.SUCCESS, emptyList()))
        every { authService.userId } returns 10
        every { teamMembershipRepository.addMembershipRequest(5) } returns Single.error(IOException())
        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.onJoinTeamClicked()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.membershipStatus.test()
                .assertValuesOnly(MembershipStatus.NONE)
    }

    @Test
    fun `onJoinTeamClicked whenSucceeded emitMembershipStatusPending`() {
        viewModel = TeamDetailsViewModel(teamRepository, authService, teamMembershipRepository)
        every { teamRepository.getTeam(5) } returns Flowable.never()
        every { teamRepository.getTeamMemberships(5) } returns Flowable.never()
        every { teamMembershipRepository.getRequestedMembershipsForTeam(5) } returns Flowable.never()
        every { teamMembershipRepository.getConfirmedMembershipsForTeam(5) } returns Flowable.just(
                Resource(FetchStatus.SUCCESS, emptyList()))
        every { authService.userId } returns 1
        every { teamMembershipRepository.addMembershipRequest(5) } returns
                Single.just(TeamMembership(50, Date(), false, users[1]!!))
        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.onJoinTeamClicked()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.membershipStatus.test()
                .assertValuesOnly(MembershipStatus.PENDING)
    }

    @Test
    fun `onCancelJoinRequest whenFailed doNotEmitNewMembershipStatus`() {
        viewModel = TeamDetailsViewModel(teamRepository, authService, teamMembershipRepository)
        every { teamRepository.getTeam(5) } returns Flowable.never()
        every { teamRepository.getTeamMemberships(5) } returns Flowable.never()
        every { teamMembershipRepository.getRequestedMembershipsForTeam(5) } returns Flowable.never()
        every { teamMembershipRepository.getConfirmedMembershipsForTeam(5) } returns Flowable.just(
                Resource(FetchStatus.SUCCESS, listOf(
                        TeamMembership(7, Date(), false, users[1]!!)
                )))
        every { authService.userId } returns 1
        every { teamMembershipRepository.deleteMembership(7) } returns
                Completable.error(IOException())
        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.onCancelJoinRequestClicked()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.membershipStatus.test()
                .assertValuesOnly(MembershipStatus.PENDING)
    }

    @Test
    fun `onCancelJoinRequest whenSuceeded emitMembershipStatusNone`() {
        viewModel = TeamDetailsViewModel(teamRepository, authService, teamMembershipRepository)
        every { teamRepository.getTeam(5) } returns Flowable.never()
        every { teamRepository.getTeamMemberships(5) } returns Flowable.never()
        every { teamMembershipRepository.getRequestedMembershipsForTeam(5) } returns Flowable.never()
        every { teamMembershipRepository.getConfirmedMembershipsForTeam(5) } returns Flowable.just(
                Resource(FetchStatus.SUCCESS, listOf(
                        TeamMembership(7, Date(), false, users[1]!!)
                )))
        every { authService.userId } returns 1
        every { teamMembershipRepository.deleteMembership(7) } returns
                Completable.complete()
        viewModel.init(5)
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.onCancelJoinRequestClicked()
        testSchedulerRule.testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        viewModel.membershipStatus.test()
                .assertValuesOnly(MembershipStatus.NONE)
    }
}