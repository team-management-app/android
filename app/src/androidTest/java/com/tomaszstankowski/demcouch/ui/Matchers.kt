package com.tomaszstankowski.demcouch.ui

import android.support.design.widget.TextInputLayout
import android.view.View
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

fun hasInputLayoutErrorText(expectedErrorText: String) = object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description?) {
    }

    override fun matchesSafely(item: View?): Boolean {
        if (item !is TextInputLayout) {
            return false
        }
        val actualErrorText = item.error?.toString()
        return actualErrorText == expectedErrorText
    }
}