package com.tomaszstankowski.demcouch.ui.medium

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.DrawerActions.open
import android.support.test.espresso.contrib.DrawerMatchers.isClosed
import android.support.test.espresso.contrib.DrawerMatchers.isOpen
import android.support.test.espresso.contrib.NavigationViewActions.navigateTo
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.MediumTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.Gravity
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.main.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@MediumTest
class NavigationTest {


    @Rule
    @JvmField
    val activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    private fun pressUpButton(): ViewInteraction =
            onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click())

    @Before
    fun before() {
        onView(withId(R.id.drawerLayout))
                .check(matches(isClosed(Gravity.LEFT)))

        onView(withId(R.id.fragmentHomeConstraintLayout))
                .check(matches(isDisplayed()))
    }

    @Test
    fun navigation_whenDrawerIsOpenedNavigateToDestination_drawerShouldClose() {
        onView(withId(R.id.drawerLayout))
                .perform(open())
                .check(matches(isOpen(Gravity.LEFT)))

        onView(withId(R.id.navigationView))
                .perform(navigateTo(R.id.my_teams_graph))

        onView(withId(R.id.drawerLayout))
                .check(matches(isClosed(Gravity.LEFT)))
    }

    @Test
    fun navigation_pressUpButtonFromStartDestination_drawerShouldOpen() {
        pressUpButton()

        onView(withId(R.id.drawerLayout))
                .check(matches(isOpen(Gravity.LEFT)))
    }

    @Test
    fun navigation_pressUpButtonFromDrawerDestination_drawerShouldOpen() {
        pressUpButton()

        onView(withId(R.id.navigationView))
                .perform(navigateTo(R.id.my_teams_graph))

        pressUpButton()

        onView(withId(R.id.drawerLayout))
                .check(matches(isOpen(Gravity.LEFT)))
    }

    @Test
    fun navigation_pressBackFromChildDestination_parentDestinationShouldBeDisplayed() {
        pressUpButton()

        onView(withId(R.id.navigationView))
                .perform(navigateTo(R.id.my_teams_graph))

        onView(withId(R.id.fragmentMyTeamsFloatingActionButton))
                .perform(click())

        pressBack()

        onView(withId(R.id.drawerLayout))
                .check(matches(isClosed()))

        onView(withId(R.id.fragmentMyTeamsCoordinatorLayout))
                .check(matches(isDisplayed()))
    }


    @Test
    fun navigation_navigateToDestinationFromStartDestination_destinationShouldBeDisplayed() {
        onView(withId(R.id.drawerLayout))
                .perform(open())

        onView(withId(R.id.navigationView))
                .perform(navigateTo(R.id.my_teams_graph))

        onView(withId(R.id.fragmentMyTeamsCoordinatorLayout))
                .check(matches(isDisplayed()))
    }

    @Test
    fun navigation_navigateToDestinationThenNavigateToStartDestination_startDestinationShouldBeDisplayed() {
        onView(withId(R.id.drawerLayout))
                .perform(open())

        onView(withId(R.id.navigationView))
                .perform(navigateTo(R.id.my_teams_graph))

        onView(withId(R.id.drawerLayout))
                .perform(open())

        onView(withId(R.id.navigationView))
                .perform(navigateTo(R.id.homeFragment))

        onView(withId(R.id.fragmentHomeConstraintLayout))
                .check(matches(isDisplayed()))
    }

    @Test
    fun navigation_navigateToDestinationThenPressBack_startDestinationShouldBeDisplayed() {
        onView(withId(R.id.drawerLayout))
                .perform(open())

        onView(withId(R.id.navigationView))
                .perform(navigateTo(R.id.my_teams_graph))

        pressBack()

        onView(withId(R.id.fragmentHomeConstraintLayout))
                .check(matches(isDisplayed()))
    }

    @Test
    fun navigation_navigateToCurrentDestination_currentDestinationShouldBeDisplayedAndDrawerShouldClose() {
        onView(withId(R.id.drawerLayout))
                .perform(open())

        onView(withId(R.id.navigationView))
                .perform(navigateTo(R.id.homeFragment))

        onView(withId(R.id.fragmentHomeConstraintLayout))
                .check(matches(isDisplayed()))

        onView(withId(R.id.drawerLayout))
                .check(matches(isClosed(Gravity.LEFT)))
    }

}

