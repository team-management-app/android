package com.tomaszstankowski.demcouch.ui.medium

import android.content.pm.ActivityInfo
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.MediumTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.createteam.CreateTeamActivity
import com.tomaszstankowski.demcouch.ui.hasInputLayoutErrorText
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class StepFragmentTest {

    companion object {
        private const val SLEEP_MILLIS = 300L
    }

    @Rule
    @JvmField
    val activityRule: ActivityTestRule<CreateTeamActivity> = ActivityTestRule(CreateTeamActivity::class.java)

    @Test
    fun stepFragment_whenNoInputChangesMade_nextButtonDisabled() {
        onView(withId(R.id.activityCreateTeamNextButton))
                .check(matches(not(isEnabled())))
    }

    @Test
    fun stepFragment_whenInvalidInput_nextButtonDisabledErrorDisplayed() {
        val name = "t".repeat(31)

        onView(withId(R.id.fragmentChooseNameEditText))
                .perform(typeText(name))
        Thread.sleep(SLEEP_MILLIS)

        onView(withId(R.id.fragmentChooseNameTextInputLayout))
                .check(matches(hasInputLayoutErrorText(activityRule.activity.getString(R.string.validation_error_max_length, 30))))
        onView(withId(R.id.activityCreateTeamNextButton))
                .check(matches(not(isEnabled())))
    }

    @Test
    fun stepFragment_whenCorrectInputAndOrientationChanged_nextButtonEnabledAndInputRemain() {
        val name = "Very Good Team Name"
        onView(withId(R.id.fragmentChooseNameEditText))
                .perform(typeText(name))
        Thread.sleep(SLEEP_MILLIS)

        activityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        onView(withId(R.id.activityCreateTeamNextButton))
                .check(matches(isEnabled()))
        onView(withId(R.id.fragmentChooseNameEditText))
                .check(matches(withText(name)))
    }

    @Test
    fun stepFragment_whenIncorrectInputAndOrientationChanged_nextButtonDisabledAndInputAndErrorRemain() {
        val name = "Short"
        onView(withId(R.id.fragmentChooseNameEditText))
                .perform(typeText(name))
        Thread.sleep(SLEEP_MILLIS)

        activityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        Thread.sleep(SLEEP_MILLIS)
        Thread.sleep(SLEEP_MILLIS)

        onView(withId(R.id.fragmentChooseNameEditText))
                .check(matches(withText(name)))
        onView(withId(R.id.fragmentChooseNameTextInputLayout))
                .check(matches(hasInputLayoutErrorText(activityRule.activity.getString(R.string.validation_error_min_length, 6))))
        onView(withId(R.id.activityCreateTeamNextButton))
                .check(matches(not(isEnabled())))

    }

    @Test
    fun stepFragment_whenCameBackToSameStep_nextButtonEnabledAndInputRemains() {
        val name = "Good name"
        onView(withId(R.id.fragmentChooseNameEditText))
                .perform(typeText(name))
        Thread.sleep(SLEEP_MILLIS)
        onView(withId(R.id.activityCreateTeamNextButton))
                .perform(click())
        onView(withId(R.id.fragmentChooseDescriptionTitleTV))
                .check(matches(isDisplayed()))

        onView(withId(R.id.activityCreateTeamBackButton))
                .perform(click())

        onView(withId(R.id.fragmentChooseNameEditText))
                .check(matches(withText(name)))
        onView(withId(R.id.activityCreateTeamNextButton))
                .check(matches(isEnabled()))
    }
}