package com.tomaszstankowski.demcouch.ui.medium

import android.app.Activity
import android.app.Instrumentation.ActivityResult
import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers.hasAction
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.MediumTest
import android.support.test.runner.AndroidJUnit4
import com.android21buttons.fragmenttestrule.FragmentTestRule
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.createteam.CreateTeamActivity
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosethumbnail.ChooseThumbnailFragment
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@MediumTest
class ChooseThumbnailFragmentTest {

    @Rule
    @JvmField
    val intentsTestRule: IntentsTestRule<CreateTeamActivity> = IntentsTestRule(CreateTeamActivity::class.java)

    @Rule
    @JvmField
    var fragmentTestRule: FragmentTestRule<*, ChooseThumbnailFragment> = FragmentTestRule.create(ChooseThumbnailFragment::class.java)

    private fun getMockUri(): Uri {
        val resourceId = R.drawable.ic_add_white_24dp
        val resources = fragmentTestRule.activity.resources
        return Uri.Builder()
                .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                .authority(resources.getResourcePackageName(resourceId))
                .appendPath(resources.getResourceTypeName(resourceId))
                .appendPath(resources.getResourceEntryName(resourceId))
                .build()
    }

    @Test
    fun chooseThumbnail_whenThumbnailSelected_imageDisplayed() {
        val intent = Intent()
        val uri = getMockUri()
        intent.data = uri
        val result = ActivityResult(Activity.RESULT_OK, intent)
        intending(hasAction(Intent.ACTION_OPEN_DOCUMENT)).respondWith(result)

        onView(withId(R.id.fragmentChooseThumbnailBrowseButton))
                .perform(click())

        onView(withId(R.id.fragmentChooseThumbnailImageView))
                .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.fragmentChooseThumbnailDeleteIcon))
                .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    @Test
    fun chooseThumbnail_whenThumbnailRemoved_imageHides() {
        val intent = Intent()
        val uri = getMockUri()
        intent.data = uri
        val result = ActivityResult(Activity.RESULT_OK, intent)
        intending(hasAction(Intent.ACTION_OPEN_DOCUMENT)).respondWith(result)

        onView(withId(R.id.fragmentChooseThumbnailBrowseButton))
                .perform(click())
        onView(withId(R.id.fragmentChooseThumbnailDeleteIcon))
                .perform(click())

        onView(withId(R.id.fragmentChooseThumbnailImageView))
                .check(matches(withEffectiveVisibility(Visibility.INVISIBLE)))
        onView(withId(R.id.fragmentChooseThumbnailDeleteIcon))
                .check(matches(withEffectiveVisibility(Visibility.GONE)))
    }
}