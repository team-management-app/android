package com.tomaszstankowski.demcouch.ui.large

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.rule.GrantPermissionRule
import android.support.test.runner.AndroidJUnit4
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.createteam.CreateTeamActivity
import junit.framework.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class CreateTeamActivityTest {

    @Rule
    @JvmField
    val activityRule: ActivityTestRule<CreateTeamActivity> = ActivityTestRule(CreateTeamActivity::class.java)

    @Rule
    @JvmField
    val grantPermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION)

    companion object {
        private const val SLEEP_MILLIS = 300L
    }

    @Test
    fun createTeam_whenAllStepsMade_closeActivity() {
        onView(withId(R.id.fragmentChooseNameEditText))
                .perform(typeText("Kaszubia"))
        Thread.sleep(SLEEP_MILLIS)
        onView(withId(R.id.activityCreateTeamNextButton))
                .perform(click())

        onView(withId(R.id.fragmentChooseDescriptionET))
                .perform(typeText("Grajcy"))
        Thread.sleep(SLEEP_MILLIS)
        onView(withId(R.id.activityCreateTeamNextButton))
                .perform(click())

        onView(withId(R.id.fragmentChooseDisciplineHandballRadio))
                .perform(click())
        onView(withId(R.id.activityCreateTeamNextButton))
                .perform(click())

        onView(withId(R.id.fragmentChooseLocationCurrentLocationButton))
                .perform(click())
        Thread.sleep(SLEEP_MILLIS)
        onView(withId(R.id.activityCreateTeamNextButton))
                .perform(click())

        onView(withId(R.id.activityCreateTeamNextButton))
                .perform(click())

        Thread.sleep(1000)
        assertTrue(activityRule.activity.isFinishing)
    }
}