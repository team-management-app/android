package com.tomaszstankowski.demcouch.main.domain.notification

import com.tomaszstankowski.demcouch.main.domain.Team
import org.threeten.bp.ZonedDateTime

data class TeamMembershipApprovalNotification(override val id: Long,
                                              override val createDate: ZonedDateTime,
                                              override val markedAsRead: Boolean,
                                              val team: Team) : Notification