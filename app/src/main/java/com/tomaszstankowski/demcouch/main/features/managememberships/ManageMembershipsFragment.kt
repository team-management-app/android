package com.tomaszstankowski.demcouch.main.features.managememberships

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.setVisibleOrElseGone
import kotlinx.android.synthetic.main.fragment_manage_memberships.*
import javax.inject.Inject

class ManageMembershipsFragment : DemCouchFragment(),
        ManageMembershipsListAdapter.OnMembershipDeleteClickListener {

    companion object {
        const val TEAM_ID_KEY = "teamId"
    }

    @Inject
    lateinit var viewModel: ManageMembershipsViewModel

    private lateinit var adapter: ManageMembershipsListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_manage_memberships, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val teamId = arguments?.getLong(TEAM_ID_KEY)
                ?: throw IllegalStateException("No '$TEAM_ID_KEY' param passed to fragment")

        viewModel = getViewModel()
        viewModel.init(teamId)

        adapter = ManageMembershipsListAdapter(activity!!)
        adapter.onMembershipDeleteClickListener = this
        fragmentManageMembershipsRecyclerView.let {
            it.adapter = adapter
            it.layoutManager = LinearLayoutManager(activity!!)
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.memberships, adapter::items::set)
        subscribe(viewModel.deleteMembership.loading,
                fragmentManageMembershipsProgressBar::setVisibleOrElseGone)
        subscribe(viewModel.deleteMembership.failure, {
            showMessage(R.string.error)
        })
    }

    override fun onMembershipDeleteClicked(membership: TeamMembership) {
        AlertDialog.Builder(context!!)
                .setTitle(R.string.manage_memberships_fragment_delete_dialog)
                .setPositiveButton(R.string.yes) { _, _ -> viewModel.onDeleteMembershipClicked(membership) }
                .setNegativeButton(R.string.cancel) { dialog: DialogInterface, _ -> dialog.dismiss() }
                .create()
                .show()
    }
}