package com.tomaszstankowski.demcouch.main.features.notifications.list

import android.content.Context
import android.view.View
import com.tomaszstankowski.demcouch.main.domain.notification.InfoNotification
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.ui.setVisibleOrElseGone

class InfoViewHolder(val context: Context, view: View) : NotificationViewHolder(view) {

    override fun bind(item: Notification) {
        val notification = item as InfoNotification
        thumbnail.setVisibleOrElseGone(false)
        text.text = notification.message
        super.bind(item)
    }
}