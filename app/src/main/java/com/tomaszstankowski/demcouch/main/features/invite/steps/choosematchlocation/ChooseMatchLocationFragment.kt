package com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_choose_match_location.*
import timber.log.Timber

class ChooseMatchLocationFragment : DemCouchFragment(), OnMapReadyCallback {

    companion object {
        val instance: ChooseMatchLocationFragment
            get() = ChooseMatchLocationFragment()
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 999
    }

    private lateinit var viewModel: ChooseMatchLocationViewModel

    private var map: GoogleMap? = null
    private var marker: Marker? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_choose_match_location, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()

        (childFragmentManager
                .findFragmentById(R.id.fragmentChooseMatchLocationMap) as SupportMapFragment)
                .getMapAsync(this)
        fragmentChooseMatchLocationCurrentLocationButton.setOnClickListener {
            if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                viewModel.onGetCurrentLocationClicked()
            } else {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_PERMISSIONS_REQUEST_CODE)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.location, {
            val latLng = LatLng(it.lat, it.lng)
            marker?.position = latLng
            map?.animateCamera(CameraUpdateFactory.newLatLng(latLng))
            (activity as OnLocationSelectedListener?)?.onLocationSelected(it)
        }, onError = {
            Timber.e(it, "Something went wrong!!!")
        })
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map
        val defaultPosition = LatLng(54.3742012, 18.6073264)
        val markerOptions = MarkerOptions()
                .position(defaultPosition)
                .draggable(true)
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultPosition, 10f))
        this.marker = map?.addMarker(markerOptions)
        map?.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {

            override fun onMarkerDragEnd(marker: Marker?) {
                if (marker != null) {
                    val coordinates = Coordinates(
                            marker.position.latitude,
                            marker.position.longitude)
                    viewModel.onLocationChangedByDraggingMarker(coordinates)
                }
            }

            override fun onMarkerDragStart(marker: Marker?) {
            }

            override fun onMarkerDrag(marker: Marker?) {
            }

        })
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSIONS_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    viewModel.onGetCurrentLocationClicked()
                }
            }
        }
    }
}