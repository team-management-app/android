package com.tomaszstankowski.demcouch.main.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.toDisplayString

class TeamHorizontalListViewHolder(view: View, private val context: Context) : RecyclerView.ViewHolder(view) {
    val name: TextView = view.findViewById(R.id.teamHorizontalListItemName)
    val discipline: TextView = view.findViewById(R.id.teamHorizontalListItemDiscipline)
    val thumbnail: ImageView = view.findViewById(R.id.teamHorizontalListItemThumbnail)

    fun bindItem(item: Team) {
        name.text = item.name
        discipline.text = item.discipline.toDisplayString(context)
        thumbnail.loadThumbnailFromUrl(item.thumbnailUrl)
    }
}