package com.tomaszstankowski.demcouch.main.features.createteam.steps.choosethumbnail

import android.net.Uri
import com.tomaszstankowski.demcouch.main.ui.Schedulers
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.ThumbnailValidator
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ChooseThumbnailViewModel
@Inject constructor(private val validator: ThumbnailValidator) : DemCouchViewModel() {

    private val _thumbnail = BehaviorSubject.create<Uri>()

    private val _thumbnailSelection = BehaviorSubject.create<ThumbnailSelection>()
    val thumbnailSelection: Flowable<ThumbnailSelection>
        get() = _thumbnailSelection.toFlowable(BackpressureStrategy.LATEST)

    val thumbnailSnapshot: Uri?
        get() = _thumbnailSelection.value?.thumbnail

    init {
        val disposable = _thumbnail
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .switchMapSingle { uri ->
                    validator.validate(uri)
                            .map {
                                if (it.isValid) {
                                    ThumbnailSelection.accepted(uri)
                                } else {
                                    ThumbnailSelection.rejected(uri, it)
                                }
                            }
                }
                .toFlowable(BackpressureStrategy.LATEST)
                .onErrorReturnItem(ThumbnailSelection.empty())
                .subscribe {
                    _thumbnailSelection.onNext(it)
                }
        compositeDisposable.add(disposable)
    }

    fun onThumbnailSelected(uri: Uri) {
        _thumbnail.onNext(uri)
    }

    fun onThumbnailRemoveClicked() {
        _thumbnailSelection.onNext(ThumbnailSelection.empty())
    }
}

class ThumbnailSelection private constructor(val thumbnail: Uri?, val status: FieldStatus?) {
    companion object {
        fun accepted(thumbnail: Uri) =
                ThumbnailSelection(thumbnail, FieldStatus.correct())

        fun rejected(thumbnail: Uri, status: FieldStatus) =
                ThumbnailSelection(thumbnail, status)

        fun empty() =
                ThumbnailSelection(null, null)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ThumbnailSelection

        if (thumbnail != other.thumbnail) return false
        if (status != other.status) return false

        return true
    }

    override fun hashCode(): Int {
        var result = thumbnail?.hashCode() ?: 0
        result = 31 * result + (status?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "ThumbnailSelection(thumbnail=$thumbnail, status=$status)"
    }


}