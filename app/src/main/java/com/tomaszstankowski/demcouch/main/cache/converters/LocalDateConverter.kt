package com.tomaszstankowski.demcouch.main.cache.converters

import android.arch.persistence.room.TypeConverter
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

class LocalDateConverter {
    private val formatter = DateTimeFormatter.ISO_LOCAL_DATE

    @TypeConverter
    @Synchronized
    fun toDbColumn(attr: LocalDate?): String? = attr?.format(formatter)

    @TypeConverter
    @Synchronized
    fun toEntityAttribute(column: String?): LocalDate? {
        if (column == null) {
            return null
        }
        return LocalDate.parse(column, formatter)
    }
}