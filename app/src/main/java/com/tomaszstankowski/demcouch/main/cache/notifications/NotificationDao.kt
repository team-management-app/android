package com.tomaszstankowski.demcouch.main.cache.notifications

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface NotificationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(notification: NotificationCache)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(notifications: List<NotificationCache>)

    @Query("SELECT * FROM notifications WHERE id = :id")
    fun findById(id: Long): Flowable<NotificationCache>

    @Query("SELECT * FROM notifications " +
            "WHERE user_id = :userId ORDER BY create_date DESC")
    fun getUserNotifications(userId: Long): Flowable<List<NotificationCache>>

    @Query("SELECT COUNT(*) FROM notifications " +
            "WHERE user_id = :userId AND marked_as_read = 0 ORDER BY create_date DESC")
    fun getUnreadNotificationCountForUser(userId: Long): Flowable<Int>
}