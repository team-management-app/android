package com.tomaszstankowski.demcouch.main.di

import com.tomaszstankowski.demcouch.main.repository.Clock
import com.tomaszstankowski.demcouch.main.repository.ResourceFetchMemory
import com.tomaszstankowski.demcouch.main.repository.notification.NotificationRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun provideNotificationRepositoryConfig() = NotificationRepository.Config(10)

    @Provides
    fun provideResourceFetchMemoryConfig() = ResourceFetchMemory.Config()

    @Provides
    fun provideClock(): Clock = object : Clock {
        override val currentTimeMs: Long
            get() = System.currentTimeMillis()
    }

}