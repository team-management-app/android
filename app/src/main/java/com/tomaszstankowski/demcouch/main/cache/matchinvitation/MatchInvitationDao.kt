package com.tomaszstankowski.demcouch.main.cache.matchinvitation

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface MatchInvitationDao {

    @Query("SELECT * FROM match_invitation WHERE id =:id")
    fun findById(id: Long): Flowable<MatchInvitationCache>

    @Query("SELECT * FROM match_invitation WHERE invited_team_id = :teamId")
    fun findByInvitedTeam(teamId: Long): Flowable<List<MatchInvitationCache>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(matchInvitationCache: MatchInvitationCache)

    @Query("DELETE FROM match_invitation WHERE id= :id")
    fun delete(id: Long)
}