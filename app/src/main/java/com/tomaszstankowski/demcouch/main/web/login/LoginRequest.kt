package com.tomaszstankowski.demcouch.main.web.login

data class LoginRequest(val email: String, val password: String)