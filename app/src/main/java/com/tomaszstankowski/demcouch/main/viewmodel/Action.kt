package com.tomaszstankowski.demcouch.main.viewmodel

import com.tomaszstankowski.demcouch.main.ui.Schedulers
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class Action {

    private val _failure = PublishSubject.create<Throwable>()
    val failure: Flowable<Throwable>
        get() = _failure.toFlowable(BackpressureStrategy.LATEST)

    private val _success = BehaviorSubject.create<Any>()
    val success: Flowable<Any>
        get() = _success.toFlowable(BackpressureStrategy.LATEST)

    private val _loading: Subject<Boolean> = BehaviorSubject.create()
    val loading: Flowable<Boolean>
        get() = _loading.toFlowable(BackpressureStrategy.LATEST)

    fun <T> invoke(single: Single<T>): Single<T> {
        return single
                .doOnSubscribe { _loading.onNext(true) }
                .doOnError { _failure.onNext(it) }
                .doOnSuccess { _success.onNext(Any()) }
                .doFinally { _loading.onNext(false) }
                .subscribeOn(Schedulers.backgroundThreadScheduler())
    }

    fun invoke(completable: Completable): Completable {
        return completable
                .doOnSubscribe { _loading.onNext(true) }
                .doOnError { _failure.onNext(it) }
                .doOnComplete { _success.onNext(Any()) }
                .doFinally { _loading.onNext(false) }
                .subscribeOn(Schedulers.backgroundThreadScheduler())
    }
}