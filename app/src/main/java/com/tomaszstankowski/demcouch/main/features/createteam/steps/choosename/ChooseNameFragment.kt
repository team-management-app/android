package com.tomaszstankowski.demcouch.main.features.createteam.steps.choosename

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.createteam.steps.StepFragment
import com.tomaszstankowski.demcouch.main.ui.addOnTextChangeListener
import com.tomaszstankowski.demcouch.main.ui.changeStatus
import com.tomaszstankowski.demcouch.main.ui.resetStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import kotlinx.android.synthetic.main.fragment_choose_name.*

class ChooseNameFragment : StepFragment() {

    private lateinit var viewModel: ChooseNameViewModel

    companion object {
        fun getInstance(name: String?): ChooseNameFragment {
            val args = Bundle()
            args.putString(NAME_KEY, name)
            val fragment = ChooseNameFragment()
            fragment.arguments = args
            return fragment
        }

        private const val NAME_KEY = "name"
    }

    val name: String?
        get() = fragmentChooseNameTextInputLayout.editText?.text?.toString()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_choose_name, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState == null) {
            val name = arguments?.getString(NAME_KEY)
            if (name != null) {
                fragmentChooseNameTextInputLayout.changeStatus(FieldStatus.correct())
                setValid(true)
            }
        }
        viewModel = getViewModel()

        fragmentChooseNameTextInputLayout.addOnTextChangeListener { text, view ->
            viewModel.name.postValue(text)
            view.resetStatus()
            setValid(true)
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.name.status, {
            setValid(it.isValid)
            fragmentChooseNameTextInputLayout.changeStatus(it)
        })
    }

}