package com.tomaszstankowski.demcouch.main.features.editteam

import android.app.Dialog
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Button
import com.tomaszstankowski.demcouch.R

class EditLocationDialog : BottomSheetDialogFragment() {

    companion object {
        const val RESULT_CODE_FIND = 1
        const val RESULT_CODE_USE_CURRENT = 2
    }

    override fun setupDialog(dialog: Dialog?, style: Int) {
        val contentView = View.inflate(context, R.layout.edit_location_dialog, null)
        contentView.findViewById<Button>(R.id.findLocationButton)
                .setOnClickListener { setResultCode(RESULT_CODE_FIND) }
        contentView.findViewById<Button>(R.id.useCurrentLocationButton)
                .setOnClickListener { setResultCode(RESULT_CODE_USE_CURRENT) }
        dialog?.setContentView(contentView)
        (contentView.parent as View).setBackgroundColor(
                ContextCompat.getColor(context!!, android.R.color.transparent))
    }

    private fun setResultCode(resultCode: Int) {
        targetFragment?.onActivityResult(targetRequestCode, resultCode, null)
        dismiss()
    }
}