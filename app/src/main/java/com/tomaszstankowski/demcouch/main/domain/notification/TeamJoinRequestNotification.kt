package com.tomaszstankowski.demcouch.main.domain.notification

import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import com.tomaszstankowski.demcouch.main.web.user.UserResponse
import org.threeten.bp.ZonedDateTime

data class
TeamJoinRequestNotification(override val id: Long,
                            override val createDate: ZonedDateTime,
                            override val markedAsRead: Boolean,
                            val requestingUser: UserResponse,
                            val team: TeamResponse) : Notification