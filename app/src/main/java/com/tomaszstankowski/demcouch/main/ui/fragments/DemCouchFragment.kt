package com.tomaszstankowski.demcouch.main.ui.fragments

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.widget.Toast
import com.tomaszstankowski.demcouch.main.ui.Schedulers
import dagger.android.support.DaggerFragment
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class DemCouchFragment : DaggerFragment() {

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    protected val compositeDisposable = CompositeDisposable()

    fun <T> subscribe(flowable: Flowable<T>,
                      onNext: (data: T) -> Unit = {},
                      onError: (error: Throwable) -> Unit = {},
                      onComplete: () -> Unit = {},
                      subscribeOn: Scheduler = Schedulers.backgroundThreadScheduler(),
                      observeOn: Scheduler = Schedulers.mainThreadScheduler()) {
        val disposable = flowable.subscribeOn(subscribeOn)
                .observeOn(observeOn)
                .subscribe(onNext, onError, onComplete)
        compositeDisposable.add(disposable)
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }

    fun showMessage(stringId: Int, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(activity!!, stringId, duration).show()
    }

    protected inline fun <reified T : ViewModel> getViewModel(): T {
        return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
    }
}