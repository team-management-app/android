package com.tomaszstankowski.demcouch.main.web.matchinvitation

import io.reactivex.Single
import retrofit2.http.*

interface MatchInvitationService {

    @GET("/match-invitations/{id}")
    fun getMatchInvitation(@Path("id") id: String): Single<MatchInvitationResponse>

    @GET("/teams/{id}/match-invitations")
    fun getMatchInvitationsForTeam(@Path("id") id: String): Single<List<MatchInvitationResponse>>

    @POST("/match-invitations")
    fun createMatchInvitation(@Body body: MatchInvitationCreateRequestBody): Single<MatchInvitationResponse>

    @PUT("/match-invitations/{id}/status")
    fun updateInvitationStatus(@Path("id") id: String,
                               @Body body: MatchInvitationStatusUpdateRequestBody)
            : Single<MatchInvitationStatusUpdateResponse>

}