package com.tomaszstankowski.demcouch.main.features.teamdetails

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.minibugdev.drawablebadge.BadgePosition
import com.minibugdev.drawablebadge.DrawableBadge
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.features.editteam.EditTeamFragment
import com.tomaszstankowski.demcouch.main.features.invite.InviteActivity
import com.tomaszstankowski.demcouch.main.features.joinrequestlist.JoinRequestListFragment
import com.tomaszstankowski.demcouch.main.features.managememberships.ManageMembershipsFragment
import com.tomaszstankowski.demcouch.main.features.matchinvitation.list.MatchInvitationListFragment
import com.tomaszstankowski.demcouch.main.features.profile.ProfileFragment
import com.tomaszstankowski.demcouch.main.features.selectthumbnail.SelectThumbnailActivity
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.ui.adapters.UserHorizontalListAdapter
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.setVisibleOrElseGone
import com.tomaszstankowski.demcouch.main.ui.toDisplayString
import kotlinx.android.synthetic.main.fragment_team_details.*

class TeamDetailsFragment : DemCouchFragment() {

    private lateinit var viewModel: TeamDetailsViewModel
    private lateinit var adapter: UserHorizontalListAdapter

    private lateinit var joinRequestsDrawableBadge: DrawableBadge

    companion object {
        const val ID_KEY = "id"
        private const val CHOOSE_THUMBNAIL_REQUEST_CODE = 456
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 123
    }

    private fun showShimmerLayout() {
        fragmentTeamDetailsContentLayout.visibility = View.GONE
        fragmentTeamDetailsShimmerFrameLayout.visibility = View.VISIBLE
        fragmentTeamDetailsShimmerFrameLayout.startShimmer()
    }

    private fun showContentLayout() {
        fragmentTeamDetailsContentLayout.visibility = View.VISIBLE
        fragmentTeamDetailsShimmerFrameLayout.visibility = View.GONE
        fragmentTeamDetailsShimmerFrameLayout.stopShimmer()
    }

    private fun showErrorLayout() {
        fragmentTeamDetailsContentLayout.visibility = View.GONE
        fragmentTeamDetailsShimmerFrameLayout.visibility = View.GONE
        fragmentTeamDetailsShimmerFrameLayout.stopShimmer()
        fragmentTeamDetailsErrorLayout.visibility = View.VISIBLE
    }

    private fun displayTeam(team: Team) {
        team.apply {
            fragmentTeamDetailsTeamNameTv.text = name
            fragmentTeamDetailsDescriptionTV.text = desc
            fragmentTeamDetailsDisciplineTv.text = discipline.toDisplayString(activity!!)
            fragmentTeamDetailsLocationTv.text = location.name
            if (thumbnailUrl == null) {
                fragmentTeamDetailsRemoveIv.visibility = View.GONE
            } else if (viewModel.membershipStatusSnapshot == MembershipStatus.LEADER) {
                fragmentTeamDetailsRemoveIv.visibility = View.VISIBLE
            }
            fragmentTeamDetailsThumbnailIv.loadThumbnailFromUrl(thumbnailUrl)
        }
    }

    private fun onUserClicked(user: User) {
        val args = Bundle()
        args.putLong(ProfileFragment.ID_KEY, user.id)
        Navigation.findNavController(activity!!, R.id.navHostFragment)
                .navigate(R.id.action_teamDetailsFragment_to_profileFragment, args)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_team_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        showShimmerLayout()

        viewModel = getViewModel()
        val teamId = arguments?.getLong(ID_KEY)
                ?: throw IllegalStateException("Param '$ID_KEY' not passed to fragment")
        viewModel.init(teamId)

        adapter = UserHorizontalListAdapter(activity!!)
        val layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, false)
        fragmentTeamDetailsMembersRecyclerView.adapter = adapter
        fragmentTeamDetailsMembersRecyclerView.layoutManager = layoutManager
        adapter.onItemClickListener = { item, _ -> onUserClicked(item) }

        fragmentTeamDetailsSettingsIv.setOnClickListener {
            val args = Bundle()
            args.putLong(EditTeamFragment.ID_KEY, teamId)
            Navigation.findNavController(activity!!, R.id.navHostFragment)
                    .navigate(R.id.action_teamDetailsFragment_to_editTeamFragment, args)
        }
        fragmentTeamDetailsRemoveIv.setOnClickListener {
            viewModel.onThumbnailRemoveClicked()
        }

        fragmentTeamDetailsEditIv.setOnClickListener {
            if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(context!!, SelectThumbnailActivity::class.java)
                startActivityForResult(intent, CHOOSE_THUMBNAIL_REQUEST_CODE)
            } else {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        REQUEST_PERMISSIONS_REQUEST_CODE)
            }
        }

        fragmentTeamDetailsJoinButton.setOnClickListener {
            viewModel.onJoinTeamClicked()
        }
        fragmentTeamDetailsCancelJoinButton.setOnClickListener {
            viewModel.onCancelJoinRequestClicked()
        }
        fragmentTeamDetailsLeaveButton.setOnClickListener {
            AlertDialog.Builder(context!!)
                    .setTitle(R.string.team_details_fragment_leave_dialog)
                    .setPositiveButton(R.string.yes) { _, _ -> viewModel.onLeaveTeamClicked() }
                    .setNegativeButton(R.string.cancel) { dialog: DialogInterface, _ -> dialog.dismiss() }
                    .create()
                    .show()
        }
        fragmentTeamDetailsManageMembershipsIcon.setOnClickListener {
            val args = Bundle()
            args.putLong(ManageMembershipsFragment.TEAM_ID_KEY, teamId)
            Navigation.findNavController(it)
                    .navigate(R.id.action_teamDetailsFragment_to_manageMembershipsFragment, args)
        }

        joinRequestsDrawableBadge = DrawableBadge.Builder(activity!!)
                .drawableResId(R.drawable.ic_person_black_24dp)
                .badgeColor(R.color.colorRed)
                .badgePosition(BadgePosition.BOTTOM_RIGHT)
                .build()
        fragmentTeamDetailsJoinRequestsButton.setOnClickListener {
            val args = Bundle()
            args.putLong(JoinRequestListFragment.TEAM_ID_KEY, teamId)
            Navigation.findNavController(it)
                    .navigate(R.id.action_teamDetailsFragment_to_joinRequestListFragment, args)
        }
        fragmentTeamDetailsInviteButton.setOnClickListener {
            val args = Bundle()
            args.putLong(InviteActivity.INVITED_TEAM_ID_KEY, teamId)
            args.putString(InviteActivity.INVITED_TEAM_DISCIPLINE_KEY, viewModel.teamSnapshot!!.discipline.toString())
            Navigation.findNavController(it)
                    .navigate(R.id.action_teamDetailsFragment_to_inviteActivity, args)
        }
        fragmentTeamDetailsMatchInvitationListButton.setOnClickListener {
            val args = Bundle()
            args.putLong(MatchInvitationListFragment.TEAM_ID_KEY, teamId)
            Navigation.findNavController(it)
                    .navigate(R.id.action_teamDetailsFragment_to_matchInvitationListFragment, args)
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.team, {
            if (it.data == null) {
                if (it.status == FetchStatus.LOADING) {
                    showShimmerLayout()
                } else {
                    showErrorLayout()
                }
            } else {
                showContentLayout()
                displayTeam(it.data)
            }
        })
        subscribe(viewModel.members, adapter::items::set)

        subscribe(viewModel.error, {
            showMessage(R.string.error)
        })

        subscribe(viewModel.membershipStatus, {
            fragmentTeamDetailsJoinButton.setVisibleOrElseGone(it == MembershipStatus.NONE)
            fragmentTeamDetailsCancelJoinButton.setVisibleOrElseGone(it == MembershipStatus.PENDING)
            fragmentTeamDetailsLeaveButton.setVisibleOrElseGone(it == MembershipStatus.PLAYER)
            fragmentTeamDetailsInviteButton.setVisibleOrElseGone(it == MembershipStatus.NONE || it == MembershipStatus.PENDING)
            val isLeader = it == MembershipStatus.LEADER
            fragmentTeamDetailsSettingsIv.setVisibleOrElseGone(isLeader)
            fragmentTeamDetailsEditIv.setVisibleOrElseGone(isLeader)
            fragmentTeamDetailsRemoveIv.setVisibleOrElseGone(isLeader)
            fragmentTeamDetailsManageMembershipsIcon.setVisibleOrElseGone(isLeader)

        })
        subscribe(viewModel.joinTeam.loading, {
            fragmentTeamDetailsJoinButton.isEnabled = !it
        })
        subscribe(viewModel.cancelJoinRequest.loading, {
            fragmentTeamDetailsCancelJoinButton.isEnabled = !it
        })
        subscribe(viewModel.leaveTeam.loading, {
            fragmentTeamDetailsLeaveButton.isEnabled = !it
        })
        subscribe(viewModel.joinTeam.failure, {
            showMessage(R.string.error)
        })
        subscribe(viewModel.cancelJoinRequest.failure, {
            showMessage(R.string.error)
        })
        subscribe(viewModel.leaveTeam.failure, {
            showMessage(R.string.error)
        })
        subscribe(viewModel.joinRequestCount, {
            if (it > 0 && viewModel.membershipStatusSnapshot == MembershipStatus.LEADER) {
                val drawable = joinRequestsDrawableBadge.get(it)
                fragmentTeamDetailsJoinRequestsButton.setImageDrawable(drawable)
                fragmentTeamDetailsJoinRequestsButton.visibility = View.VISIBLE
            } else {
                fragmentTeamDetailsJoinRequestsButton.visibility = GONE
            }
        })
        subscribe(viewModel.matchInvitationsCount, {
            val shouldButtonBeVisible = it > 0 && viewModel.membershipStatusSnapshot == MembershipStatus.LEADER
            fragmentTeamDetailsMatchInvitationListButton.setVisibleOrElseGone(shouldButtonBeVisible)
            val text = context!!.getString(R.string.team_details_fragment_match_invitation_list_button, it)
            fragmentTeamDetailsMatchInvitationListButton.text = text
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        if (requestCode == CHOOSE_THUMBNAIL_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val uri = resultData?.data
            if (uri != null) {
                viewModel.onThumbnailSelected(uri)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSIONS_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    val intent = Intent(context!!, SelectThumbnailActivity::class.java)
                    startActivityForResult(intent, CHOOSE_THUMBNAIL_REQUEST_CODE)
                }
            }
        }
    }
}