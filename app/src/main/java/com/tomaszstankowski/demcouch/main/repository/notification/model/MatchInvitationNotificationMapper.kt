package com.tomaszstankowski.demcouch.main.repository.notification.model

import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.domain.notification.MatchInvitationNotification
import com.tomaszstankowski.demcouch.main.web.matchinvitation.MatchInvitationMapper
import com.tomaszstankowski.demcouch.main.web.matchinvitation.MatchInvitationResponse
import javax.inject.Inject

class MatchInvitationNotificationMapper
@Inject constructor(moshi: Moshi,
                    private val matchInvitationMapper: MatchInvitationMapper)
    : NotificationCacheToDomainMapper<MatchInvitationNotification> {

    private val adapter = moshi.adapter(MatchInvitationNotificationData::class.java)

    override fun mapCacheToDomain(notificationCache: NotificationCache): MatchInvitationNotification {
        val data = adapter.fromJson(notificationCache.data)!!
        return notificationCache.run {
            MatchInvitationNotification(
                    id, createDate,
                    markedAsRead,
                    matchInvitationMapper.mapApiResponseToDomain(data.matchInvitation))
        }
    }

    private data class MatchInvitationNotificationData(
            @Json(name = "invitation") val matchInvitation: MatchInvitationResponse)
}