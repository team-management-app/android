package com.tomaszstankowski.demcouch.main.repository.notification.model

import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.domain.notification.TeamJoinRequestNotification
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import com.tomaszstankowski.demcouch.main.web.user.UserResponse
import javax.inject.Inject

class TeamJoinRequestNotificationMapper @Inject constructor(moshi: Moshi)
    : NotificationCacheToDomainMapper<TeamJoinRequestNotification> {

    private val adapter = moshi.adapter(TeamJoinRequestNotificationData::class.java)

    override fun mapCacheToDomain(notificationCache: NotificationCache): TeamJoinRequestNotification {
        val data = adapter.fromJson(notificationCache.data)!!
        return notificationCache.run {
            TeamJoinRequestNotification(
                    id,
                    createDate,
                    markedAsRead,
                    data.requestingUser,
                    data.team)
        }
    }

    private data class TeamJoinRequestNotificationData
    constructor(@Json(name = "sender") val requestingUser: UserResponse,
                val team: TeamResponse)
}