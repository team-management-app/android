package com.tomaszstankowski.demcouch.main.repository.notification.model

import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.domain.notification.MatchInvitationRejectionNotification
import com.tomaszstankowski.demcouch.main.web.matchinvitation.MatchInvitationMapper
import com.tomaszstankowski.demcouch.main.web.matchinvitation.MatchInvitationResponse
import javax.inject.Inject

class MatchInvitationRejectionNotificationMapper
@Inject constructor(moshi: Moshi,
                    private val matchInvitationMapper: MatchInvitationMapper)
    : NotificationCacheToDomainMapper<MatchInvitationRejectionNotification> {

    private val adapter = moshi.adapter(MatchInvitationRejectionNotificationData::class.java)

    override fun mapCacheToDomain(notificationCache: NotificationCache): MatchInvitationRejectionNotification {
        val data = adapter.fromJson(notificationCache.data)!!
        return notificationCache.run {
            MatchInvitationRejectionNotification(
                    id, createDate,
                    markedAsRead,
                    matchInvitationMapper.mapApiResponseToDomain(data.matchInvitation))
        }
    }

    private data class MatchInvitationRejectionNotificationData(
            @Json(name = "invitation") val matchInvitation: MatchInvitationResponse)
}