package com.tomaszstankowski.demcouch.main.repository.matchinvitation

import com.tomaszstankowski.demcouch.main.cache.match.MatchDao
import com.tomaszstankowski.demcouch.main.cache.matchinvitation.MatchInvitationDao
import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.domain.MatchInvitation
import com.tomaszstankowski.demcouch.main.domain.MatchInvitationStatus
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation.Coordinates
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.combineCacheWithApi
import com.tomaszstankowski.demcouch.main.repository.match.MatchMapper
import com.tomaszstankowski.demcouch.main.repository.team.TeamMapper
import com.tomaszstankowski.demcouch.main.web.match.MatchResponse
import com.tomaszstankowski.demcouch.main.web.matchinvitation.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.threeten.bp.ZonedDateTime
import javax.inject.Inject

class MatchInvitationRepository @Inject constructor(private val service: MatchInvitationService,
                                                    private val mapper: MatchInvitationMapper,
                                                    private val dao: MatchInvitationDao,
                                                    private val teamMapper: TeamMapper,
                                                    private val teamDao: TeamDao,
                                                    private val matchDao: MatchDao,
                                                    private val matchMapper: MatchMapper) {

    fun saveMatchInvitationInCache(matchInvitationResponse: MatchInvitationResponse) {
        val invitingTeamCache = teamMapper.mapApiResponseToCache(matchInvitationResponse.invitingTeam)
        val invitedTeamCache = teamMapper.mapApiResponseToCache(matchInvitationResponse.invitedTeam)
        teamDao.insert(invitingTeamCache)
        teamDao.insert(invitedTeamCache)
        val invitationCache = mapper.mapApiResponseToCache(matchInvitationResponse)
        dao.insert(invitationCache)
    }

    fun saveMatchInCache(matchResponse: MatchResponse) {
        val hostTeamCache = teamMapper.mapApiResponseToCache(matchResponse.hostTeam)
        val guestTeamCache = teamMapper.mapApiResponseToCache(matchResponse.guestTeam)
        teamDao.insert(hostTeamCache)
        teamDao.insert(guestTeamCache)
        val matchCache = matchMapper.mapApiResponseToCache(matchResponse)
        matchDao.insert(matchCache)
    }

    fun createMatchInvitation(invitingTeamId: Long, invitedTeamId: Long, matchLocation: Coordinates, matchDate: ZonedDateTime): Single<MatchInvitation> {
        val body = MatchInvitationCreateRequestBody(invitingTeamId, invitedTeamId, matchLocation, matchDate)
        return service.createMatchInvitation(body)
                .doOnSuccess(this::saveMatchInvitationInCache)
                .map(mapper::mapApiResponseToDomain)
    }

    fun getMatchInvitation(invitationId: Long): Flowable<Resource<MatchInvitation>> {
        return combineCacheWithApi(
                cacheSource = dao.findById(invitationId),
                apiSource = service.getMatchInvitation(invitationId.toString()),
                saveInCache = this::saveMatchInvitationInCache,
                mapCacheToDomain = mapper::mapCacheToDomain)
    }

    fun getReceivedMatchInvitationsForTeam(teamId: Long): Flowable<Resource<List<MatchInvitation>>> {
        return combineCacheWithApi(
                cacheSource = dao.findByInvitedTeam(teamId)
                        .map { list ->
                            list.filter { it.status == MatchInvitationStatus.WAITING.toString() }
                        },
                apiSource = service.getMatchInvitationsForTeam(teamId.toString()),
                saveInCache = { list -> list.forEach(this::saveMatchInvitationInCache) },
                mapCacheToDomain = { list -> list.map(mapper::mapCacheToDomain) })
    }

    fun acceptMatchInvitation(invitationId: Long): Completable {
        val body = MatchInvitationStatusUpdateRequestBody(MatchInvitationStatus.APPROVED)
        return service.updateInvitationStatus(invitationId.toString(), body)
                .doOnSuccess { saveMatchInvitationInCache(it.matchInvitation) }
                .doOnSuccess { saveMatchInCache(it.match!!) }
                .flatMapCompletable { Completable.complete() }
    }

    fun rejectMatchInvitation(invitationId: Long): Completable {
        val body = MatchInvitationStatusUpdateRequestBody(MatchInvitationStatus.REJECTED)
        return service.updateInvitationStatus(invitationId.toString(), body)
                .doOnSuccess { saveMatchInvitationInCache(it.matchInvitation) }
                .flatMapCompletable { Completable.complete() }
    }
}