package com.tomaszstankowski.demcouch.main.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.tomaszstankowski.demcouch.R

class ErrorViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val text: TextView = view.findViewById(R.id.recyclerViewErrorLayoutText)
}