package com.tomaszstankowski.demcouch.main.features.createteam.steps.choosediscipline

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.features.createteam.steps.StepFragment
import kotlinx.android.synthetic.main.fragment_choose_discipline.*

class ChooseDisciplineFragment : StepFragment() {

    companion object {
        fun getInstance(discipline: Discipline?): ChooseDisciplineFragment {
            val args = Bundle()
            args.putString(DISCIPLINE_KEY, discipline?.toString())
            val fragment = ChooseDisciplineFragment()
            fragment.arguments = args
            return fragment
        }

        private const val DISCIPLINE_KEY = "discipline"
    }

    val discipline: Discipline
        get() = when (fragmentChooseDisciplineRadioGroup.checkedRadioButtonId) {
            R.id.fragmentChooseDisciplineFootballRadio -> Discipline.FOOTBALL
            R.id.fragmentChooseDisciplineBasketballRadio -> Discipline.BASKETBALL
            R.id.fragmentChooseDisciplineVolleyballRadio -> Discipline.VOLLEYBALL
            R.id.fragmentChooseDisciplineHandballRadio -> Discipline.HANDBALL
            else -> throw IllegalStateException("Unknown checked radio button id")
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_choose_discipline, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState == null) {
            val disciplineString = arguments?.getString(DISCIPLINE_KEY)
            if (disciplineString != null) {
                val discipline = Discipline.valueOf(disciplineString)
                when (discipline) {
                    Discipline.FOOTBALL -> fragmentChooseDisciplineRadioGroup.check(R.id.fragmentChooseDisciplineFootballRadio)
                    Discipline.BASKETBALL -> fragmentChooseDisciplineRadioGroup.check(R.id.fragmentChooseDisciplineBasketballRadio)
                    Discipline.VOLLEYBALL -> fragmentChooseDisciplineRadioGroup.check(R.id.fragmentChooseDisciplineVolleyballRadio)
                    Discipline.HANDBALL -> fragmentChooseDisciplineRadioGroup.check(R.id.fragmentChooseDisciplineHandballRadio)
                }
                setValid(true)
            }
        }

        fragmentChooseDisciplineRadioGroup.setOnCheckedChangeListener { _, _ ->
            setValid(true)
        }
    }
}