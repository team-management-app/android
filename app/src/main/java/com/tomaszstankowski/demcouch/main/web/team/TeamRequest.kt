package com.tomaszstankowski.demcouch.main.web.team

import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Location

data class TeamRequest(val name: String,
                       val description: String,
                       val location: Location,
                       val discipline: Discipline)