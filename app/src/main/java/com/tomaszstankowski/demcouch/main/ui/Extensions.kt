package com.tomaszstankowski.demcouch.main.ui

import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.location.places.Place
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.TeamMembershipRole
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.temporal.ChronoUnit

fun Discipline.toDisplayString(context: Context): String = when (this) {
    Discipline.FOOTBALL -> context.getString(R.string.football)
    Discipline.BASKETBALL -> context.getString(R.string.basketball)
    Discipline.VOLLEYBALL -> context.getString(R.string.volleyball)
    Discipline.HANDBALL -> context.getString(R.string.handball)
}

fun TeamMembershipRole.toDisplayString(context: Context): String = when (this) {
    TeamMembershipRole.LEADER -> context.getString(R.string.role_leader)
    TeamMembershipRole.PLAYER -> context.getString(R.string.role_player)
}

fun ValidationError.getMessage(context: Context, data: Any?): String =
        when (this) {
            ValidationError.REQUIRED ->
                context.getString(R.string.validation_error_required)
            ValidationError.MIN_LENGTH ->
                context.getString(R.string.validation_error_min_length, data as Int?)
            ValidationError.MAX_LENGTH ->
                context.getString(R.string.validation_error_max_length, data as Int?)
            ValidationError.ILLEGAL_CHARACTERS ->
                context.getString(R.string.validation_error_illegal_characters)
            ValidationError.CONFLICT ->
                context.getString(R.string.validation_error_conflict, data as String?)
            ValidationError.PATTERN ->
                context.getString(R.string.validation_error_pattern, data as String?)
            ValidationError.NOT_MATCH ->
                context.getString(R.string.validation_error_fields_do_not_match)
            ValidationError.SIZE_EXCEEDED ->
                context.getString(R.string.validation_error_size_exceeded, data as Int?)
        }

fun DemCouchFragment.startFileChooser(requestCode: Int) {
    val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
    intent.addCategory(Intent.CATEGORY_OPENABLE)
    intent.type = "image/*"
    startActivityForResult(intent, requestCode)
}

fun Place.toLocation() =
        Location(address.toString(), latLng.latitude, latLng.longitude)

fun Location.toBundle(): Bundle {
    val bundle = Bundle()
    bundle.putString("name", name)
    bundle.putDouble("lat", lat)
    bundle.putDouble("lng", lng)
    return bundle
}

fun Bundle.toLocation(): Location {
    return Location(
            getString("name")!!,
            getDouble("lat"),
            getDouble("lng"))
}

fun TextInputLayout.changeStatus(status: FieldStatus) {
    if (status.isValid) {
        error = null
        val icon = context.getDrawable(R.drawable.ic_check_circle_green_24dp)
        editText?.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null)
    } else {
        error = status.error?.getMessage(context, status.data)
        val icon = context.getDrawable(R.drawable.ic_error_red_24dp)
        editText?.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null)
    }
}

fun TextInputLayout.resetStatus() {
    error = null
    editText?.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
}

fun TextInputLayout.addOnTextChangeListener(listener: (String, TextInputLayout) -> Unit) {
    editText?.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            listener(p0?.toString() ?: "", this@addOnTextChangeListener)
        }
    })
}

fun ImageView.loadThumbnailFromUrl(url: String?) {
    Glide.with(this)
            .load(url ?: R.drawable.default_thumbnail)
            .apply(
                    RequestOptions()
                            .error(R.drawable.default_thumbnail)
                            .placeholder(R.drawable.default_thumbnail)
                            .circleCrop())
            .into(this)
}

fun ImageView.loadThumbnailFromUri(uri: Uri?) {
    Glide.with(this)
            .load(uri ?: R.drawable.default_thumbnail)
            .apply(
                    RequestOptions()
                            .error(R.drawable.default_thumbnail)
                            .placeholder(R.drawable.default_thumbnail)
                            .circleCrop())
            .into(this)
}

fun DemCouchFragment.hideKeyboard() {
    val windowToken = view?.rootView?.windowToken
    val inputMethodManager: InputMethodManager? = context?.getSystemService(INPUT_METHOD_SERVICE)
            as InputMethodManager?
    inputMethodManager?.hideSoftInputFromWindow(windowToken, 0)
}

fun View.setVisibleOrElseGone(value: Boolean) {
    visibility = if (value) View.VISIBLE else View.GONE
}

fun View.setVisibleOrElseInvisible(value: Boolean) {
    visibility = if (value) View.VISIBLE else View.GONE
}

fun ZonedDateTime.toMillis(): Long {
    val instant = toInstant()
    return instant.toEpochMilli()
}

fun LocalDate.toMillis(): Long {
    val instant = atStartOfDay(ZoneId.systemDefault())
    return instant.toMillis()
}

val LocalDate.yearsAgo: Long
    get() = ChronoUnit.YEARS.between(this, LocalDate.now())