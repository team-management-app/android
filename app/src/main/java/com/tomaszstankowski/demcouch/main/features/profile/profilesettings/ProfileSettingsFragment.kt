package com.tomaszstankowski.demcouch.main.features.profile.profilesettings

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.selectthumbnail.SelectThumbnailActivity
import com.tomaszstankowski.demcouch.main.ui.*
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_profile_settings.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

class ProfileSettingsFragment : DemCouchFragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var viewModel: ProfileSettingsViewModel

    companion object {
        private const val CHANGE_THUMBNAIL_REQUEST_CODE = 7573
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_settings, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()


        if (savedInstanceState == null) {
            val disposable = viewModel.getUser()
                    .subscribeOn(Schedulers.backgroundThreadScheduler())
                    .observeOn(Schedulers.mainThreadScheduler())
                    .subscribe({
                        fragmentProfileSettingsFirstNameTIL?.editText?.setText(it.firstName)
                        fragmentProfileSettingsLastNameTIL?.editText?.setText(it.lastName)
                        if (it.bornDate != null) {
                            viewModel.onBornDateChanged(it.bornDate)
                        }
                        fragmentProfileSettingsThumbnail.loadThumbnailFromUrl(it.thumbnail)
                    }, {
                        Navigation.findNavController(activity!!, R.id.navHostFragment).navigateUp()
                        showMessage(R.string.load_error)
                    })
            compositeDisposable.add(disposable)
        }
        fragmentProfileSettingsSaveButton.setOnClickListener { viewModel.onSaveChangesClicked() }
        fragmentProfileSettingsFirstNameTIL.addOnTextChangeListener { text, view ->
            view.resetStatus()
            viewModel.firstName.postValue(text)
        }
        fragmentProfileSettingsLastNameTIL.addOnTextChangeListener { text, view ->
            view.resetStatus()
            viewModel.lastName.postValue(text)
        }
        fragmentProfileSettingsChangeThumbnailIcon.setOnClickListener {
            val intent = Intent(context!!, SelectThumbnailActivity::class.java)
            startActivityForResult(intent, CHANGE_THUMBNAIL_REQUEST_CODE)
        }
        fragmentProfileSettingsRemoveThumbnailIcon.setOnClickListener {
            viewModel.onRemoveThumbnailClicked()
        }
        fragmentProfileSettingsBornDateET.setOnClickListener {
            val dialog = DatePickerDialog(activity!!, this, 1990, 0, 1)
            val datePicker = dialog.datePicker
            datePicker.minDate = viewModel.minBornDate.toMillis()
            datePicker.maxDate = viewModel.maxBornDate.toMillis()
            dialog.show()
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.form.valid, {
            fragmentProfileSettingsSaveButton.isEnabled = it
        })

        subscribe(viewModel.bornDate, {
            val dateString = DateTimeFormatter.ISO_LOCAL_DATE.format(it)
            fragmentProfileSettingsBornDateET.setText(dateString)
        })
        subscribe(viewModel.saveChanges.failure, {
            showMessage(R.string.profile_settings_fragment_save_failure)
        })
        subscribe(viewModel.saveChanges.loading, {
            fragmentProfileSettingsProgressBar.setVisibleOrElseGone(it)
        })
        subscribe(viewModel.changeThumbnail.loading, {
            fragmentProfileSettingsProgressBar.setVisibleOrElseGone(it)
        })
        subscribe(viewModel.changeThumbnail.failure, {
            showMessage(R.string.error)
        })

        subscribe(viewModel.deleteThumbnail.loading, {
            fragmentProfileSettingsProgressBar.setVisibleOrElseGone(it)
        })
        subscribe(viewModel.deleteThumbnail.failure, {
            showMessage(R.string.error)
        })

        subscribe(viewModel.firstName.status, fragmentProfileSettingsFirstNameTIL::changeStatus)
        subscribe(viewModel.lastName.status, fragmentProfileSettingsLastNameTIL::changeStatus)

        subscribe(viewModel.thumbnailSelection, {
            fragmentProfileSettingsRemoveThumbnailIcon.visibility =
                    if (it.url == null) View.INVISIBLE
                    else View.VISIBLE
            fragmentProfileSettingsRemoveThumbnailIcon.isEnabled = it.url != null
            fragmentProfileSettingsThumbnail.loadThumbnailFromUrl(it.url)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CHANGE_THUMBNAIL_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val uri = data?.data
                    if (uri != null)
                        viewModel.changeThumbnail(uri)
                }
            }
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val localDate = LocalDate.of(year, month + 1, dayOfMonth)
        viewModel.onBornDateChanged(localDate)
    }
}