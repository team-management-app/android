package com.tomaszstankowski.demcouch.main.web.matchinvitation

import com.tomaszstankowski.demcouch.main.domain.MatchInvitationStatus

data class MatchInvitationStatusUpdateRequestBody(val status: MatchInvitationStatus)