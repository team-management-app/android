package com.tomaszstankowski.demcouch.main.features.match

import com.tomaszstankowski.demcouch.main.domain.Match
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.match.MatchRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MatchViewModel
@Inject constructor(private val matchRepository: MatchRepository) : DemCouchViewModel() {

    private val _match = BehaviorSubject.create<Resource<Match>>()
    val match: Flowable<Resource<Match>>
        get() = _match.toFlowable(BackpressureStrategy.LATEST)
    val matchSnapshot: Resource<Match>?
        get() = _match.value

    private var init = false

    fun init(matchId: Long) {
        if (init)
            return
        val disposable = matchRepository.getMatch(matchId)
                .subscribeOn(backgroundThreadScheduler())
                .subscribe(_match::onNext)
        compositeDisposable.add(disposable)
    }
}