package com.tomaszstankowski.demcouch.main.features.notifications

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.repository.notification.NotificationRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class NotificationsViewModel
@Inject constructor(private val notificationRepository: NotificationRepository,
                    authService: AuthenticationService) : DemCouchViewModel() {

    private val _notifications = BehaviorSubject.create<List<Notification>>()
    val notifications: Flowable<List<Notification>>
        get() = _notifications.toFlowable(BackpressureStrategy.LATEST)

    init {
        val disposable = notificationRepository.getNotificationsForUser(authService.userId)
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .subscribe { _notifications.onNext(it) }
        compositeDisposable.add(disposable)
    }

    fun markAsRead(notification: Notification) {
        val disposable = notificationRepository.markNotificationAsRead(notification.id)
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .onErrorComplete()
                .subscribe()
        compositeDisposable.add(disposable)
    }
}