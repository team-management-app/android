package com.tomaszstankowski.demcouch.main.domain

import com.squareup.moshi.Json
import org.threeten.bp.ZonedDateTime

data class MatchInvitation(val id: Long,
                           val createDate: ZonedDateTime,
                           val status: MatchInvitationStatus,
                           val invitingTeam: Team,
                           val invitedTeam: Team,
                           val matchLocation: Location,
                           val matchDate: ZonedDateTime)

enum class MatchInvitationStatus {
    @Json(name = "waiting")
    WAITING,
    @Json(name = "approved")
    APPROVED,
    @Json(name = "rejected")
    REJECTED
}