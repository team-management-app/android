package com.tomaszstankowski.demcouch.main.features.matchinvitation

import com.tomaszstankowski.demcouch.main.domain.MatchInvitation
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.matchinvitation.MatchInvitationRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class MatchInvitationViewModel
@Inject constructor(private val repository: MatchInvitationRepository) : DemCouchViewModel() {

    private var invitationId = 0L

    private val _invitation = BehaviorSubject.create<Resource<MatchInvitation>>()
    val invitation: Flowable<Resource<MatchInvitation>>
        get() = _invitation.toFlowable(BackpressureStrategy.LATEST)
    val invitationSnapshot: Resource<MatchInvitation>?
        get() = _invitation.value

    val accept = Action()

    val reject = Action()

    fun init(invitationId: Long) {
        if (this.invitationId > 0)
            return
        this.invitationId = invitationId
        val disposable = repository.getMatchInvitation(invitationId)
                .subscribeOn(backgroundThreadScheduler())
                .subscribe(_invitation::onNext)
        compositeDisposable.add(disposable)
    }

    fun onAcceptClicked() {
        val disposable = accept.invoke(repository.acceptMatchInvitation(invitationId))
                .subscribe({}, {
                    Timber.e(it, "Failed to accept invitation")
                })
        compositeDisposable.add(disposable)
    }

    fun onRejectClicked() {
        val disposable = reject.invoke(repository.rejectMatchInvitation(invitationId))
                .subscribe({}, {
                    Timber.e(it, "Failed to reject invitation")
                })
        compositeDisposable.add(disposable)
    }
}