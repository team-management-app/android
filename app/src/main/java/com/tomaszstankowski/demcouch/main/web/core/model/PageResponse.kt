package com.tomaszstankowski.demcouch.main.web.core.model

import com.squareup.moshi.Json

data class PageResponse<T>(val data: List<T>, val pagination: Pagination) {
    data class Pagination(
            @Json(name = "current_page") val page: Int,
            @Json(name = "page_count") val pageCount: Int,
            @Json(name = "total_item_count") val totalItemCount: Int)
}

