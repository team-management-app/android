package com.tomaszstankowski.demcouch.main.repository

enum class FetchStatus {
    LOADING, SUCCESS, FAIL
}