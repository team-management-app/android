package com.tomaszstankowski.demcouch.main.auth

import com.tomaszstankowski.demcouch.main.web.login.LoginRequest
import com.tomaszstankowski.demcouch.main.web.login.LoginService
import com.tomaszstankowski.demcouch.main.web.user.ChangePasswordRequest
import com.tomaszstankowski.demcouch.main.web.user.UserService
import io.reactivex.Completable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthenticationService
@Inject constructor(private val userCacheService: UserCacheService,
                    private val loginWebService: LoginService,
                    private val userWebService: UserService,
                    private val logOutEventEmitter: LogOutEventEmitter) {
    val userId: Long
        get() {
            val id = userCacheService.userId
            val idNotPresentHandler = {
                logOut().blockingAwait()
                0L
            }
            return id ?: idNotPresentHandler.invoke()

        }

    fun ensureUserIsAuthenticated() {
        if (userCacheService.userId == null)
            logOut().blockingAwait()
    }

    fun logIn(username: String, password: String): Completable {
        val request = LoginRequest(username, password)
        return loginWebService.login(request)
                .doOnSuccess {
                    userCacheService.setCurrentUser(username, it.token, it.userId)
                }
                .flatMapCompletable { Completable.complete() }
    }

    fun logOut(): Completable {
        return Completable.create {
            userCacheService.invalidateCurrentUser()
            logOutEventEmitter.emit()
            it.onComplete()
        }
    }

    fun changePassword(password: String): Completable {
        val request = ChangePasswordRequest(password)
        return userWebService.changePassword(userId, request)
    }
}