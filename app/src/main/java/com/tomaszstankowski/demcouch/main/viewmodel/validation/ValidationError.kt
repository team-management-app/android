package com.tomaszstankowski.demcouch.main.viewmodel.validation

enum class ValidationError {
    REQUIRED,
    MIN_LENGTH,
    MAX_LENGTH,
    ILLEGAL_CHARACTERS,
    CONFLICT,
    PATTERN,
    NOT_MATCH,
    SIZE_EXCEEDED
}