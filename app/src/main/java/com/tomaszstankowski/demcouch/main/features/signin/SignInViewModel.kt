package com.tomaszstankowski.demcouch.main.features.signin

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import timber.log.Timber
import javax.inject.Inject

class SignInViewModel
@Inject constructor(private val authService: AuthenticationService) : DemCouchViewModel() {

    var email: String = ""
    var password: String = ""

    val signIn = Action()

    fun onSignInClicked() {
        val disposable = signIn.invoke(authService.logIn(email, password))
                .subscribe({
                }, {
                    Timber.e(it, "Error while signing in")
                })
        compositeDisposable.add(disposable)
    }
}