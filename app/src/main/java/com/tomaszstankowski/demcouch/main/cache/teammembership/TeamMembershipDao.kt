package com.tomaszstankowski.demcouch.main.cache.teammembership

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface TeamMembershipDao {

    @Query("SELECT * FROM team_membership WHERE user_id = :userId")
    fun findMembershipsForUser(userId: Long): Flowable<List<TeamMembershipCache>>

    @Query("SELECT * FROM team_membership WHERE team_id = :teamId")
    fun findMembershipsForTeam(teamId: Long): Flowable<List<TeamMembershipCache>>

    @Query("DELETE FROM team_membership WHERE team_id = :teamId")
    fun deleteMembershipsForTeam(teamId: Long)

    @Query("DELETE FROM team_membership WHERE user_id =:userId")
    fun deleteMembershipsForUser(userId: Long)

    @Query("DELETE FROM team_membership WHERE id =:id")
    fun deleteById(id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(membership: TeamMembershipCache)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(memberships: List<TeamMembershipCache>)
}