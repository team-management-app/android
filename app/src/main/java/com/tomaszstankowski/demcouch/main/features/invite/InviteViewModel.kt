package com.tomaszstankowski.demcouch.main.features.invite

import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation.Coordinates
import com.tomaszstankowski.demcouch.main.repository.matchinvitation.MatchInvitationRepository
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import org.threeten.bp.ZonedDateTime
import timber.log.Timber
import javax.inject.Inject

class InviteViewModel
@Inject constructor(private val repository: MatchInvitationRepository) : DemCouchViewModel() {

    private val _selectedTeam = BehaviorSubject.create<Team>()

    private val _selectedLocation = BehaviorSubject.create<Coordinates>()

    private val _selectedDate = BehaviorSubject.create<ZonedDateTime>()

    var currentStep = 0
        set(value) {
            field = value
            val isReady = value == 2
            _ready.onNext(isReady)
        }

    var invitedTeamId: Long = 0

    val invitation = Action()

    private val _ready = BehaviorSubject.create<Boolean>()
    val ready: Flowable<Boolean>
        get() = _ready.toFlowable(BackpressureStrategy.LATEST)

    fun onTeamSelected(team: Team) {
        _selectedTeam.onNext(team)
    }

    fun onLocationSelected(coordinates: Coordinates) {
        _selectedLocation.onNext(coordinates)
    }

    fun onDateSelected(date: ZonedDateTime) {
        _selectedDate.onNext(date)
    }

    fun onInviteClicked() {
        val single = repository.createMatchInvitation(
                _selectedTeam.value!!.id,
                invitedTeamId,
                _selectedLocation.value!!,
                _selectedDate.value!!)
        val disposable = invitation.invoke(single)
                .subscribe({}, {
                    Timber.e(it, "Failed to invite")
                })
        compositeDisposable.add(disposable)
    }
}