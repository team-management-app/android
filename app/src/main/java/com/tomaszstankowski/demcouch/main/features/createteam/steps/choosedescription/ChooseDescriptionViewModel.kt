package com.tomaszstankowski.demcouch.main.features.createteam.steps.choosedescription

import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidatedField
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.DescriptionValidator
import javax.inject.Inject

class ChooseDescriptionViewModel
@Inject constructor(private val validator: DescriptionValidator) : DemCouchViewModel() {

    val description = ValidatedField(validator::validate)
}