package com.tomaszstankowski.demcouch.main.web.core.model

data class ThumbnailResponse(val thumbnail: String?)