package com.tomaszstankowski.demcouch.main.domain

import com.squareup.moshi.Json

enum class Discipline {
    @Json(name = "football")
    FOOTBALL,
    @Json(name = "basketball")
    BASKETBALL,
    @Json(name = "volleyball")
    VOLLEYBALL,
    @Json(name = "handball")
    HANDBALL
}