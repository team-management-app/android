package com.tomaszstankowski.demcouch.main.features.editteam

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.ui.*
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_edit_team.*
import timber.log.Timber

class EditTeamFragment : DemCouchFragment() {

    companion object {
        const val ID_KEY = "id"
        const val DIALOG_REQUEST_CODE = 777
        const val REQUEST_PERMISSIONS_REQUEST_CODE = 123
        const val PLACE_AUTOCOMPLETE_REQUEST_CODE = 456
    }

    private lateinit var viewModel: EditTeamViewModel

    private fun openPlaceAutocomplete() {
        try {
            val filter = AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                    .build()
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(filter)
                    .build(activity!!)
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
        } catch (e: Exception) {
            Timber.e(e, "Could not open places autocomplete")
        }
    }

    private fun setFormDefaultValues(team: Team) {
        fragmentEditTeamContentLoadErrorFrame.visibility = View.GONE
        fragmentEditTeamContentFrame.visibility = View.VISIBLE

        team.apply {
            fragmentEditTeamNameTIL.editText?.setText(name)
            fragmentEditTeamDescriptionTIL.editText?.setText(desc)
            fragmentEditTeamLocationTV.text = location.name
            val checkedRadioButtonId = when (discipline) {
                Discipline.FOOTBALL -> R.id.fragmentEditTeamFootballRadio
                Discipline.BASKETBALL -> R.id.fragmentEditTeamBasketballRadio
                Discipline.VOLLEYBALL -> R.id.fragmentEditTeamVolleyballRadio
                Discipline.HANDBALL -> R.id.fragmentEditTeamHandballRadio
            }
            fragmentEditTeamDisciplineRadioGroup.check(checkedRadioButtonId)
        }
    }

    private fun showErrorView() {
        fragmentEditTeamContentLoadErrorFrame.visibility = View.VISIBLE
        fragmentEditTeamContentFrame.visibility = View.GONE
    }

    private fun tryPickCurrentLocation() {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            viewModel.onPickCurrentLocationClicked()
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_team, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel()

        if (savedInstanceState == null) {
            val id = arguments!!.getLong(ID_KEY)

            val disposable = viewModel.getTeam(id)
                    .subscribeOn(Schedulers.backgroundThreadScheduler())
                    .observeOn(Schedulers.mainThreadScheduler())
                    .subscribe(this::setFormDefaultValues) {
                        showErrorView()
                    }
            compositeDisposable.add(disposable)
        }

        fragmentEditTeamNameTIL.addOnTextChangeListener { text, view ->
            viewModel.name.postValue(text)
            view.resetStatus()
        }
        fragmentEditTeamDescriptionTIL.addOnTextChangeListener { text, view ->
            viewModel.description.postValue(text)
            view.resetStatus()
        }

        fragmentEditTeamDisciplineRadioGroup.setOnCheckedChangeListener { _, checkedItemId ->
            val discipline = when (checkedItemId) {
                R.id.fragmentEditTeamFootballRadio -> Discipline.FOOTBALL
                R.id.fragmentEditTeamBasketballRadio -> Discipline.BASKETBALL
                R.id.fragmentEditTeamVolleyballRadio -> Discipline.VOLLEYBALL
                R.id.fragmentEditTeamHandballRadio -> Discipline.HANDBALL
                else -> throw IllegalArgumentException("Item with given id is not known")
            }
            viewModel.onDisciplineSelected(discipline)
        }

        fragmentEditTeamLocationTV.setOnClickListener {
            val dialog = EditLocationDialog()
            dialog.setTargetFragment(this, DIALOG_REQUEST_CODE)
            dialog.show(fragmentManager, null)
        }

        fragmentEditTeamSaveChangesButton.setOnClickListener {
            viewModel.onSaveChangesClicked()
        }

        fragmentEditTeamDeleteTeamBttn.setOnClickListener {
            AlertDialog.Builder(context!!)
                    .setTitle(R.string.edit_team_fragment_delete_dialog_title)
                    .setPositiveButton(R.string.yes) { _, _ -> viewModel.onDeleteTeamClicked() }
                    .setNegativeButton(R.string.cancel) { dialog: DialogInterface, _ -> dialog.dismiss() }
                    .create()
                    .show()
        }
    }

    override fun onStart() {
        super.onStart()

        subscribe(viewModel.updateTeam.success, {
            Navigation.findNavController(fragmentEditTeamNameTIL).navigateUp()
        })
        subscribe(viewModel.updateTeam.failure, {
            showMessage(R.string.edit_team_fragment_save_error)
        })
        subscribe(viewModel.updateTeam.loading, {
            fragmentEditTeamSaveChangesButton.isEnabled = !it
            fragmentEditTeamProgressBar.visibility = if (it) View.VISIBLE else View.GONE
        })

        subscribe(viewModel.pickCurrentLocation.failure, {
            showMessage(R.string.edit_team_fragment_location_error)
        })
        subscribe(viewModel.pickCurrentLocation.loading, {
            fragmentEditTeamSaveChangesButton.isEnabled = !it
            fragmentEditTeamProgressBar.visibility = if (it) View.VISIBLE else View.GONE
        })

        subscribe(viewModel.location, {
            fragmentEditTeamLocationTV.text = it.name
        })
        subscribe(viewModel.form.valid, {
            fragmentEditTeamSaveChangesButton.isEnabled = it
        })
        subscribe(viewModel.name.status, fragmentEditTeamNameTIL::changeStatus)
        subscribe(viewModel.description.status, fragmentEditTeamDescriptionTIL::changeStatus)

        subscribe(viewModel.deleteTeam.failure, {
            showMessage(R.string.edit_team_fragment_delete_error)
        })
        subscribe(viewModel.deleteTeam.success, {
            Navigation.findNavController(activity!!, R.id.navHostFragment)
                    .navigate(R.id.action_editTeamFragment_to_myTeamsFragment)
        })
        subscribe(viewModel.deleteTeam.loading, {
            fragmentEditTeamSaveChangesButton.isEnabled = !it
            fragmentEditTeamProgressBar.visibility = if (it) View.VISIBLE else View.GONE
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            DIALOG_REQUEST_CODE -> {
                when (resultCode) {
                    EditLocationDialog.RESULT_CODE_FIND -> openPlaceAutocomplete()
                    EditLocationDialog.RESULT_CODE_USE_CURRENT -> tryPickCurrentLocation()
                }
            }
            PLACE_AUTOCOMPLETE_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val place = PlaceAutocomplete.getPlace(activity!!, data)
                    val location = place.toLocation()
                    viewModel.onLocationFromAutoCompleteSelected(location)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSIONS_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewModel.onPickCurrentLocationClicked()
                }
            }
        }
    }

}