package com.tomaszstankowski.demcouch.main.features.main

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.view.Menu
import android.view.MenuItem
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.minibugdev.drawablebadge.BadgePosition
import com.minibugdev.drawablebadge.DrawableBadge
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.signin.SignInActivity
import com.tomaszstankowski.demcouch.main.ui.activities.DemCouchActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DemCouchActivity(),
        NavController.OnNavigatedListener {

    private lateinit var viewModel: MainViewModel
    private lateinit var navController: NavController
    private lateinit var notificationsDrawableBadge: DrawableBadge

    private var notificationsCount = 0

    private fun startSignInActivity() {
        val intent = Intent(applicationContext, SignInActivity::class.java)
        startActivity(intent)
    }

    private fun restart() {
        val intent = Intent(applicationContext, MainActivity::class.java)
        finish()
        startActivity(intent)
    }

    private fun updateToolbarAccordingToDestination(destinationId: Int) {
        setToolbarTitleAccordingToDestination(destinationId)
        setToolbarIconAccordingToDestination(destinationId)
    }

    private fun setToolbarTitleAccordingToDestination(destinationId: Int) {
        val toolbarTitle = when (destinationId) {
            R.id.accountFragment -> getString(R.string.account_fragment_title)
            R.id.matchListFragment -> getString(R.string.match_list_fragment_title)
            R.id.matchFragment -> getString(R.string.match_fragment_title)
            R.id.myTeamsFragment -> getString(R.string.my_teams_fragment_title)
            R.id.profileFragment -> getString(R.string.profile_fragment_title)
            R.id.searchTeamsFragment -> getString(R.string.search_teams_fragment_title)
            R.id.teamDetailsFragment -> getString(R.string.team_details_fragment_title)
            R.id.profileSettingsFragment -> getString(R.string.profile_settings_fragment_title)
            R.id.changePasswordFragment -> getString(R.string.change_password_fragment_title)
            R.id.editTeamFragment -> getString(R.string.edit_team_fragment_title)
            R.id.notificationsFragment -> getString(R.string.notifications_fragment_title)
            R.id.manageMembershipsFragment -> getString(R.string.manage_memberships_fragment_title)
            R.id.joinRequestListFragment -> getString(R.string.join_request_list_fragment_title)
            R.id.matchInvitationListFragment -> getString(R.string.match_invitation_list_fragment_title)
            R.id.matchInvitationFragment -> getString(R.string.match_invitation_fragment_title)
            else -> ""
        }
        toolbar.title = toolbarTitle
    }

    private fun setToolbarIconAccordingToDestination(destinationId: Int) {
        val iconId = when (destinationId) {
            R.id.matchListFragment,
            R.id.myTeamsFragment,
            R.id.accountFragment,
            R.id.searchTeamsFragment -> R.drawable.ic_menu_white_24dp
            R.id.teamDetailsFragment,
            R.id.changePasswordFragment,
            R.id.editTeamFragment,
            R.id.profileSettingsFragment,
            R.id.notificationsFragment,
            R.id.manageMembershipsFragment,
            R.id.joinRequestListFragment,
            R.id.matchInvitationListFragment,
            R.id.matchInvitationFragment,
            R.id.profileFragment,
            R.id.matchFragment -> R.drawable.ic_arrow_back_white_24dp
            else -> R.drawable.ic_arrow_back_white_24dp
        }
        supportActionBar?.setHomeAsUpIndicator(iconId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = getViewModel()

        navController = Navigation.findNavController(this, R.id.navHostFragment)
        navController.addOnNavigatedListener(this)
        navigationView.setCheckedItem(R.id.matchListFragment)
        NavigationUI.setupWithNavController(navigationView, navController)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)

        notificationsDrawableBadge = DrawableBadge.Builder(this)
                .drawableResId(R.drawable.ic_notifications_white_36dp)
                .badgeColor(R.color.colorRed)
                .badgePosition(BadgePosition.BOTTOM_RIGHT)
                .build()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_bar, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.findItem(R.id.action_bar_notifications)
                ?.icon = notificationsDrawableBadge.get(notificationsCount)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onSupportNavigateUp(): Boolean {
        drawerLayout.openDrawer(GravityCompat.START)
        return true
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.logOutEvent, {
            if (it.isConsumed)
                return@subscribe
            it.consume()
            restart()
            startSignInActivity()
        })
        subscribe(viewModel.notificationsCount, {
            notificationsCount = it
            invalidateOptionsMenu()
        })
        viewModel.onViewStarted()

        val currentDestinationId = navController.currentDestination?.id ?: R.id.matchListFragment
        updateToolbarAccordingToDestination(currentDestinationId)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            when (navController.currentDestination?.id) {
                R.id.profileFragment,
                R.id.teamDetailsFragment,
                R.id.changePasswordFragment,
                R.id.editTeamFragment,
                R.id.profileSettingsFragment,
                R.id.notificationsFragment,
                R.id.manageMembershipsFragment,
                R.id.joinRequestListFragment,
                R.id.matchInvitationListFragment,
                R.id.matchInvitationFragment,
                R.id.matchFragment -> {
                    navController.navigateUp()
                    return true
                }
            }
        }
        if (item?.itemId == R.id.action_bar_notifications) {
            navController.navigate(R.id.action_to_notifications_fragment)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigated(controller: NavController, destination: NavDestination) {
        updateToolbarAccordingToDestination(destination.id)
    }
}
