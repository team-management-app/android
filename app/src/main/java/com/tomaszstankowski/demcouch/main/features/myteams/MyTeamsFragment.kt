package com.tomaszstankowski.demcouch.main.features.myteams

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.features.createteam.CreateTeamActivity
import com.tomaszstankowski.demcouch.main.features.teamdetails.TeamDetailsFragment
import com.tomaszstankowski.demcouch.main.ui.adapters.TeamListAdapter
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_my_teams.*

class MyTeamsFragment : DemCouchFragment() {
    companion object {
        private const val DIALOG_REQUEST_CODE = 123
        private const val CREATE_TEAM_ACTIVITY_REQUEST_CODE = 888
    }

    private lateinit var adapter: TeamListAdapter
    private lateinit var viewModel: MyTeamsViewModel

    private fun onItemClicked(item: Team) {
        val args = Bundle()
        args.putLong(TeamDetailsFragment.ID_KEY, item.id)
        Navigation.findNavController(fragmentMyTeamsCoordinatorLayout)
                .navigate(R.id.action_myTeamsFragment_to_teamDetailsFragment, args)
    }

    private fun openCreateOrJoinTeamDialog() {
        val dialog = MyTeamsFragmentBottomSheetDialog()
        dialog.setTargetFragment(this, DIALOG_REQUEST_CODE)
        dialog.show(fragmentManager, null)
    }

    private fun setUpRecyclerView() {
        adapter = TeamListAdapter(context!!)
        adapter.onItemClickListener = { item, _ -> onItemClicked(item) }
        fragmentMyTeamsRecyclerView.adapter = adapter
        fragmentMyTeamsRecyclerView.layoutManager = LinearLayoutManager(context)

        fragmentMyTeamsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && fragmentMyTeamsFloatingActionButton.visibility == View.VISIBLE) {
                    fragmentMyTeamsFloatingActionButton.hide()
                } else if (dy < 0 && fragmentMyTeamsFloatingActionButton.visibility != View.VISIBLE) {
                    fragmentMyTeamsFloatingActionButton.show()
                }
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_my_teams, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()
        setUpRecyclerView()
        fragmentMyTeamsFloatingActionButton.setOnClickListener {
            openCreateOrJoinTeamDialog()
        }
        fragmentMyTeamsSwipeRefreshLayout.setOnRefreshListener {
            viewModel.refresh()
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.teams, adapter::items::set)
        subscribe(viewModel.refreshing, {
            fragmentMyTeamsSwipeRefreshLayout.isRefreshing = it
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            DIALOG_REQUEST_CODE -> {
                when (resultCode) {
                    MyTeamsFragmentBottomSheetDialog.RESULT_CODE_CREATE_TEAM -> {
                        val intent = Intent(context, CreateTeamActivity::class.java)
                        startActivityForResult(intent, CREATE_TEAM_ACTIVITY_REQUEST_CODE)
                    }
                    MyTeamsFragmentBottomSheetDialog.RESULT_CODE_JOIN_TEAM ->
                        Navigation.findNavController(fragmentMyTeamsCoordinatorLayout)
                                .navigate(R.id.action_myTeamsFragment_to_searchTeamsFragment)
                }
            }
            CREATE_TEAM_ACTIVITY_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.refresh()
                }
            }
        }
    }
}