package com.tomaszstankowski.demcouch.main.web.team

import com.tomaszstankowski.demcouch.main.web.core.model.PageResponse
import com.tomaszstankowski.demcouch.main.web.core.model.ThumbnailResponse
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*

interface TeamService {

    @POST("teams")
    fun createTeam(@Body team: TeamRequest): Single<TeamResponse>

    @POST("teams/{id}/thumbnail")
    @Multipart
    fun uploadThumbnail(@Path("id") id: String,
                        @Part body: MultipartBody.Part): Single<ThumbnailResponse>

    @DELETE("teams/{id}/thumbnail")
    fun deleteThumbnail(@Path("id") id: String): Completable

    @PUT("teams/{id}")
    fun updateTeam(@Path("id") id: String, @Body team: TeamRequest): Single<TeamResponse>

    @DELETE("teams/{id}")
    fun deleteTeam(@Path("id") id: String): Completable

    @GET("teams")
    fun getTeams(@Query("page") page: Int = 1,
                 @Query("per_page") pageSize: Int = 10,
                 @Query("name") name: String? = null,
                 @Query("lat") lat: Float? = null,
                 @Query("lng") lng: Float? = null,
                 @Query("radius") radius: Float? = null): Single<PageResponse<TeamResponse>>

    @GET("teams/{id}")
    fun getTeam(@Path("id") id: String): Single<TeamResponse>

}