package com.tomaszstankowski.demcouch.main.cache.match

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.tomaszstankowski.demcouch.main.domain.Location
import org.threeten.bp.ZonedDateTime

@Entity(tableName = "match")
data class MatchCache(@PrimaryKey val id: Long,
                      val date: ZonedDateTime,
                      @Embedded(prefix = "location") val location: Location,
                      @ColumnInfo(name = "host_team_id") val hostTeamId: Long,
                      @ColumnInfo(name = "guest_team_id") val guestTeamId: Long)