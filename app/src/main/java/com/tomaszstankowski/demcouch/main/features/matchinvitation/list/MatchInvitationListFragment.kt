package com.tomaszstankowski.demcouch.main.features.matchinvitation.list

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.matchinvitation.MatchInvitationFragment
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_match_invitation_list.*

class MatchInvitationListFragment : DemCouchFragment() {

    private lateinit var viewModel: MatchInvitationListViewModel

    private lateinit var adapter: MatchInvitationListAdapter

    companion object {
        const val TEAM_ID_KEY = "teamId"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_match_invitation_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()
        val teamId = arguments?.getLong(TEAM_ID_KEY)
                ?: throw IllegalStateException("No '$TEAM_ID_KEY' passed to fragment")
        viewModel.init(teamId)

        adapter = MatchInvitationListAdapter(context!!)
        fragmentMatchInvitationListRecyclerView.layoutManager = LinearLayoutManager(context)
        fragmentMatchInvitationListRecyclerView.adapter = adapter
        adapter.onItemClickListener = { item, _ ->
            val args = Bundle()
            args.putLong(MatchInvitationFragment.INVITATION_ID_KEY, item.id)
            Navigation.findNavController(activity!!, R.id.navHostFragment)
                    .navigate(R.id.action_matchInvitationListFragment_to_matchInvitationFragment, args)
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.invitations, adapter::items::set)
    }
}