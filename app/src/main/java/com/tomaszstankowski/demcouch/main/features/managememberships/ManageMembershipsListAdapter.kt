package com.tomaszstankowski.demcouch.main.features.managememberships

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.domain.TeamMembershipRole
import com.tomaszstankowski.demcouch.main.ui.adapters.EmptyListViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.ErrorViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.FixedLengthAdapter
import com.tomaszstankowski.demcouch.main.ui.adapters.PlaceholderViewHolder
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.setVisibleOrElseGone
import com.tomaszstankowski.demcouch.main.ui.toDisplayString
import com.tomaszstankowski.demcouch.main.ui.toMillis

class ManageMembershipsListAdapter(private val context: Context) : FixedLengthAdapter<TeamMembership>() {

    var onMembershipDeleteClickListener: OnMembershipDeleteClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return when (viewType) {
            ITEM_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.manage_memberships_list_item, parent, false)
                ManageMembershipsViewHolder(view)
            }
            PLACE_HOLDER_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.manage_memberships_list_item_placeholder, parent, false)
                PlaceholderViewHolder(view)
            }
            EMPTY_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_empty_layout, parent, false)
                EmptyListViewHolder(view)
            }
            ERROR_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_error_layout, parent, false)
                ErrorViewHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, pos: Int) {
        if (holder is ManageMembershipsViewHolder) {
            val item = items.data!![pos]
            holder.deleteButton.setOnClickListener {
                onMembershipDeleteClickListener?.onMembershipDeleteClicked(item)
            }
            item.apply {
                holder.thumbnail.loadThumbnailFromUrl(user.thumbnail)
                @SuppressLint("SetTextI18n")
                holder.name.text = "${user.firstName} ${user.lastName}"
                val relativeTimeSpanString = if (startDate == null) "?"
                else DateUtils.getRelativeTimeSpanString(
                        startDate.toMillis(),
                        System.currentTimeMillis(),
                        DateUtils.DAY_IN_MILLIS)
                holder.joinDate.text = context.getString(
                        R.string.manage_memberships_list_item_join_date,
                        relativeTimeSpanString)
                holder.deleteButton.setVisibleOrElseGone(role != TeamMembershipRole.LEADER)
                holder.role.text = role.toDisplayString(context)
            }
        }
    }

    class ManageMembershipsViewHolder
    (view: View) : RecyclerView.ViewHolder(view) {
        val thumbnail: ImageView = view.findViewById(R.id.manageMembershipsListItemThumbnail)
        val name: TextView = view.findViewById(R.id.manageMembershipsListItemName)
        val deleteButton: ImageView = view.findViewById(R.id.manageMembershipsListItemDeleteButton)
        val joinDate: TextView = view.findViewById(R.id.manageMembershipsListItemJoinDate)
        val role: TextView = view.findViewById(R.id.manageMembershipsListItemRole)
    }

    interface OnMembershipDeleteClickListener {
        fun onMembershipDeleteClicked(membership: TeamMembership)
    }
}