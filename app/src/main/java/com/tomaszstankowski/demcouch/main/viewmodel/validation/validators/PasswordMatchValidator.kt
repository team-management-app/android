package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import io.reactivex.Single
import javax.inject.Inject

class PasswordMatchValidator @Inject constructor() {

    fun validate(password: String, passwordMatch: String): Single<FieldStatus> {
        return Single.create {
            if (password == passwordMatch) {
                it.onSuccess(FieldStatus.correct())
            } else {
                it.onSuccess(FieldStatus.error(ValidationError.NOT_MATCH, null))
            }
        }
    }
}