package com.tomaszstankowski.demcouch.main.features.main

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.auth.LogOutEventEmitter
import com.tomaszstankowski.demcouch.main.misc.ConsumableEvent
import com.tomaszstankowski.demcouch.main.repository.notification.NotificationRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MainViewModel @Inject constructor(private val authenticationService: AuthenticationService,
                                        private val logOutEventEmitter: LogOutEventEmitter,
                                        private val notificationRepository: NotificationRepository) : DemCouchViewModel() {
    private val _notificationsCount = BehaviorSubject.create<Int>()
    val notificationsCount: Flowable<Int>
        get() = _notificationsCount.toFlowable(BackpressureStrategy.LATEST)

    init {
        notificationRepository.startListeningForNotifications()
        val disposable = notificationRepository.getUnreadNotificationsCountForUser(userId)
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .subscribe { _notificationsCount.onNext(it) }
        compositeDisposable.add(disposable)
    }

    val logOutEvent: Flowable<ConsumableEvent<Any>>
        get() = logOutEventEmitter.logOutEvent

    val userId: Long
        get() = authenticationService.userId

    fun onViewStarted() {
        authenticationService.ensureUserIsAuthenticated()
    }

    override fun onCleared() {
        notificationRepository.stopListeningForNotifications()
        super.onCleared()
    }
}