package com.tomaszstankowski.demcouch.main.cache.notifications

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.threeten.bp.ZonedDateTime

@Entity(tableName = "notifications")
data class NotificationCache(@PrimaryKey val id: Long,
                             @ColumnInfo(name = "user_id") val userId: Long,
                             @ColumnInfo(name = "create_date") val createDate: ZonedDateTime,
                             val data: String,
                             val type: String,
                             @ColumnInfo(name = "marked_as_read") val markedAsRead: Boolean)