package com.tomaszstankowski.demcouch.main.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.yearsAgo

class UserHorizontalListViewHolder(private val context: Context, view: View) : RecyclerView.ViewHolder(view) {
    private val firstName: TextView = view.findViewById(R.id.userHorizontalListItemFirstName)
    private val lastName: TextView = view.findViewById(R.id.userHorizontalListItemLastName)
    private val age: TextView = view.findViewById(R.id.userHorizontalListItemAge)
    private val thumbnail: ImageView = view.findViewById(R.id.userHorizontalListItemThumbnail)

    fun bind(user: User) {
        firstName.text = user.firstName
        lastName.text = user.lastName
        age.text = if (user.bornDate == null) null
        else context.getString(R.string.user_horizontal_list_item_age, user.bornDate.yearsAgo)
        thumbnail.loadThumbnailFromUrl(user.thumbnail)
    }
}