package com.tomaszstankowski.demcouch.main.ui

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class Schedulers {

    companion object {
        fun mainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

        fun backgroundThreadScheduler(): Scheduler = Schedulers.io()
    }
}