package com.tomaszstankowski.demcouch.main.features.createteam.steps.choosename

import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidatedField
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.TeamNameValidator
import javax.inject.Inject

class ChooseNameViewModel
@Inject constructor(private val validator: TeamNameValidator) : DemCouchViewModel() {

    val name = ValidatedField({ name: String -> validator.validate(name) })

    override fun onCleared() {
        name.release()
        super.onCleared()
    }
}