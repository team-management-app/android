package com.tomaszstankowski.demcouch.main.features.createteam

import android.net.Uri
import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.repository.team.TeamRepository
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class CreateTeamViewModel
@Inject constructor(private val teamRepository: TeamRepository,
                    private val authService: AuthenticationService)
    : DemCouchViewModel() {

    var currentStep: Int = 0
        set(value) {
            _ready.onNext(value == 4)
            field = value
        }

    var name: String? = null
    var description: String? = null
    var discipline: Discipline? = null
    var location: Location? = null
    var thumbnail: Uri? = null

    private val _ready = BehaviorSubject.create<Boolean>()
    val ready: Flowable<Boolean>
        get() = _ready.toFlowable(BackpressureStrategy.LATEST)

    val createTeam = Action()

    fun createTeam() {
        val disposable = createTeam.invoke(
                teamRepository.createTeam(name!!, description, location!!, discipline!!))
                .flatMapCompletable {
                    val thumbnailSnapshot = thumbnail
                    if (thumbnailSnapshot == null) {
                        Completable.complete()
                    } else {
                        teamRepository.updateThumbnail(it.id, thumbnailSnapshot)
                                .doOnError { t -> Timber.e(t, "Failed to upload thumbnail") }
                                .onErrorComplete()
                    }
                }
                .subscribe({
                }, {
                    Timber.e(it, "Failed to create team")
                })
        compositeDisposable.add(disposable)
    }
}