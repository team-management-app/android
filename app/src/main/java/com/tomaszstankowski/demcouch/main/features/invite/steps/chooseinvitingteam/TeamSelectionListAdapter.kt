package com.tomaszstankowski.demcouch.main.features.invite.steps.chooseinvitingteam

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.ui.adapters.EmptyListViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.ErrorViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.FixedLengthAdapter
import com.tomaszstankowski.demcouch.main.ui.adapters.PlaceholderViewHolder
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl

class TeamSelectionListAdapter(private val context: Context) : FixedLengthAdapter<TeamSelection>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return when (viewType) {
            ITEM_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.team_selection_list_item, parent, false)
                TeamSelectionViewHolder(view)
            }
            PLACE_HOLDER_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.team_selection_list_item_placeholder, parent, false)
                PlaceholderViewHolder(view)
            }
            EMPTY_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_empty_layout, parent, false)
                EmptyListViewHolder(view)
            }
            ERROR_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_error_layout, parent, false)
                ErrorViewHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, pos: Int) {
        when (holder) {
            is TeamSelectionViewHolder -> {
                val item = items.data!![pos]
                item.apply {
                    holder.thumbnail.loadThumbnailFromUrl(team.thumbnailUrl)
                    holder.name.text = team.name
                    holder.location.text = team.location.name
                    holder.radioButton.isChecked = selected
                }
                holder.itemView.setOnClickListener {
                    onItemClickListener?.invoke(item, pos)
                }
            }
            is EmptyListViewHolder -> holder.text.text = context.getString(R.string.choose_inviting_team_no_items)
        }
    }

    private class TeamSelectionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val thumbnail: ImageView = view.findViewById(R.id.teamSelectionListItemThumbnail)
        val name: TextView = view.findViewById(R.id.teamSelectionListItemName)
        val location: TextView = view.findViewById(R.id.teamSelectionListItemLocation)
        val radioButton: RadioButton = view.findViewById(R.id.teamSelectionListItemRadioButton)
    }
}