package com.tomaszstankowski.demcouch.main.repository.user

import android.net.Uri
import com.tomaszstankowski.demcouch.main.cache.teammembership.TeamMembershipDao
import com.tomaszstankowski.demcouch.main.cache.user.UserCache
import com.tomaszstankowski.demcouch.main.cache.user.UserDao
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.misc.ThumbnailService
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.ResourceFetchMemory
import com.tomaszstankowski.demcouch.main.repository.combineCacheWithApi
import com.tomaszstankowski.demcouch.main.web.core.model.ThumbnailResponse
import com.tomaszstankowski.demcouch.main.web.user.UserCreateRequest
import com.tomaszstankowski.demcouch.main.web.user.UserResponse
import com.tomaszstankowski.demcouch.main.web.user.UserService
import com.tomaszstankowski.demcouch.main.web.user.UserUpdateRequest
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class UserRepository
@Inject constructor(private val userService: UserService,
                    private val userDao: UserDao,
                    private val userMapper: UserMapper,
                    private val thumbnailService: ThumbnailService,
                    private val teamMembershipDao: TeamMembershipDao,
                    private val fetchMemory: ResourceFetchMemory) {

    private fun saveUserResponseInCache(userResponse: UserResponse) {
        val userCache = userMapper.mapApiResponseToCache(userResponse)
        userDao.insert(userCache)
    }

    private fun onUserNoLongerExistsInApi(id: Long) {
        teamMembershipDao.deleteMembershipsForUser(id)
        userDao.delete(id)
    }

    fun createUser(email: String, firstName: String, lastName: String, password: String): Single<User> {
        val request = UserCreateRequest(email, firstName, lastName, password)
        return userService.createUser(request)
                .map(userMapper::mapApiResponseToCache)
                .doOnSuccess(userDao::insert)
                .map(userMapper::mapCacheToDomain)
    }

    fun updateUser(user: User): Completable {
        val request = UserUpdateRequest(user.firstName, user.lastName, user.bornDate)
        return userService.updateUser(user.id, request)
                .map(userMapper::mapApiResponseToCache)
                .doOnSuccess(userDao::insert)
                .flatMapCompletable { Completable.complete() }
    }

    fun updateThumbnail(userId: Long, thumbnailUri: Uri): Single<String> {
        return Single.fromCallable {
            thumbnailService.prepareMultipartBody(thumbnailUri)
        }
                .flatMap { body ->
                    userService.uploadThumbnail(userId.toString(), body)
                }
                .map(ThumbnailResponse::thumbnail)
                .zipWith(userDao.findById(userId).firstOrError(), BiFunction { thumbnailUrl: String?, user: UserCache ->
                    user.run {
                        UserCache(userId, firstName, lastName, email, thumbnailService.normalizeThumbnailUrl(thumbnailUrl))
                    }
                })
                .doOnSuccess(userDao::insert)
                .map(UserCache::thumbnailUrl)
    }

    fun deleteThumbnail(userId: Long): Completable {
        return userService.deleteThumbnail(userId.toString())
                .andThen(
                        userDao.findById(userId)
                                .firstOrError()
                                .map { it.run { UserCache(id, firstName, lastName, email, null) } }
                                .doOnSuccess(userDao::insert)
                                .flatMapCompletable { Completable.complete() }
                                .onErrorComplete())
    }

    fun getUser(id: Long): Flowable<Resource<User>> {
        return combineCacheWithApi(
                cacheSource = userDao.findById(id),
                apiSource = userService.getUser(id.toString()),
                onNotFoundInApi = { onUserNoLongerExistsInApi(id) },
                saveInCache = this::saveUserResponseInCache,
                mapCacheToDomain = userMapper::mapCacheToDomain,
                shouldFetch = { fetchMemory.shouldFetch("users/$id") },
                onFetchCompleted = { fetchMemory.onResourceFetched("users/$id") }
        )
    }

    fun setUserNotFresh(id: Long) {
        fetchMemory.onResourceNeedsFetch("users/$id")
    }

    fun deleteUser(id: Long): Completable {
        return userService.deleteUser(id)
                .doOnComplete { onUserNoLongerExistsInApi(id) }
    }
}