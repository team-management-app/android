package com.tomaszstankowski.demcouch.main.di

import com.tomaszstankowski.demcouch.main.DemCouchApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    ViewBinderModule::class,
    ViewModelBinderModule::class,
    AndroidModule::class,
    WebModule::class,
    CacheModule::class,
    LocationModule::class,
    RepositoryModule::class])
interface DemCouchAppComponent : AndroidInjector<DemCouchApp> {

    override fun inject(application: DemCouchApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: DemCouchApp): Builder

        fun build(): DemCouchAppComponent
    }
}