package com.tomaszstankowski.demcouch.main.features.notifications.list

import android.content.Context
import android.view.View
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.domain.notification.TeamJoinRequestNotification
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl

class TeamJoinRequestViewHolder(private val context: Context, view: View)
    : NotificationViewHolder(view) {

    override fun bind(item: Notification) {
        val notification = item as TeamJoinRequestNotification
        thumbnail.loadThumbnailFromUrl(notification.requestingUser.thumbnail)
        text.text = context.getString(
                R.string.notification_team_join_request,
                notification.requestingUser.firstName,
                notification.requestingUser.lastName)
        super.bind(item)
    }
}