package com.tomaszstankowski.demcouch.main.repository.notification

import com.squareup.moshi.Moshi
import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationDao
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.domain.notification.NotificationType
import com.tomaszstankowski.demcouch.main.repository.match.MatchRepository
import com.tomaszstankowski.demcouch.main.repository.matchinvitation.MatchInvitationRepository
import com.tomaszstankowski.demcouch.main.repository.notification.model.NotificationMapper
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers
import com.tomaszstankowski.demcouch.main.web.matchinvitation.MatchInvitationResponse
import com.tomaszstankowski.demcouch.main.web.notifications.NotificationResponse
import com.tomaszstankowski.demcouch.main.web.notifications.NotificationService
import com.tomaszstankowski.demcouch.main.web.teammembership.TeamMembershipResponse
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NotificationRepository
@Inject constructor(private val notificationDao: NotificationDao,
                    private val notificationService: NotificationService,
                    private val notificationMapper: NotificationMapper,
                    private val authService: AuthenticationService,
                    private val moshi: Moshi,
                    private val teamMembershipRepository: TeamMembershipRepository,
                    private val matchInvitationRepository: MatchInvitationRepository,
                    private val matchRepository: MatchRepository,
                    private val config: Config) {

    private var disposable: Disposable? = null

    private fun handleNotification(notification: NotificationResponse) {
        when (notification.type) {
            NotificationType.TEAM_JOIN_REQUEST,
            NotificationType.TEAM_MEMBERSHIP_APPROVAL -> {
                val teamMembershipResponse = moshi.adapter(TeamMembershipResponse::class.java)
                        .fromJsonValue(notification.data["team_membership"])!!
                teamMembershipRepository.saveTeamMembershipInCache(teamMembershipResponse)
            }
            NotificationType.TEAM_MEMBERSHIP_REJECTION -> {
                //TODO check response
            }
            NotificationType.MATCH_INVITATION,
            NotificationType.MATCH_INVITATION_REJECTION -> {
                val matchInvitationResponse = moshi.adapter(MatchInvitationResponse::class.java)
                        .fromJsonValue(notification.data["invitation"])!!
                matchInvitationRepository.saveMatchInvitationInCache(matchInvitationResponse)
            }
            NotificationType.MATCH_INVITATION_APPROVAL -> {
                val matchInvitationResponse = moshi.adapter(MatchInvitationResponse::class.java)
                        .fromJsonValue(notification.data["invitation"])!!
                matchInvitationRepository.saveMatchInvitationInCache(matchInvitationResponse)
                matchRepository.setMatchesForUserNotFresh(authService.userId)
            }
        }
    }

    private fun saveNotifications(notifications: List<NotificationResponse>, userId: Long): Completable {
        return Completable.create { emitter ->
            val notificationsCacheList = notifications.map {
                notificationMapper.mapApiResponseToCache(it, userId)
            }
            notificationDao.insertAll(notificationsCacheList)
            emitter.onComplete()
        }
    }

    fun startListeningForNotifications() {
        disposable = Flowable.interval(config.fetchIntervalSeconds, TimeUnit.SECONDS)
                .map { authService.userId }
                .filter { it > 0 }
                .flatMapSingle(notificationService::getUserNotifications)
                .doOnNext { list -> list.forEach(this::handleNotification) }
                .flatMapCompletable { saveNotifications(it, authService.userId) }
                .doOnError { Timber.e(it, "Failed to fetch notifications") }
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .retry()
                .subscribe()
    }

    fun stopListeningForNotifications() {
        disposable?.dispose()
    }

    fun getNotificationsForUser(id: Long): Flowable<List<Notification>> {
        return notificationDao.getUserNotifications(id)
                .map { list -> list.map(notificationMapper::mapCacheToDomain) }
    }

    fun getUnreadNotificationsCountForUser(id: Long): Flowable<Int> {
        return notificationDao.getUnreadNotificationCountForUser(id)
    }

    fun markNotificationAsRead(id: Long): Completable {
        return notificationService.delete(id)
                .andThen(notificationDao.findById(id).firstOrError())
                .map {
                    it.run {
                        NotificationCache(id, userId, createDate, data, type, true)
                    }
                }
                .flatMapCompletable {
                    notificationDao.insert(it)
                    Completable.complete()
                }
    }

    class Config(val fetchIntervalSeconds: Long = 60)
}