package com.tomaszstankowski.demcouch.main.features.invite.steps.chooseinvitingteam

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_choose_inviting_team.*

class ChooseInvitingTeamFragment : DemCouchFragment() {

    private lateinit var viewModel: ChooseInvitingTeamViewModel
    private lateinit var adapter: TeamSelectionListAdapter

    companion object {
        fun instance(invitedTeamId: Long, invitedTeamDiscipline: Discipline): ChooseInvitingTeamFragment {
            val args = Bundle()
            args.putLong(INVITED_TEAM_ID_KEY, invitedTeamId)
            args.putString(INVITED_TEAM_DISCIPLINE_KEY, invitedTeamDiscipline.toString())
            val fragment = ChooseInvitingTeamFragment()
            fragment.arguments = args
            return fragment
        }

        private const val INVITED_TEAM_ID_KEY = "invitedTeamId"
        private const val INVITED_TEAM_DISCIPLINE_KEY = "invitedTeamDiscipline"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_choose_inviting_team, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()
        val invitedTeamId = arguments?.getLong(INVITED_TEAM_ID_KEY)
                ?: throw IllegalStateException("No '$INVITED_TEAM_ID_KEY' param passed to fragment")
        val invitedTeamDiscipline = Discipline.valueOf(
                arguments?.getString(INVITED_TEAM_DISCIPLINE_KEY)
                        ?: throw java.lang.IllegalStateException("No '$INVITED_TEAM_DISCIPLINE_KEY' param passed to fragment"))
        viewModel.init(invitedTeamId, invitedTeamDiscipline)
        adapter = TeamSelectionListAdapter(context!!)
        fragmentChooseInvitingTeamRecyclerView.adapter = adapter
        fragmentChooseInvitingTeamRecyclerView.layoutManager = LinearLayoutManager(context!!)
        adapter.onItemClickListener = { item, _ ->
            viewModel.onTeamSelected(item.team)
            (activity as OnTeamSelectedListener?)?.onTeamSelected(item.team)
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.teams, adapter::items::set)
    }
}