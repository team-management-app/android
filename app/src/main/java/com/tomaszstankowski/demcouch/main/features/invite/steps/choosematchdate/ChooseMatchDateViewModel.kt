package com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchdate

import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import org.threeten.bp.ZonedDateTime
import javax.inject.Inject

class ChooseMatchDateViewModel @Inject constructor() : DemCouchViewModel() {

    private fun emitNewDateOrError(date: ZonedDateTime) {
        if (date > maxDate || date < minDate) {
            _invalidDate.onNext(date)
            _date.onNext(_date.value!!)
        } else {
            _date.onNext(date)
        }
    }

    val minDate: ZonedDateTime = ZonedDateTime.now().plusHours(1)
    val maxDate: ZonedDateTime = ZonedDateTime.now().plusMonths(1)

    private val _date = BehaviorSubject.create<ZonedDateTime>()
    val date: Flowable<ZonedDateTime>
        get() = _date.toFlowable(BackpressureStrategy.LATEST)
    val dateSnapshot: ZonedDateTime
        get() = _date.value!!

    private val _invalidDate = PublishSubject.create<ZonedDateTime>()
    val invalidDate: Flowable<ZonedDateTime>
        get() = _invalidDate.toFlowable(BackpressureStrategy.LATEST)

    init {
        _date.onNext(minDate)
    }

    fun onDateSelected(year: Int, month: Int, day: Int) {
        val newDate = dateSnapshot
                .withYear(year)
                .withMonth(month)
                .withDayOfMonth(day)
        emitNewDateOrError(newDate)
    }

    fun onTimeSelected(hour: Int, minute: Int) {
        val newDate = dateSnapshot
                .withHour(hour)
                .withMinute(minute)
        emitNewDateOrError(newDate)
    }
}