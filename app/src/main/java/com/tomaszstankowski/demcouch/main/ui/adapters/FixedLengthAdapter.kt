package com.tomaszstankowski.demcouch.main.ui.adapters

import android.support.v7.widget.RecyclerView
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource

abstract class FixedLengthAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var init = false
    var items: Resource<List<T>> = Resource(FetchStatus.LOADING, null)
        set(value) {
            field = value
            init = true
            notifyDataSetChanged()
        }

    var onItemClickListener: ((item: T, position: Int) -> Unit)? = null

    companion object {
        const val ITEM_VIEW_TYPE = 1
        const val PLACE_HOLDER_VIEW_TYPE = 2
        const val ERROR_VIEW_TYPE = 3
        const val EMPTY_VIEW_TYPE = 4
        private const val PLACEHOLDERS_COUNT = 6
    }

    override fun getItemViewType(position: Int): Int {
        return if (items.status == FetchStatus.LOADING && items.data?.isEmpty() != false)
            PLACE_HOLDER_VIEW_TYPE
        else if (items.status == FetchStatus.FAIL && items.data?.isEmpty() != false)
            ERROR_VIEW_TYPE
        else if (items.status == FetchStatus.SUCCESS && items.data?.isEmpty() != false)
            EMPTY_VIEW_TYPE
        else
            ITEM_VIEW_TYPE
    }

    override fun getItemCount(): Int {
        return if (!init) {
            0
        } else if (items.data?.isEmpty() == false) {
            items.data!!.size
        } else {
            if (items.status == FetchStatus.LOADING) PLACEHOLDERS_COUNT
            else 1
        }
    }


}