package com.tomaszstankowski.demcouch.main.web.core.interceptors

import com.tomaszstankowski.demcouch.main.auth.LogOutEventEmitter
import com.tomaszstankowski.demcouch.main.auth.UserCacheService
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import javax.inject.Inject

class TokenInterceptor @Inject constructor(private val userCacheService: UserCacheService,
                                           private val logOutEventEmitter: LogOutEventEmitter)
    : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val token = userCacheService.token
        val proceededRequest = if (token == null) request else
            request.newBuilder()
                    .addHeader("Authorization", "JWT $token")
                    .build()
        val response = chain.proceed(proceededRequest)
        if (response.code() == 401) {
            Timber.i("Token expired")
            userCacheService.invalidateCurrentUser()
            logOutEventEmitter.emit()
        }
        return response
    }
}