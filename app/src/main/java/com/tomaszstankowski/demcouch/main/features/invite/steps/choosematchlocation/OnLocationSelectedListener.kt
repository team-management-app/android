package com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation

interface OnLocationSelectedListener {
    fun onLocationSelected(coordinates: Coordinates)
}