package com.tomaszstankowski.demcouch.main.web.match

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface MatchService {

    @GET("/users/{id}/matches")
    fun getMatchesForUsers(@Path("id") id: String): Single<List<MatchResponse>>

    @GET("matches/{id}")
    fun getMatch(@Path("id") id: String): Single<MatchResponse>
}