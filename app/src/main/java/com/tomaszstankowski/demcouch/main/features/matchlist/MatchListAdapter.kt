package com.tomaszstankowski.demcouch.main.features.matchlist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Match
import com.tomaszstankowski.demcouch.main.ui.adapters.EmptyListViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.ErrorViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.FixedLengthAdapter
import com.tomaszstankowski.demcouch.main.ui.adapters.PlaceholderViewHolder
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.toMillis

class MatchListAdapter(private val context: Context) : FixedLengthAdapter<Match>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return when (viewType) {
            ITEM_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.match_list_item, parent, false)
                MatchListViewHolder(view)
            }
            PLACE_HOLDER_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.match_list_item_placeholder, parent, false)
                PlaceholderViewHolder(view)
            }
            EMPTY_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_empty_layout, parent, false)
                EmptyListViewHolder(view)
            }
            ERROR_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_error_layout, parent, false)
                ErrorViewHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, pos: Int) {
        when (holder) {
            is MatchListViewHolder -> {
                val item = items.data!![pos]
                item.apply {
                    holder.hostTeamName.text = hostTeam.name
                    holder.hostTeamThumbnail.loadThumbnailFromUrl(hostTeam.thumbnailUrl)
                    holder.guestTeamName.text = guestTeam.name
                    holder.guestTeamThumbnail.loadThumbnailFromUrl(guestTeam.thumbnailUrl)
                    holder.date.text = DateUtils.getRelativeTimeSpanString(date.toMillis())
                    holder.location.text = location.name
                }
                holder.itemView.setOnClickListener {
                    onItemClickListener?.invoke(item, pos)
                }
            }
            is EmptyListViewHolder -> holder.text.text = context.getString(R.string.match_list_empty)
        }
    }

    private class MatchListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val hostTeamName: TextView = view.findViewById(R.id.matchListItemHostTeamName)
        val guestTeamName: TextView = view.findViewById(R.id.matchListItemGuestTeamName)
        val hostTeamThumbnail: ImageView = view.findViewById(R.id.matchListItemHostTeamThumbnail)
        val guestTeamThumbnail: ImageView = view.findViewById(R.id.matchListItemGuestTeamThumbnail)
        val date: TextView = view.findViewById(R.id.matchListItemDate)
        val location: TextView = view.findViewById(R.id.matchListItemLocation)
    }
}