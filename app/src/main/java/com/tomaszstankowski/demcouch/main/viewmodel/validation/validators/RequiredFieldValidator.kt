package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import io.reactivex.Single
import javax.inject.Inject

class RequiredFieldValidator @Inject constructor() {

    fun validate(text: String): Single<FieldStatus> {
        return Single.create {
            val status = if (text.isEmpty())
                FieldStatus.error(ValidationError.REQUIRED, null)
            else FieldStatus.correct()
            it.onSuccess(status)
        }
    }
}