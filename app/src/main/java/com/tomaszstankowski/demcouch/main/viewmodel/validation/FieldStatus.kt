package com.tomaszstankowski.demcouch.main.viewmodel.validation

class FieldStatus private constructor(val isValid: Boolean, val error: ValidationError?, val data: Any?) {
    companion object {
        fun correct() = FieldStatus(true, null, null)
        fun error(error: ValidationError, data: Any? = null) = FieldStatus(false, error, data)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FieldStatus

        if (isValid != other.isValid) return false
        if (error != other.error) return false
        if (data != other.data) return false

        return true
    }

    override fun hashCode(): Int {
        var result = isValid.hashCode()
        result = 31 * result + (error?.hashCode() ?: 0)
        result = 31 * result + (data?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "FieldStatus(isValid=$isValid, error=$error, data=$data)"
    }


}
