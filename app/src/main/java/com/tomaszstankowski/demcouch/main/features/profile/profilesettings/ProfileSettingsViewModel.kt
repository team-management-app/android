package com.tomaszstankowski.demcouch.main.features.profile.profilesettings

import android.net.Uri
import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.repository.user.UserRepository
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import com.tomaszstankowski.demcouch.main.viewmodel.validation.Form
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidatedField
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.NameValidator
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import org.threeten.bp.LocalDate
import timber.log.Timber
import javax.inject.Inject

class ProfileSettingsViewModel
@Inject constructor(private val repo: UserRepository,
                    private val authenticationService: AuthenticationService,
                    nameValidator: NameValidator) : DemCouchViewModel() {

    lateinit var user: User

    val firstName = ValidatedField(nameValidator::validate)
    val lastName = ValidatedField(nameValidator::validate)

    val minBornDate: LocalDate = LocalDate.of(1940, 1, 1)
    val maxBornDate: LocalDate = LocalDate.now().minusYears(5)
    private val _bornDate = BehaviorSubject.create<LocalDate>()
    val bornDate: Flowable<LocalDate>
        get() = _bornDate.toFlowable(BackpressureStrategy.LATEST)

    val form = Form(
            Form.Field(firstName),
            Form.Field(lastName))

    val saveChanges = Action()

    val changeThumbnail = Action()

    val deleteThumbnail = Action()

    private val _thumbnailSelection = BehaviorSubject.create<ThumbnailSelection>()
    val thumbnailSelection: Flowable<ThumbnailSelection>
        get() = _thumbnailSelection.toFlowable(BackpressureStrategy.LATEST)

    fun onBornDateChanged(bornDate: LocalDate) {
        if (bornDate != _bornDate.value) {
            _bornDate.onNext(bornDate)
        }
    }

    fun getUser(): Single<User> {
        return repo.getUser(authenticationService.userId)
                .firstOrError()
                .map { it.data!! }
                .doOnSuccess { user = it }
                .doOnSuccess {
                    if (it.thumbnail == null)
                        _thumbnailSelection.onNext(ThumbnailSelection.removed())
                    else
                        _thumbnailSelection.onNext(ThumbnailSelection.selected(it.thumbnail))
                }
    }

    fun onSaveChangesClicked() {
        val updateCompletable = Single.create<User> {
            val updated = user.run {
                User(
                        id,
                        this@ProfileSettingsViewModel.firstName.value!!,
                        this@ProfileSettingsViewModel.lastName.value!!,
                        email,
                        thumbnail,
                        _bornDate.value)
            }
            it.onSuccess(updated)
        }
                .flatMapCompletable(repo::updateUser)
        val disposable = saveChanges.invoke(updateCompletable)
                .subscribe({
                }, {
                    Timber.e(it, "Error while updating account")
                })
        compositeDisposable.add(disposable)
    }

    fun changeThumbnail(uri: Uri) {
        val disposable = changeThumbnail.invoke(repo.updateThumbnail(user.id, uri))
                .subscribe({
                    _thumbnailSelection.onNext(ThumbnailSelection.selected(it))
                }, {
                    Timber.e(it, "Error while changing thumbnail")
                })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        form.release()
        super.onCleared()
    }

    fun onRemoveThumbnailClicked() {
        val disposable = deleteThumbnail.invoke(repo.deleteThumbnail(user.id))
                .subscribe({
                    _thumbnailSelection.onNext(ThumbnailSelection.removed())
                }, {
                    Timber.e(it, "Error while removing thumbnail")
                })
        compositeDisposable.add(disposable)
    }
}

class ThumbnailSelection private constructor(val url: String?) {
    companion object {
        fun selected(url: String) = ThumbnailSelection(url)
        fun removed() = ThumbnailSelection(null)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ThumbnailSelection

        if (url != other.url) return false

        return true
    }

    override fun hashCode(): Int {
        return url?.hashCode() ?: 0
    }


}