package com.tomaszstankowski.demcouch.main.features.createteam

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Toast
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosedescription.ChooseDescriptionFragment
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosediscipline.ChooseDisciplineFragment
import com.tomaszstankowski.demcouch.main.features.createteam.steps.chooselocation.ChooseLocationFragment
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosename.ChooseNameFragment
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosethumbnail.ChooseThumbnailFragment
import com.tomaszstankowski.demcouch.main.ui.activities.DemCouchActivity
import kotlinx.android.synthetic.main.activity_create_team.*

class CreateTeamActivity : DemCouchActivity(), ValidationStateListener {

    private lateinit var viewModel: CreateTeamViewModel

    override fun onValidationStatusChanged(valid: Boolean) {
        activityCreateTeamNextButton.isEnabled = valid
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_team)

        viewModel = getViewModel()

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(
                            R.id.activityCreateTeamFrameLayout,
                            ChooseNameFragment.getInstance(null))
                    .commit()
        }

        activityInviteStepView.go(viewModel.currentStep, false)

        activityCreateTeamBackButton.setOnClickListener { previousStep() }
        activityCreateTeamNextButton.setOnClickListener { nextStep() }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.createTeam.loading, {
            activityCreateTeamProgressBar.visibility = if (it) View.VISIBLE else View.GONE
            activityCreateTeamNextButton.isEnabled = !it
        })
        subscribe(viewModel.createTeam.failure, {
            Toast.makeText(this, R.string.create_team_activity_error, Toast.LENGTH_LONG).show()
        })
        subscribe(viewModel.createTeam.success, {
            setResult(Activity.RESULT_OK)
            finish()
        })
        subscribe(viewModel.ready, {
            if (it) {
                activityCreateTeamNextButton.setText(R.string.create_team_activity_create_team)
            } else {
                activityCreateTeamNextButton.setText(R.string.next)
            }
        })
    }

    private fun nextStep() {
        val newFragment: Fragment
        when (viewModel.currentStep + 1) {
            1 -> {
                viewModel.name = (supportFragmentManager.findFragmentById(R.id.activityCreateTeamFrameLayout)
                        as ChooseNameFragment?)!!.name
                newFragment = ChooseDescriptionFragment.getInstance(viewModel.description)
            }
            2 -> {
                viewModel.description = (supportFragmentManager.findFragmentById(R.id.activityCreateTeamFrameLayout)
                        as ChooseDescriptionFragment?)!!.description
                newFragment = ChooseDisciplineFragment.getInstance(viewModel.discipline)
            }
            3 -> {
                viewModel.discipline = (supportFragmentManager.findFragmentById(R.id.activityCreateTeamFrameLayout)
                        as ChooseDisciplineFragment?)!!.discipline
                newFragment = ChooseLocationFragment.getInstance(viewModel.location)
            }
            4 -> {
                viewModel.location = (supportFragmentManager.findFragmentById(R.id.activityCreateTeamFrameLayout) as ChooseLocationFragment?)!!
                        .location
                newFragment = ChooseThumbnailFragment.getInstance(viewModel.thumbnail)
            }
            5 -> {
                viewModel.thumbnail = (supportFragmentManager.findFragmentById(R.id.activityCreateTeamFrameLayout)
                        as ChooseThumbnailFragment?)!!.thumbnail
                viewModel.createTeam()
                return
            }
            else -> throw IllegalStateException(String.format("Step number (%d) is not valid", viewModel.currentStep + 1))
        }
        viewModel.currentStep++

        supportFragmentManager.beginTransaction()
                .replace(R.id.activityCreateTeamFrameLayout, newFragment)
                .commit()
        activityCreateTeamNextButton.isEnabled = false
        activityInviteStepView.go(viewModel.currentStep, true)
    }

    private fun previousStep() {
        val fragment: Fragment
        when (viewModel.currentStep - 1) {
            -1 -> {
                onBackPressed()
                return
            }
            0 -> fragment = ChooseNameFragment.getInstance(viewModel.name)
            1 -> fragment = ChooseDescriptionFragment.getInstance(viewModel.description)
            2 -> fragment = ChooseDisciplineFragment.getInstance(viewModel.discipline)
            3 -> {
                fragment = ChooseLocationFragment.getInstance(viewModel.location)
                viewModel.thumbnail = (supportFragmentManager.findFragmentById(R.id.activityCreateTeamFrameLayout)
                        as ChooseThumbnailFragment?)!!
                        .thumbnail
            }
            else -> throw IllegalStateException(String.format("Step number (%d) is not valid", viewModel.currentStep - 1))
        }
        viewModel.currentStep--

        supportFragmentManager.beginTransaction()
                .replace(R.id.activityCreateTeamFrameLayout, fragment)
                .commit()
        activityCreateTeamNextButton.isEnabled = false
        activityInviteStepView.go(viewModel.currentStep, true)
    }
}