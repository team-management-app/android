package com.tomaszstankowski.demcouch.main.features.profile.profilesettings.changepassword

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import com.tomaszstankowski.demcouch.main.viewmodel.validation.Form
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidatedField
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.PasswordMatchValidator
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.PasswordValidator
import timber.log.Timber
import javax.inject.Inject

class ChangePasswordViewModel
@Inject constructor(private val authService: AuthenticationService,
                    passwordValidator: PasswordValidator,
                    passwordMatchValidator: PasswordMatchValidator) : DemCouchViewModel() {
    val password: ValidatedField<String> = ValidatedField({ password ->
        passwordRepeat.revalidate()
        passwordValidator.validate(password)
    })
    val passwordRepeat: ValidatedField<String> = ValidatedField({ passwordMatch ->
        passwordMatchValidator.validate(password.value ?: "", passwordMatch)
    })

    val form = Form(
            Form.Field(password, true),
            Form.Field(passwordRepeat, true))

    val changePassword = Action()

    fun onSavePasswordClicked() {
        val disposable = changePassword.invoke(authService.changePassword(password.value!!))
                .subscribe({ }, {
                    Timber.e(it, "Failed to change password")
                })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        form.release()
        super.onCleared()
    }
}