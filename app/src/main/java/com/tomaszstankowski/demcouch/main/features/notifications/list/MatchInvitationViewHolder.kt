package com.tomaszstankowski.demcouch.main.features.notifications.list

import android.content.Context
import android.view.View
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.notification.MatchInvitationNotification
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl

class MatchInvitationViewHolder(private val context: Context, view: View)
    : NotificationViewHolder(view) {

    override fun bind(item: Notification) {
        val notification = item as MatchInvitationNotification
        text.text = context.getString(R.string.notification_match_invitation, notification.matchInvitation.invitingTeam.name)
        thumbnail.loadThumbnailFromUrl(notification.matchInvitation.invitingTeam.thumbnailUrl)
        super.bind(item)
    }
}