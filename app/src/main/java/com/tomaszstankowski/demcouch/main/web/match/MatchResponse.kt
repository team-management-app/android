package com.tomaszstankowski.demcouch.main.web.match

import com.squareup.moshi.Json
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import org.threeten.bp.ZonedDateTime

data class MatchResponse(val id: Long,
                         val date: ZonedDateTime,
                         val location: Location,
                         @Json(name = "inviting_team") val hostTeam: TeamResponse,
                         @Json(name = "invited_team") val guestTeam: TeamResponse)