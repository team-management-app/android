package com.tomaszstankowski.demcouch.main.features.register

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.ui.addOnTextChangeListener
import com.tomaszstankowski.demcouch.main.ui.changeStatus
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.hideKeyboard
import com.tomaszstankowski.demcouch.main.ui.resetStatus
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : DemCouchFragment() {

    private lateinit var viewModel: RegisterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()

        fragmentRegisterEmailTIL.addOnTextChangeListener { text, view ->
            view.resetStatus()
            viewModel.email.postValue(text)
        }
        fragmentRegisterFirstNameTIL.addOnTextChangeListener { text, view ->
            view.resetStatus()
            viewModel.firstName.postValue(text)
        }
        fragmentRegisterLastNameTIL.addOnTextChangeListener { text, view ->
            view.resetStatus()
            viewModel.lastName.postValue(text)
        }
        fragmentRegisterPasswordTIL.addOnTextChangeListener { text, view ->
            view.resetStatus()
            viewModel.password.postValue(text)
        }
        fragmentRegisterRepeatPasswordTIL.addOnTextChangeListener { text, view ->
            view.resetStatus()
            viewModel.passwordRepetition.postValue(text)
        }
        fragmentRegisterAcceptTermsCB.setOnCheckedChangeListener { _, checked ->
            viewModel.termsAgreement.postValue(checked)
        }
        fragmentRegisterButton.setOnClickListener {
            hideKeyboard()
            viewModel.onRegisterButtonClicked()
        }
        fragmentRegisterBackButton.setOnClickListener {
            Navigation.findNavController(it).navigateUp()
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.register.success, {
            AlertDialog.Builder(context!!)
                    .setTitle(R.string.register_fragment_confirmation_link_dialog_title)
                    .setPositiveButton(R.string.ok) { _, _ ->
                    }
                    .setOnDismissListener { _ ->
                        Navigation.findNavController(activity!!, R.id.activitySignInNavHostFragment)
                                .navigateUp()
                    }
                    .create()
                    .show()
        })
        subscribe(viewModel.register.failure, {
            showMessage(R.string.error)
        })
        subscribe(viewModel.register.loading, {
            fragmentRegisterButton.isEnabled = !it
            fragmentRegisterProgressBar.visibility = if (it) View.VISIBLE else View.GONE
        })
        subscribe(viewModel.form.valid, {
            fragmentRegisterButton.isEnabled = it
        })
        subscribe(viewModel.email.status, fragmentRegisterEmailTIL::changeStatus)
        subscribe(viewModel.firstName.status, fragmentRegisterFirstNameTIL::changeStatus)
        subscribe(viewModel.lastName.status, fragmentRegisterLastNameTIL::changeStatus)
        subscribe(viewModel.password.status, fragmentRegisterPasswordTIL::changeStatus)
        subscribe(viewModel.passwordRepetition.status, fragmentRegisterRepeatPasswordTIL::changeStatus)
    }
}