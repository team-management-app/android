package com.tomaszstankowski.demcouch.main.features.matchinvitation

import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.MatchInvitation
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.toDisplayString
import com.tomaszstankowski.demcouch.main.ui.toMillis
import kotlinx.android.synthetic.main.fragment_match_invitation.*
import javax.inject.Inject

class MatchInvitationFragment : DemCouchFragment(), OnMapReadyCallback {

    @Inject
    lateinit var viewModel: MatchInvitationViewModel

    companion object {
        const val INVITATION_ID_KEY = "invitationId"
        private const val MAP_ZOOM = 15f
        private const val DEFAULT_MARKER_LAT = 54.3742012
        private const val DEFAULT_MARKER_LNG = 18.6073264
    }

    private var marker: Marker? = null
    private var map: GoogleMap? = null

    private fun updateMarkerAndMapPositionIfPresent(matchLocation: Location) {
        matchLocation.apply {
            val latLng = LatLng(lat, lng)
            marker?.position = latLng
            marker?.title = name
            marker?.showInfoWindow()
            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM))
        }
    }

    private fun displayInvitation(invitation: MatchInvitation) {
        invitation.apply {
            fragmentMatchInvitationInvitingTeamName.text = invitingTeam.name
            fragmentMatchInvitationYourTeamName.text = invitedTeam.name
            fragmentMatchInvitationDiscipline.text = invitingTeam.discipline.toDisplayString(activity!!)
            fragmentMatchInvitationDate.text = DateFormat.getLongDateFormat(activity!!).format(matchDate.toMillis())
            fragmentMatchInvitationTime.text = DateFormat.getTimeFormat(activity!!).format(matchDate.toMillis())
            fragmentMatchInvitationInvitingTeamThumbnail.loadThumbnailFromUrl(invitingTeam.thumbnailUrl)
            fragmentMatchInvitationYourTeamThumbnail.loadThumbnailFromUrl(invitedTeam.thumbnailUrl)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_match_invitation, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val invitationId = arguments?.getLong(INVITATION_ID_KEY)
                ?: throw IllegalStateException("No '$INVITATION_ID_KEY' arg passed to fragment")
        viewModel.init(invitationId)
        fragmentMatchInvitationAcceptButton.setOnClickListener {
            viewModel.onAcceptClicked()
        }
        fragmentMatchInvitationRejectButton.setOnClickListener {
            viewModel.onRejectClicked()
        }
        val mapFragment = childFragmentManager.findFragmentById(R.id.fragmentMatchInvitationMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.invitation, {
            if (it.data == null && it.status == FetchStatus.FAIL) {
                showMessage(R.string.error)
                Navigation.findNavController(activity!!, R.id.navHostFragment)
                        .navigateUp()
            } else if (it.data != null) {
                displayInvitation(it.data)
                updateMarkerAndMapPositionIfPresent(it.data.matchLocation)
            }
        })
        subscribe(viewModel.accept.success, {
            Navigation.findNavController(activity!!, R.id.navHostFragment)
                    .navigateUp()
        })
        subscribe(viewModel.accept.loading, {
            fragmentMatchInvitationAcceptButton.isEnabled = !it
            fragmentMatchInvitationRejectButton.isEnabled = !it
        })
        subscribe(viewModel.accept.failure, {
            showMessage(R.string.error)
        })
        subscribe(viewModel.reject.success, {
            Navigation.findNavController(activity!!, R.id.navHostFragment)
                    .navigateUp()
        })
        subscribe(viewModel.reject.loading, {
            fragmentMatchInvitationAcceptButton.isEnabled = !it
            fragmentMatchInvitationRejectButton.isEnabled = !it
        })
        subscribe(viewModel.reject.failure, {
            showMessage(R.string.error)
        })
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map
        val matchLocation = viewModel.invitationSnapshot?.data?.matchLocation
        val latLng = LatLng(
                matchLocation?.lat ?: DEFAULT_MARKER_LAT,
                matchLocation?.lng ?: DEFAULT_MARKER_LNG)
        val markerOptions = MarkerOptions()
                .position(latLng)
                .title(matchLocation?.name)
        marker = map?.addMarker(markerOptions)
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM))

        if (matchLocation != null) {
            updateMarkerAndMapPositionIfPresent(matchLocation)
        }
    }
}