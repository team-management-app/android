package com.tomaszstankowski.demcouch.main.features.register

import com.tomaszstankowski.demcouch.main.repository.user.UserRepository
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.Form
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidatedField
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.EmailValidator
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.NameValidator
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.PasswordMatchValidator
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.PasswordValidator
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class RegisterViewModel
@Inject constructor(emailValidator: EmailValidator,
                    passwordValidator: PasswordValidator,
                    passwordMatchValidator: PasswordMatchValidator,
                    nameValidator: NameValidator,
                    private val repo: UserRepository) : DemCouchViewModel() {

    val email = ValidatedField(emailValidator::validate)

    val firstName = ValidatedField(nameValidator::validate)

    val lastName = ValidatedField(nameValidator::validate)

    val password: ValidatedField<String> = ValidatedField({ password ->
        passwordRepetition.revalidate()
        passwordValidator.validate(password)
    })

    val passwordRepetition: ValidatedField<String> = ValidatedField({ passwordRep ->
        passwordMatchValidator.validate(password.value ?: "", passwordRep)
    })

    val termsAgreement = ValidatedField({ agreed: Boolean ->
        Single.create<FieldStatus> {
            if (agreed)
                it.onSuccess(FieldStatus.correct())
            else
                it.onSuccess(FieldStatus.error(ValidationError.REQUIRED, null))
        }
    }, 0)

    val form = Form(
            Form.Field(email, true),
            Form.Field(firstName, true),
            Form.Field(lastName, true),
            Form.Field(password, true),
            Form.Field(passwordRepetition, true),
            Form.Field(termsAgreement, true)
    )

    val register = Action()

    fun onRegisterButtonClicked() {
        val disposable = register.invoke(
                repo.createUser(email.value!!, firstName.value!!, lastName.value!!, password.value!!))
                .subscribe({
                }, {
                    Timber.e(it, "Error while registering account")
                })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        form.release()
        super.onCleared()
    }
}