package com.tomaszstankowski.demcouch.main.repository.team

import android.net.Uri
import com.tomaszstankowski.demcouch.main.cache.team.TeamCache
import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.cache.teammembership.TeamMembershipDao
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.misc.ThumbnailService
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.ResourceFetchMemory
import com.tomaszstankowski.demcouch.main.repository.combineCacheWithApi
import com.tomaszstankowski.demcouch.main.web.core.model.PageResponse
import com.tomaszstankowski.demcouch.main.web.team.TeamRequest
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import com.tomaszstankowski.demcouch.main.web.team.TeamService
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class TeamRepository @Inject constructor(private val teamService: TeamService,
                                         private val teamDao: TeamDao,
                                         private val teamMapper: TeamMapper,
                                         private val thumbnailService: ThumbnailService,
                                         private val teamMembershipDao: TeamMembershipDao,
                                         private val fetchMemory: ResourceFetchMemory) {

    private fun saveTeamInCache(teamResponse: TeamResponse) {
        val teamCache = teamMapper.mapApiResponseToCache(teamResponse)
        teamDao.insert(teamCache)
    }

    private fun onTeamNoLongerExistInApi(id: Long) {
        teamMembershipDao.deleteMembershipsForTeam(id)
        teamDao.delete(id)
    }

    fun createTeam(name: String, description: String?, location: Location, discipline: Discipline): Single<Team> {
        val request = TeamRequest(name, description ?: "", location, discipline)
        return teamService.createTeam(request)
                .map(teamMapper::mapApiResponseToCache)
                .doOnSuccess {
                    teamDao.insert(it)
                    fetchMemory.onResourceNeedsFetch("teams/${it.id}/team-memberships")
                    fetchMemory.onResourceNeedsFetch("users/${it.id}/team-memberships")
                }
                .map(teamMapper::mapCacheToDomain)
    }

    fun updateTeam(team: Team): Completable {
        val request = team.run {
            TeamRequest(name, desc ?: "", location, discipline)
        }
        return teamService.updateTeam(team.id.toString(), request)
                .doOnSuccess(this::saveTeamInCache)
                .flatMapCompletable { Completable.complete() }
    }

    fun deleteTeam(id: Long): Completable {
        return teamService.deleteTeam(id.toString())
                .doOnComplete { onTeamNoLongerExistInApi(id) }
    }

    fun updateThumbnail(teamId: Long, thumbnailUri: Uri): Completable {
        return Single.fromCallable {
            thumbnailService.prepareMultipartBody(thumbnailUri)
        }
                .flatMap { body ->
                    teamService.uploadThumbnail(teamId.toString(), body)
                }
                .map { it.thumbnail }
                .zipWith(teamDao.findById(teamId).firstOrError(), BiFunction { thumbnailUrl: String?, team: TeamCache ->
                    team.run {
                        TeamCache(teamId, name, desc, location, discipline, thumbnailService.normalizeThumbnailUrl(thumbnailUrl))
                    }
                })
                .doOnSuccess {
                    teamDao.insert(it)
                }
                .flatMapCompletable { Completable.complete() }
    }

    fun deleteThumbnail(teamId: Long): Completable {
        return teamService.deleteThumbnail(teamId.toString())
                .toSingleDefault(Any())
                .zipWith(teamDao.findById(teamId).firstOrError(), BiFunction { _: Any, cache: TeamCache ->
                    cache.run {
                        TeamCache(id, name, desc, location, discipline, null)
                    }
                })
                .doOnSuccess(teamDao::insert)
                .flatMapCompletable { Completable.complete() }
    }

    // Fetches teams directly from API
    fun findTeams(pageNumber: Int = 1,
                  pageSize: Int = 20,
                  name: String? = null,
                  locationQuery: LocationQuery? = null): Flowable<Resource<PageResponse<Team>>> {
        val queryParam = name?.plus('%') ?: "%"
        return teamService.getTeams(pageNumber, pageSize, queryParam,
                locationQuery?.lat, locationQuery?.lng, locationQuery?.radius)
                .map { PageResponse(it.data.map(teamMapper::mapApiResponseToCache), it.pagination) }
                .doOnSuccess { teamDao.insertAll(it.data) }
                .map { PageResponse(it.data.map(teamMapper::mapCacheToDomain), it.pagination) }
                .toFlowable()
                .map { Resource(FetchStatus.SUCCESS, it) }
                .startWith(Resource<PageResponse<Team>>(FetchStatus.LOADING, null))
                .onErrorReturnItem(Resource(FetchStatus.FAIL, null))
    }

    fun getTeam(id: Long): Flowable<Resource<Team>> {
        return combineCacheWithApi(
                cacheSource = teamDao.findById(id),
                apiSource = teamService.getTeam(id.toString()),
                onNotFoundInApi = { onTeamNoLongerExistInApi(id) },
                saveInCache = this::saveTeamInCache,
                mapCacheToDomain = teamMapper::mapCacheToDomain,
                shouldFetch = { fetchMemory.shouldFetch("teams/$id") },
                onFetchCompleted = { fetchMemory.onResourceFetched("teams/$id") })
    }

    data class LocationQuery(val lat: Float, val lng: Float, val radius: Float)
}