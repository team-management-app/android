package com.tomaszstankowski.demcouch.main.web.teammembership

import com.squareup.moshi.Json

data class TeamMembershipCreateRequestBody(@Json(name = "team_id") val teamId: Long)