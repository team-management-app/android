package com.tomaszstankowski.demcouch.main.di

import android.content.Context
import com.tomaszstankowski.demcouch.main.DemCouchApp
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @Binds
    @Suppress("unused")
    abstract fun provideContext(application: DemCouchApp): Context
}