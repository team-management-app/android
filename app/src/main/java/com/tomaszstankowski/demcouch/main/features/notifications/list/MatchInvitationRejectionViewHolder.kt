package com.tomaszstankowski.demcouch.main.features.notifications.list

import android.content.Context
import android.view.View
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.notification.MatchInvitationRejectionNotification
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl

class MatchInvitationRejectionViewHolder(private val context: Context,
                                         view: View) : NotificationViewHolder(view) {

    override fun bind(item: Notification) {
        super.bind(item)
        val notification = item as MatchInvitationRejectionNotification
        text.text = context.getString(R.string.notification_match_invitation_rejection, notification.matchInvitation.invitedTeam.name)
        thumbnail.loadThumbnailFromUrl(notification.matchInvitation.invitedTeam.thumbnailUrl)
        super.bind(item)
    }
}