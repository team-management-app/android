package com.tomaszstankowski.demcouch.main.features.teamdetails

import android.net.Uri
import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.domain.TeamMembershipRole
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.matchinvitation.MatchInvitationRepository
import com.tomaszstankowski.demcouch.main.repository.team.TeamRepository
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class TeamDetailsViewModel
@Inject constructor(private val teamRepository: TeamRepository,
                    private val authService: AuthenticationService,
                    private val teamMembershipRepository: TeamMembershipRepository,
                    private val matchInvitationRepository: MatchInvitationRepository)
    : DemCouchViewModel() {

    private fun updateCurrentUserMembershipStatus(memberships: List<TeamMembership>) {
        val currentUserMembership = memberships.find { it.user.id == authService.userId }
        currentUserMembershipId = currentUserMembership?.id ?: 0
        when {
            currentUserMembership == null -> _membershipStatus.onNext(MembershipStatus.NONE)
            currentUserMembership.confirmed && currentUserMembership.role == TeamMembershipRole.LEADER ->
                _membershipStatus.onNext(MembershipStatus.LEADER)
            currentUserMembership.confirmed && currentUserMembership.role != TeamMembershipRole.LEADER ->
                _membershipStatus.onNext(MembershipStatus.PLAYER)
            else -> _membershipStatus.onNext(MembershipStatus.PENDING)
        }
    }

    private fun updateMembers(resource: Resource<List<TeamMembership>>) {
        val members = resource.data
                ?.filter(TeamMembership::confirmed)
                ?.map(TeamMembership::user)
        _members.onNext(Resource(resource.status, members))
    }

    private fun updateJoinRequestCount(memberships: List<TeamMembership>) {
        val joinRequestCount = memberships.filter { !it.confirmed }.size
        _joinRequestsCount.onNext(joinRequestCount)
    }

    private var teamId = 0L

    private var currentUserMembershipId = 0L

    private val _team = BehaviorSubject.create<Resource<Team>>()
    val team: Flowable<Resource<Team>>
        get() = _team.toFlowable(BackpressureStrategy.LATEST)

    val teamSnapshot: Team?
        get() = _team.value?.data

    private val _members = BehaviorSubject.create<Resource<List<User>>>()
    val members: Flowable<Resource<List<User>>>
        get() = _members.toFlowable(BackpressureStrategy.LATEST)

    private val _error = PublishSubject.create<Throwable>()
    val error: Flowable<Throwable>
        get() = _error.toFlowable(BackpressureStrategy.LATEST)

    private val _joinRequestsCount = BehaviorSubject.create<Int>()
    val joinRequestCount: Flowable<Int>
        get() = _joinRequestsCount.toFlowable(BackpressureStrategy.LATEST)

    private val _membershipStatus = BehaviorSubject.create<MembershipStatus>()
    val membershipStatus: Flowable<MembershipStatus>
        get() = _membershipStatus.toFlowable(BackpressureStrategy.LATEST)

    val membershipStatusSnapshot: MembershipStatus
        get() = _membershipStatus.value ?: MembershipStatus.NONE

    private val _matchInvitationsCount = BehaviorSubject.create<Int>()
    val matchInvitationsCount: Flowable<Int>
        get() = _matchInvitationsCount.toFlowable(BackpressureStrategy.LATEST)

    val joinTeam = Action()

    val cancelJoinRequest = Action()

    val leaveTeam = Action()

    fun init(id: Long) {
        if (teamId > 0)
            return
        teamId = id
        compositeDisposable.addAll(
                teamRepository.getTeam(id)
                        .subscribeOn(backgroundThreadScheduler())
                        .subscribe {
                            _team.onNext(it)
                        },
                teamMembershipRepository.getTeamMemberships(id)
                        .subscribeOn(backgroundThreadScheduler())
                        .subscribe {
                            if (it.data != null) {
                                updateCurrentUserMembershipStatus(it.data)
                                updateJoinRequestCount(it.data)
                            }
                            updateMembers(it)
                        },
                matchInvitationRepository.getReceivedMatchInvitationsForTeam(teamId)
                        .subscribeOn(backgroundThreadScheduler())
                        .subscribe {
                            val count = it.data?.size ?: 0
                            _matchInvitationsCount.onNext(count)
                        }
        )
    }

    fun onThumbnailRemoveClicked() {
        val disposable = teamRepository.deleteThumbnail(teamId)
                .subscribeOn(backgroundThreadScheduler())
                .subscribe({}, {
                    _error.onNext(it)
                    Timber.e(it, "Could not remove thumbnail")
                })
        compositeDisposable.add(disposable)
    }

    fun onThumbnailSelected(uri: Uri) {
        val disposable = teamRepository.updateThumbnail(teamId, uri)
                .subscribeOn(backgroundThreadScheduler())
                .subscribe({
                }, {
                    _error.onNext(it)
                    Timber.e(it, "Could not change thumbnail")
                })
        compositeDisposable.add(disposable)
    }

    fun onJoinTeamClicked() {
        val disposable = joinTeam.invoke(
                teamMembershipRepository.addMembershipRequest(teamId))
                .subscribe({
                }, {
                    Timber.e(it, "Failed to join team")
                })
        compositeDisposable.add(disposable)
    }

    fun onCancelJoinRequestClicked() {
        val disposable = cancelJoinRequest.invoke(
                teamMembershipRepository.deleteMembership(currentUserMembershipId))
                .subscribe({
                }, {
                    Timber.e(it, "Failed to cancel join request")
                })
        compositeDisposable.add(disposable)
    }

    fun onLeaveTeamClicked() {
        val disposable = leaveTeam.invoke(
                teamMembershipRepository.deleteMembership(currentUserMembershipId))
                .subscribe({
                }, {
                    Timber.e(it, "Failed to leave team")
                })
        compositeDisposable.add(disposable)
    }
}

enum class MembershipStatus {
    NONE, PENDING, PLAYER, LEADER
}