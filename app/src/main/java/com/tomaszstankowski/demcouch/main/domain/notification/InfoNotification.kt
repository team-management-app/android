package com.tomaszstankowski.demcouch.main.domain.notification

import org.threeten.bp.ZonedDateTime

data class InfoNotification(override val id: Long,
                            override val createDate: ZonedDateTime,
                            override val markedAsRead: Boolean,
                            val message: String) : Notification