package com.tomaszstankowski.demcouch.main.features.createteam

interface ValidationStateListener {
    fun onValidationStatusChanged(valid: Boolean)
}