package com.tomaszstankowski.demcouch.main.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View

class PlaceholderViewHolder(view: View) : RecyclerView.ViewHolder(view)