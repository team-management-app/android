package com.tomaszstankowski.demcouch.main.features.createteam.steps.choosethumbnail

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.createteam.steps.StepFragment
import com.tomaszstankowski.demcouch.main.ui.getMessage
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUri
import com.tomaszstankowski.demcouch.main.ui.startFileChooser
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import kotlinx.android.synthetic.main.fragment_choose_thumbnail.*


class ChooseThumbnailFragment : StepFragment() {
    val thumbnail: Uri?
        get() = viewModel.thumbnailSnapshot

    private lateinit var viewModel: ChooseThumbnailViewModel

    private fun showThumbnail(uri: Uri) {
        fragmentChooseThumbnailErrorTextView.visibility = View.INVISIBLE
        fragmentChooseThumbnailDeleteIcon.visibility = View.VISIBLE
        fragmentChooseThumbnailImageView.visibility = View.VISIBLE
        fragmentChooseThumbnailImageView.loadThumbnailFromUri(uri)
    }

    private fun showError(status: FieldStatus) {
        fragmentChooseThumbnailErrorTextView.visibility = View.VISIBLE
        fragmentChooseThumbnailErrorTextView.text = status.error?.getMessage(context!!, status.data)
        fragmentChooseThumbnailDeleteIcon.visibility = View.GONE
        fragmentChooseThumbnailImageView.visibility = View.INVISIBLE
    }

    private fun showEmptySpace() {
        fragmentChooseThumbnailErrorTextView.visibility = View.INVISIBLE
        fragmentChooseThumbnailDeleteIcon.visibility = View.GONE
        fragmentChooseThumbnailImageView.visibility = View.INVISIBLE
    }

    companion object {
        fun getInstance(thumbnailUri: Uri?): ChooseThumbnailFragment {
            val args = Bundle()
            args.putParcelable(THUMBNAIL_KEY, thumbnailUri)
            val fragment = ChooseThumbnailFragment()
            fragment.arguments = args
            return fragment
        }

        private const val CHOOSE_FILE_REQUEST_CODE = 456
        private const val THUMBNAIL_KEY = "thumbnail"
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 899
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_choose_thumbnail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel()

        setValid(true)

        if (savedInstanceState == null) {
            val thumbnail: Uri? = arguments?.getParcelable(THUMBNAIL_KEY)
            if (thumbnail != null) {
                viewModel.onThumbnailSelected(thumbnail)
            }
        }

        fragmentChooseThumbnailBrowseButton.setOnClickListener {
            if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                startFileChooser(CHOOSE_FILE_REQUEST_CODE)
            } else {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        REQUEST_PERMISSIONS_REQUEST_CODE)
            }
        }

        fragmentChooseThumbnailDeleteIcon.setOnClickListener {
            viewModel.onThumbnailRemoveClicked()
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.thumbnailSelection, {
            when {
                it.status == null ->
                    showEmptySpace()
                it.status.isValid && it.thumbnail != null ->
                    showThumbnail(it.thumbnail)
                !it.status.isValid ->
                    showError(it.status)
                else ->
                    showEmptySpace()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int,
                                  resultData: Intent?) {
        when (requestCode) {
            CHOOSE_FILE_REQUEST_CODE ->
                if (resultCode == Activity.RESULT_OK) {
                    val uri = resultData?.data
                    if (uri != null) {
                        viewModel.onThumbnailSelected(uri)
                    }
                }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSIONS_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startFileChooser(CHOOSE_FILE_REQUEST_CODE)
                }
            }
        }
    }
}