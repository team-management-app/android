package com.tomaszstankowski.demcouch.main.features.editteam

import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.misc.LocationService
import com.tomaszstankowski.demcouch.main.repository.team.TeamRepository
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import com.tomaszstankowski.demcouch.main.viewmodel.validation.Form
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidatedField
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.DescriptionValidator
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.TeamNameValidator
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class EditTeamViewModel
@Inject constructor(private val repository: TeamRepository,
                    teamNameValidator: TeamNameValidator,
                    descriptionValidator: DescriptionValidator,
                    private val locationService: LocationService) : DemCouchViewModel() {

    val name = ValidatedField({ name: String ->
        teamNameValidator.validate(name, _team.name)
    })

    val description = ValidatedField(descriptionValidator::validate)

    private var _discipline: Discipline? = null

    private val _location = BehaviorSubject.create<Location>()
    val location: Flowable<Location>
        get() = _location.toFlowable(BackpressureStrategy.LATEST)

    val pickCurrentLocation = Action()

    val updateTeam = Action()

    val deleteTeam = Action()

    val form = Form(
            Form.Field(name, false),
            Form.Field(description, false))

    private lateinit var _team: Team
    private var init = false

    fun onLocationFromAutoCompleteSelected(location: Location) {
        _location.onNext(location)
    }

    fun onDisciplineSelected(discipline: Discipline) {
        _discipline = discipline
    }

    fun getTeam(id: Long): Single<Team> {
        return if (init) {
            Single.create { it.onSuccess(_team) }
        } else {
            init = true
            repository.getTeam(id)
                    .firstOrError()
                    .map { it.data!! }
                    .doOnSuccess { _team = it }
        }
    }

    fun onSaveChangesClicked() {
        val completable = Single.create<Team> {
            val updatedTeam = _team.run {
                Team(
                        id,
                        this@EditTeamViewModel.name.value ?: name,
                        this@EditTeamViewModel.description.value ?: desc,
                        this@EditTeamViewModel._location.value ?: location,
                        this@EditTeamViewModel._discipline ?: discipline,
                        thumbnailUrl
                )
            }
            it.onSuccess(updatedTeam)
        }
                .flatMapCompletable(repository::updateTeam)
        val disposable = updateTeam.invoke(completable)
                .subscribe({
                    Timber.i("Updated team")
                }, {
                    Timber.e(it, "Could not update team")
                })
        compositeDisposable.add(disposable)
    }

    fun onPickCurrentLocationClicked() {
        val disposable = pickCurrentLocation.invoke(locationService.getCurrentLocation())
                .subscribe({
                    Timber.i("Picked current location")
                    _location.onNext(it)
                }, {
                    Timber.e(it, "Could not get current location")
                })
        compositeDisposable.add(disposable)
    }

    fun onDeleteTeamClicked() {
        val disposable = deleteTeam.invoke(repository.deleteTeam(_team.id))
                .subscribe({
                    Timber.i("Deleted team")
                }, {
                    Timber.e(it, "Could not delete team")
                })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        form.release()
        super.onCleared()
    }
}