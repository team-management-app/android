package com.tomaszstankowski.demcouch.main.features.joinrequestlist

import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class JoinRequestListViewModel
@Inject constructor(private val repository: TeamMembershipRepository) : DemCouchViewModel() {

    private var teamId = 0L


    private val _requestedMemberships = BehaviorSubject.create<Resource<List<TeamMembership>>>()
    val requestedMemberships: Flowable<Resource<List<TeamMembership>>>
        get() = _requestedMemberships.toFlowable(BackpressureStrategy.LATEST)

    val membershipConfirmation = Action()

    val membershipRejection = Action()

    fun init(teamId: Long) {
        if (this.teamId > 0)
            return
        this.teamId = teamId
        val disposable = repository.getTeamMemberships(teamId)
                .map {
                    Resource(it.status, it.data?.filter { membership -> !membership.confirmed })
                }
                .subscribeOn(backgroundThreadScheduler())
                .subscribe { _requestedMemberships.onNext(it) }
        compositeDisposable.add(disposable)
    }

    fun onConfirmMembershipClicked(membershipId: Long) {
        val disposable = membershipConfirmation.invoke(repository.confirmMembership(membershipId))
                .subscribeOn(backgroundThreadScheduler())
                .subscribe({
                }, {
                    Timber.e(it, "Failed to confirm membership")
                })
        compositeDisposable.add(disposable)
    }

    fun onRejectMembershipClicked(membershipId: Long) {
        val disposable = membershipRejection.invoke(repository.deleteMembership(membershipId))
                .subscribeOn(backgroundThreadScheduler())
                .subscribe({
                }, {
                    Timber.e(it, "Failed to reject membership")
                })
        compositeDisposable.add(disposable)
    }
}