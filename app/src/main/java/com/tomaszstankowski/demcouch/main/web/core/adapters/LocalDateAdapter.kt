package com.tomaszstankowski.demcouch.main.web.core.adapters

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

class LocalDateAdapter {
    private val formatter = DateTimeFormatter.ISO_LOCAL_DATE

    @ToJson
    @Synchronized
    fun toJson(date: LocalDate) = date.format(formatter)

    @FromJson
    @Synchronized
    fun fromJson(json: String) = LocalDate.parse(json, formatter)

}