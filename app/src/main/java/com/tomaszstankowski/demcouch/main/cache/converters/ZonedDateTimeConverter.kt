package com.tomaszstankowski.demcouch.main.cache.converters

import android.arch.persistence.room.TypeConverter
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter

class ZonedDateTimeConverter {
    private val formatter = DateTimeFormatter.ISO_DATE_TIME

    @TypeConverter
    @Synchronized
    fun toDbColumn(attr: ZonedDateTime?): String? = attr?.format(formatter)

    @TypeConverter
    @Synchronized
    fun toEntityAttribute(column: String?): ZonedDateTime? {
        if (column == null) {
            return null
        }
        return ZonedDateTime.parse(column, formatter)
    }
}