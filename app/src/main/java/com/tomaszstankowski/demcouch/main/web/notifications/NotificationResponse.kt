package com.tomaszstankowski.demcouch.main.web.notifications

import com.squareup.moshi.Json
import com.tomaszstankowski.demcouch.main.domain.notification.NotificationType
import org.threeten.bp.ZonedDateTime

data class NotificationResponse(val id: Long,
                                @Json(name = "create_date") val createDate: ZonedDateTime,
                                val data: Map<String, Any>,
                                val type: NotificationType)