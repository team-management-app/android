package com.tomaszstankowski.demcouch.main.domain.notification

import com.squareup.moshi.Json
import org.threeten.bp.ZonedDateTime

interface Notification {
    val id: Long
    val createDate: ZonedDateTime
    val markedAsRead: Boolean
}

enum class NotificationType {
    @Json(name = "team_membership_approval")
    TEAM_MEMBERSHIP_APPROVAL,
    @Json(name = "team_membership_rejection")
    TEAM_MEMBERSHIP_REJECTION,
    @Json(name = "team_join_request")
    TEAM_JOIN_REQUEST,
    @Json(name = "match_invitation")
    MATCH_INVITATION,
    @Json(name = "match_invitation_approval")
    MATCH_INVITATION_APPROVAL,
    @Json(name = "match_invitation_rejection")
    MATCH_INVITATION_REJECTION,
    @Json(name = "info")
    INFO
}