package com.tomaszstankowski.demcouch.main.web.matchinvitation

import com.squareup.moshi.Json
import com.tomaszstankowski.demcouch.main.web.match.MatchResponse

data class MatchInvitationStatusUpdateResponse(
        @Json(name = "invitation") val matchInvitation: MatchInvitationResponse,
        val match: MatchResponse?)