package com.tomaszstankowski.demcouch.main.web.user

import com.tomaszstankowski.demcouch.main.web.core.model.ThumbnailResponse
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*

interface UserService {

    @POST("/users")
    fun createUser(@Body body: UserCreateRequest): Single<UserResponse>

    @PUT("/users/{id}")
    fun updateUser(@Path("id") id: Long, @Body body: UserUpdateRequest): Single<UserResponse>

    @POST("/users/{id}/thumbnail")
    @Multipart
    fun uploadThumbnail(@Path("id") id: String,
                        @Part body: MultipartBody.Part): Single<ThumbnailResponse>

    @DELETE("/users/{id}/thumbnail")
    fun deleteThumbnail(@Path("id") id: String): Completable

    @PUT("/users/{id}/password")
    fun changePassword(@Path("id") id: Long, @Body body: ChangePasswordRequest): Completable

    @GET("/users/{id}")
    fun getUser(@Path("id") id: String): Single<UserResponse>

    @DELETE("/users/{id}")
    fun deleteUser(@Path("id") id: Long): Completable
}