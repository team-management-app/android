package com.tomaszstankowski.demcouch.main.auth

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserCacheService @Inject constructor(private val sharedPreferences: SharedPreferences) {

    companion object {
        private const val TOKEN_KEY = "token"
        private const val USER_KEY = "user"
        private const val USER_ID_KEY = "userId"
    }

    fun setCurrentUser(email: String, token: String, userId: Long) {
        sharedPreferences.edit()
                .putString(USER_KEY, email)
                .putString(TOKEN_KEY, token)
                .putLong(USER_ID_KEY, userId)
                .apply()
    }

    val username: String?
        get() = sharedPreferences.getString(USER_KEY, null)

    val userId: Long?
        get() {
            val userId = sharedPreferences.getLong(USER_ID_KEY, 0)
            return if (userId == 0L) null else userId
        }

    var token: String?
        get() = sharedPreferences.getString(TOKEN_KEY, null)
        set(value) {
            if (username == null) {
                throw IllegalStateException("Cannot save token while user is not present")
            }
            sharedPreferences.edit()
                    .putString(TOKEN_KEY, value)
                    .apply()
        }

    fun invalidateCurrentUser() {
        sharedPreferences.edit()
                .remove(USER_KEY)
                .remove(USER_ID_KEY)
                .remove(TOKEN_KEY)
                .apply()
    }
}