package com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchdate

import org.threeten.bp.ZonedDateTime


interface OnDateSelectedListener {
    fun onDateSelected(date: ZonedDateTime)
}