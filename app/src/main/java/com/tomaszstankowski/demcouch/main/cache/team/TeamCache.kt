package com.tomaszstankowski.demcouch.main.cache.team

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.tomaszstankowski.demcouch.main.domain.Location

@Entity(tableName = "team")
data class TeamCache(@PrimaryKey val id: Long,
                     val name: String,
                     val desc: String?,
                     @Embedded(prefix = "location") val location: Location,
                     val discipline: String,
                     @ColumnInfo(name = "thumbnail") val thumbnailUrl: String?)