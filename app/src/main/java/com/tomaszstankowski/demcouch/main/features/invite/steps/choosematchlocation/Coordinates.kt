package com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation

data class Coordinates(val lat: Double, val lng: Double)