package com.tomaszstankowski.demcouch.main.repository.teammembership

import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.cache.teammembership.TeamMembershipDao
import com.tomaszstankowski.demcouch.main.cache.user.UserDao
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.ResourceFetchMemory
import com.tomaszstankowski.demcouch.main.repository.combineCacheWithApi
import com.tomaszstankowski.demcouch.main.repository.team.TeamMapper
import com.tomaszstankowski.demcouch.main.repository.user.UserMapper
import com.tomaszstankowski.demcouch.main.web.teammembership.TeamMembershipCreateRequestBody
import com.tomaszstankowski.demcouch.main.web.teammembership.TeamMembershipResponse
import com.tomaszstankowski.demcouch.main.web.teammembership.TeamMembershipService
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class TeamMembershipRepository
@Inject constructor(private val teamMembershipService: TeamMembershipService,
                    private val teamMembershipDao: TeamMembershipDao,
                    private val teamMembershipMapper: TeamMembershipMapper,
                    private val userMapper: UserMapper,
                    private val userDao: UserDao,
                    private val teamDao: TeamDao,
                    private val teamMapper: TeamMapper,
                    private val fetchMemory: ResourceFetchMemory) {

    fun saveTeamMembershipInCache(membership: TeamMembershipResponse) {
        val userCache = userMapper.mapApiResponseToCache(membership.user)
        val teamCache = teamMapper.mapApiResponseToCache(membership.team)
        userDao.insert(userCache)
        teamDao.insert(teamCache)
        val membershipCache = teamMembershipMapper.mapApiResponseToCache(membership)
        teamMembershipDao.insert(membershipCache)
    }

    private fun onTeamNoLongerExistInApi(id: Long) {
        teamMembershipDao.deleteMembershipsForTeam(id)
        teamDao.delete(id)
    }

    private fun onUserNoLongerExistInApi(id: Long) {
        teamMembershipDao.deleteMembershipsForUser(id)
        userDao.delete(id)
    }

    fun addMembershipRequest(teamId: Long): Single<TeamMembership> {
        val requestBody = TeamMembershipCreateRequestBody(teamId)
        return teamMembershipService.addMembershipRequest(requestBody)
                .map(teamMembershipMapper::mapApiResponseToCache)
                .doOnSuccess(teamMembershipDao::insert)
                .map(teamMembershipMapper::mapCacheToDomain)
    }

    fun confirmMembership(membershipId: Long): Single<TeamMembership> {
        return teamMembershipService.confirmMembership(membershipId.toString())
                .map(teamMembershipMapper::mapApiResponseToCache)
                .doOnSuccess(teamMembershipDao::insert)
                .map(teamMembershipMapper::mapCacheToDomain)
    }

    fun deleteMembership(membershipId: Long): Completable {
        return teamMembershipService.deleteMembership(membershipId.toString())
                .doOnComplete { teamMembershipDao.deleteById(membershipId) }
    }

    fun getTeamMemberships(id: Long): Flowable<Resource<List<TeamMembership>>> {
        return combineCacheWithApi(
                cacheSource = teamMembershipDao.findMembershipsForTeam(id),
                apiSource = teamMembershipService.getMembershipsForTeam(id.toString()),
                onNotFoundInApi = { onTeamNoLongerExistInApi(id) },
                saveInCache = { list -> list.forEach(this::saveTeamMembershipInCache) },
                mapCacheToDomain = { list -> list.map(teamMembershipMapper::mapCacheToDomain) },
                shouldFetch = { fetchMemory.shouldFetch("teams/$id/team-memberships") },
                onFetchCompleted = { fetchMemory.onResourceFetched("teams/$id/team-memberships") })
    }

    fun getUserMemberships(id: Long): Flowable<Resource<List<TeamMembership>>> {
        return combineCacheWithApi(
                cacheSource = teamMembershipDao.findMembershipsForUser(id),
                apiSource = teamMembershipService.getMembershipsForUser(id.toString()),
                onNotFoundInApi = { onUserNoLongerExistInApi(id) },
                saveInCache = { list -> list.forEach(this::saveTeamMembershipInCache) },
                mapCacheToDomain = { list -> list.map(teamMembershipMapper::mapCacheToDomain) },
                shouldFetch = { fetchMemory.shouldFetch("users/$id/team-memberships") },
                onFetchCompleted = { fetchMemory.onResourceFetched("users/$id/team-memberships") })
    }

    fun refreshUserMemberships(id: Long): Completable {
        return teamMembershipService.getMembershipsForUser(id.toString())
                .doOnSuccess { list -> list.forEach(this::saveTeamMembershipInCache) }
                .flatMapCompletable { Completable.complete() }
    }

}