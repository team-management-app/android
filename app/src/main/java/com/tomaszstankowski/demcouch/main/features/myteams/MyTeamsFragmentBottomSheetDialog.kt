package com.tomaszstankowski.demcouch.main.features.myteams

import android.app.Dialog
import android.support.design.widget.BottomSheetDialogFragment
import android.view.View
import android.widget.Button
import com.tomaszstankowski.demcouch.R


class MyTeamsFragmentBottomSheetDialog : BottomSheetDialogFragment() {
    companion object {
        const val RESULT_CODE_CREATE_TEAM = 1
        const val RESULT_CODE_JOIN_TEAM = 2
    }


    @Suppress("DEPRECATION")
    override fun setupDialog(dialog: Dialog?, style: Int) {
        val contentView = View.inflate(context, R.layout.fragment_my_teams_bottom_sheet, null)
        contentView.findViewById<Button>(R.id.fragmentMyTeamsBottomSheetCreateTeamButton)
                .setOnClickListener { setResultCode(RESULT_CODE_CREATE_TEAM) }
        contentView.findViewById<Button>(R.id.fragmentMyTeamsBottomSheetJoinTeamButton)
                .setOnClickListener { setResultCode(RESULT_CODE_JOIN_TEAM) }
        dialog?.setContentView(contentView)
        (contentView.parent as View).setBackgroundColor(resources.getColor(android.R.color.transparent))
    }

    private fun setResultCode(resultCode: Int) {
        targetFragment?.onActivityResult(targetRequestCode, resultCode, null)
        dismiss()
    }
}