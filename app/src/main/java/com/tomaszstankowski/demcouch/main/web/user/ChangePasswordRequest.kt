package com.tomaszstankowski.demcouch.main.web.user

data class ChangePasswordRequest(val password: String)