package com.tomaszstankowski.demcouch.main.features.myteams

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class MyTeamsViewModel
@Inject constructor(private val repository: TeamMembershipRepository,
                    private val authService: AuthenticationService) : DemCouchViewModel() {
    private val _teams = BehaviorSubject.create<Resource<List<Team>>>()
    val teams: Flowable<Resource<List<Team>>>
        get() = _teams.toFlowable(BackpressureStrategy.LATEST)

    private val _refreshing = BehaviorSubject.create<Boolean>()
    val refreshing: Flowable<Boolean>
        get() = _refreshing.toFlowable(BackpressureStrategy.LATEST)

    val userId: Long
        get() = authService.userId


    init {
        val disposable = repository.getUserMemberships(userId)
                .map {
                    Resource(it.status, it.data
                            ?.filter(TeamMembership::confirmed)
                            ?.map(TeamMembership::team))
                }
                .subscribe { _teams.onNext(it) }
        compositeDisposable.add(disposable)
    }

    fun refresh() {
        val disposable = repository.refreshUserMemberships(userId)
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .doOnSubscribe { _refreshing.onNext(true) }
                .doFinally { _refreshing.onNext(false) }
                .subscribe({}, {
                    Timber.e(it, "Failed to refresh user memberships")
                })
        compositeDisposable.add(disposable)
    }
}