package com.tomaszstankowski.demcouch.main.repository.match

import com.tomaszstankowski.demcouch.main.cache.match.MatchCache
import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.domain.Match
import com.tomaszstankowski.demcouch.main.repository.team.TeamMapper
import com.tomaszstankowski.demcouch.main.web.match.MatchResponse
import javax.inject.Inject

class MatchMapper @Inject constructor(private val teamMapper: TeamMapper,
                                      private val teamDao: TeamDao) {

    fun mapApiResponseToCache(apiResponse: MatchResponse): MatchCache {
        return apiResponse.run {
            MatchCache(id, date, location, hostTeam.id, guestTeam.id)
        }
    }

    fun mapCacheToDomain(cache: MatchCache): Match {
        return cache.run {
            val hostTeam = teamDao.findByIdBlocking(cache.hostTeamId)!!
            val guestTeam = teamDao.findByIdBlocking(cache.guestTeamId)!!
            Match(id, date, location,
                    teamMapper.mapCacheToDomain(hostTeam), teamMapper.mapCacheToDomain(guestTeam))
        }
    }
}