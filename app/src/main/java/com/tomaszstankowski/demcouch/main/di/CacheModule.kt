package com.tomaszstankowski.demcouch.main.di

import android.arch.persistence.room.Room
import android.content.Context
import com.tomaszstankowski.demcouch.main.cache.AppDatabase
import com.tomaszstankowski.demcouch.main.cache.match.MatchDao
import com.tomaszstankowski.demcouch.main.cache.matchinvitation.MatchInvitationDao
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationDao
import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.cache.teammembership.TeamMembershipDao
import com.tomaszstankowski.demcouch.main.cache.user.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CacheModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, "demcouch-db")
                    .fallbackToDestructiveMigration()
                    .build()

    @Provides
    @Singleton
    fun provideTeamDao(appDatabase: AppDatabase): TeamDao = appDatabase.teamDao()

    @Provides
    @Singleton
    fun provideUserDao(appDatabase: AppDatabase): UserDao = appDatabase.userDao()

    @Provides
    @Singleton
    fun provideNotificationDao(appDatabase: AppDatabase): NotificationDao = appDatabase.notificationDao()

    @Provides
    @Singleton
    fun provideTeamMembershipDao(appDatabase: AppDatabase): TeamMembershipDao = appDatabase.teamMembershipDao()

    @Provides
    @Singleton
    fun provideMatchInvitationDao(appDatabase: AppDatabase): MatchInvitationDao = appDatabase.matchInvitationDao()

    @Provides
    @Singleton
    fun provideMatchDao(appDatabase: AppDatabase): MatchDao = appDatabase.matchDao()
}