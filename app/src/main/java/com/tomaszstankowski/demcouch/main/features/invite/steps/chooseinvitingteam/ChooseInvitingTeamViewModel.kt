package com.tomaszstankowski.demcouch.main.features.invite.steps.chooseinvitingteam

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.domain.TeamMembershipRole
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ChooseInvitingTeamViewModel
@Inject constructor(private val teamMembershipRepository: TeamMembershipRepository,
                    private val authService: AuthenticationService) : DemCouchViewModel() {

    private val _teams = BehaviorSubject.create<Resource<List<TeamSelection>>>()
    val teams: Flowable<Resource<List<TeamSelection>>>
        get() = _teams.toFlowable(BackpressureStrategy.LATEST)

    private var init = false

    fun init(invitedTeamId: Long, invitedTeamDiscipline: Discipline) {
        if (init) {
            return
        }
        val disposable = teamMembershipRepository
                .getUserMemberships(authService.userId)
                .subscribeOn(backgroundThreadScheduler())
                .map {
                    Resource(it.status, it.data
                            ?.filter { membership -> membership.role == TeamMembershipRole.LEADER }
                            ?.filter { membership -> membership.team.id != invitedTeamId }
                            ?.filter { membership -> membership.team.discipline == invitedTeamDiscipline }
                            ?.map { membership -> membership.team }
                            ?.map { team -> TeamSelection(team) })
                }
                .subscribe { _teams.onNext(it) }
        compositeDisposable.add(disposable)
    }

    fun onTeamSelected(selectedTeam: Team) {
        val snapshot = _teams.value!!
        val newList = snapshot.data!!.map {
            if (it.team.id == selectedTeam.id) {
                TeamSelection(it.team, true)
            } else {
                TeamSelection(it.team, false)
            }
        }
        _teams.onNext(Resource(snapshot.status, newList))
    }
}

class TeamSelection(val team: Team, val selected: Boolean = false)