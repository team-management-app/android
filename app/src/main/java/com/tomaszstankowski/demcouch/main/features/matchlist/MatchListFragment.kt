package com.tomaszstankowski.demcouch.main.features.matchlist

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.match.MatchFragment
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_match_list.*

class MatchListFragment : DemCouchFragment() {

    private lateinit var viewModel: MatchListViewModel
    private lateinit var adapter: MatchListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_match_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()

        adapter = MatchListAdapter(context!!)
        adapter.onItemClickListener = { item, _ ->
            val args = Bundle()
            args.putLong(MatchFragment.MATCH_ID_KEY, item.id)
            Navigation.findNavController(activity!!, R.id.navHostFragment)
                    .navigate(R.id.action_matchListFragment_to_matchFragment, args)
        }
        fragmentMatchListRecyclerView.layoutManager = LinearLayoutManager(context!!)
        fragmentMatchListRecyclerView.adapter = adapter
        fragmentMatchListSwipeRefreshLayout.setOnRefreshListener {
            viewModel.refreshMatchList()
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.matches, adapter::items::set)
        subscribe(viewModel.refreshing, {
            fragmentMatchListSwipeRefreshLayout.isRefreshing = it
        })
    }
}