package com.tomaszstankowski.demcouch.main.features.account

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.user.UserRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class AccountViewModel
@Inject constructor(private val authService: AuthenticationService,
                    private val userRepository: UserRepository) : DemCouchViewModel() {
    private val _user = BehaviorSubject.create<Resource<User>>()
    val user: Flowable<Resource<User>>
        get() = _user.toFlowable(BackpressureStrategy.LATEST)
    private val userSnapshot: User?
        get() = _user.value?.data

    val deleteAccount = Action()

    val logOut = Action()

    init {
        //user could be cached without e-mail
        userRepository.setUserNotFresh(authService.userId)
        val disposable = userRepository.getUser(authService.userId)
                .subscribeOn(backgroundThreadScheduler())
                .subscribe(_user::onNext)
        compositeDisposable.add(disposable)
    }


    fun onDeleteAccountClicked() {
        val disposable = deleteAccount.invoke(
                userRepository.deleteUser(userSnapshot!!.id)
                        .andThen(authService.logOut()))
                .subscribe({
                }, {
                    Timber.e(it, "Error while deleting account")
                })
        compositeDisposable.add(disposable)
    }

    fun onLogOutClicked() {
        val disposable = logOut.invoke(authService.logOut())
                .subscribe({ }, { Timber.e(it, "Failed to log out") })
        compositeDisposable.add(disposable)
    }
}