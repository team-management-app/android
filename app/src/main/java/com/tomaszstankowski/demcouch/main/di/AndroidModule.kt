package com.tomaszstankowski.demcouch.main.di

import android.content.ContentResolver
import android.content.Context
import android.content.SharedPreferences
import com.tomaszstankowski.demcouch.R
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AndroidModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences =
            context.getSharedPreferences(
                    context.getString(R.string.shared_pref_key),
                    Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideContentResolver(context: Context): ContentResolver = context.contentResolver
}