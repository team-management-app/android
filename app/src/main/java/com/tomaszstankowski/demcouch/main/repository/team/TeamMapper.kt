package com.tomaszstankowski.demcouch.main.repository.team

import com.tomaszstankowski.demcouch.main.cache.team.TeamCache
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.misc.ThumbnailService
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import javax.inject.Inject

class TeamMapper @Inject constructor(private val thumbnailService: ThumbnailService) {

    fun mapApiResponseToCache(apiResponse: TeamResponse): TeamCache {
        return apiResponse.run {
            TeamCache(id, name, description, location, discipline.toString(), thumbnailService.normalizeThumbnailUrl(thumbnail))
        }
    }

    fun mapCacheToDomain(cache: TeamCache): Team {
        return cache.run {
            Team(id, name, desc, location, Discipline.valueOf(discipline), thumbnailUrl)
        }
    }

    fun mapApiResponseToDomain(teamResponse: TeamResponse): Team {
        return teamResponse.run {
            Team(id, name, description, location, discipline, thumbnailService.normalizeThumbnailUrl(thumbnail))
        }
    }

}