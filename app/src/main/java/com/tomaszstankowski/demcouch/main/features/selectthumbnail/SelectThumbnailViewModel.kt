package com.tomaszstankowski.demcouch.main.features.selectthumbnail

import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidatedField
import com.tomaszstankowski.demcouch.main.viewmodel.validation.validators.ThumbnailValidator
import javax.inject.Inject

class SelectThumbnailViewModel @Inject constructor(validator: ThumbnailValidator) : DemCouchViewModel() {

    val thumbnail = ValidatedField(validator::validate)
}