package com.tomaszstankowski.demcouch.main.repository.notification.model

import com.squareup.moshi.Moshi
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.domain.notification.NotificationType
import com.tomaszstankowski.demcouch.main.web.notifications.NotificationResponse
import javax.inject.Inject

class NotificationMapper
@Inject constructor(
        moshi: Moshi,
        private val matchInvitationNotificationMapper: MatchInvitationNotificationMapper,
        private val matchInvitationApprovalNotificationMapper: MatchInvitationApprovalNotificationMapper,
        private val matchInvitationRejectionNotificationMapper: MatchInvitationRejectionNotificationMapper,
        private val teamJoinRequestNotificationMapper: TeamJoinRequestNotificationMapper,
        private val infoNotificationMapper: InfoNotificationMapper,
        private val teamMembershipApprovalNotificationMapper: TeamMembershipApprovalNotificationMapper,
        private val teamMembershipRejectionNotificationMapper: TeamMembershipRejectionNotificationMapper) {

    private val adapter = moshi.adapter(Any::class.java)

    fun mapApiResponseToCache(response: NotificationResponse, userId: Long): NotificationCache {
        val notificationDataAsJson = adapter.toJson(response.data)
        return response.run {
            NotificationCache(id, userId, createDate, notificationDataAsJson, type.toString(), false)
        }
    }

    fun mapCacheToDomain(cache: NotificationCache): Notification {
        val type = NotificationType.valueOf(cache.type)
        val mapper: NotificationCacheToDomainMapper<*> =
                when (type) {
                    NotificationType.MATCH_INVITATION -> matchInvitationNotificationMapper
                    NotificationType.MATCH_INVITATION_APPROVAL -> matchInvitationApprovalNotificationMapper
                    NotificationType.MATCH_INVITATION_REJECTION -> matchInvitationRejectionNotificationMapper
                    NotificationType.TEAM_JOIN_REQUEST -> teamJoinRequestNotificationMapper
                    NotificationType.INFO -> infoNotificationMapper
                    NotificationType.TEAM_MEMBERSHIP_APPROVAL -> teamMembershipApprovalNotificationMapper
                    NotificationType.TEAM_MEMBERSHIP_REJECTION -> teamMembershipRejectionNotificationMapper
                }
        return mapper.mapCacheToDomain(cache)
    }
}
