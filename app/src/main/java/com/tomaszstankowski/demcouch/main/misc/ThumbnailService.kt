package com.tomaszstankowski.demcouch.main.misc

import android.content.ContentResolver
import android.net.Uri
import android.provider.OpenableColumns
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject
import javax.inject.Named


class ThumbnailService
@Inject constructor(private val contentResolver: ContentResolver,
                    @Named("apiUrl") private val apiUrl: String) {

    private fun getBytesFromUri(uri: Uri): ByteArray? {
        val iStream = contentResolver.openInputStream(uri)
        val bytes = iStream?.readBytes()
        iStream?.close()
        return bytes
    }

    private fun getFileNameFromUri(uri: Uri): String? {
        contentResolver.query(uri, null, null, null, null)
                ?.use {
                    val nameIndex = it.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                    it.moveToFirst()
                    return it.getString(nameIndex)
                }
        return null
    }

    fun prepareMultipartBody(thumbnail: Uri): MultipartBody.Part {
        val content = getBytesFromUri(thumbnail)!!
        val body = RequestBody.create(
                MediaType.parse(contentResolver.getType(thumbnail) ?: "image/jpeg"),
                content)
        return MultipartBody.Part.createFormData("file", getFileNameFromUri(thumbnail)
                ?: "unknown_image.jpeg", body)
    }

    fun normalizeThumbnailUrl(url: String?): String? {
        if (url == null) {
            return null;
        }
        return "$apiUrl/$url"
    }
}