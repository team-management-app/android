package com.tomaszstankowski.demcouch.main.features.signin

import android.os.Bundle
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.ui.activities.DemCouchActivity

class SignInActivity : DemCouchActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
    }

    override fun onBackPressed() {
    }
}