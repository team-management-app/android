package com.tomaszstankowski.demcouch.main.features.notifications.list

import android.content.Context
import android.view.View
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.domain.notification.TeamMembershipApprovalNotification
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl

class TeamMembershipApprovalViewHolder(private val context: Context, view: View)
    : NotificationViewHolder(view) {

    override fun bind(item: Notification) {
        val notification = item as TeamMembershipApprovalNotification
        notification.team.apply {
            this@TeamMembershipApprovalViewHolder.thumbnail.loadThumbnailFromUrl(thumbnailUrl)
            text.text = context.getString(R.string.notification_membership_approval, name)
        }
        super.bind(item)
    }
}