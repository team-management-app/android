package com.tomaszstankowski.demcouch.main.repository.notification.model

import com.squareup.moshi.Moshi
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.domain.notification.TeamMembershipRejectionNotification
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import javax.inject.Inject

class TeamMembershipRejectionNotificationMapper @Inject constructor(moshi: Moshi)
    : NotificationCacheToDomainMapper<TeamMembershipRejectionNotification> {
    private val adapter = moshi.adapter(TeamMembershipRejectionNotificationData::class.java)

    override fun mapCacheToDomain(notificationCache: NotificationCache): TeamMembershipRejectionNotification {
        val data = adapter.fromJson(notificationCache.data)!!
        return notificationCache.run {
            TeamMembershipRejectionNotification(
                    id,
                    createDate,
                    markedAsRead,
                    data.team)
        }
    }

    private data class TeamMembershipRejectionNotificationData(val team: TeamResponse)
}