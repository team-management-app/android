package com.tomaszstankowski.demcouch.main.features.createteam.steps

import com.tomaszstankowski.demcouch.main.features.createteam.CreateTeamActivity
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment

abstract class StepFragment : DemCouchFragment() {

    protected fun setValid(valid: Boolean) {
        (activity as? CreateTeamActivity?)?.onValidationStatusChanged(valid)
    }
}