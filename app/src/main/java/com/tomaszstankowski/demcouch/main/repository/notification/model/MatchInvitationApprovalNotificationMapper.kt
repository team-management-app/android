package com.tomaszstankowski.demcouch.main.repository.notification.model

import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.domain.notification.MatchInvitationApprovalNotification
import com.tomaszstankowski.demcouch.main.web.matchinvitation.MatchInvitationMapper
import com.tomaszstankowski.demcouch.main.web.matchinvitation.MatchInvitationResponse
import javax.inject.Inject

class MatchInvitationApprovalNotificationMapper
@Inject constructor(moshi: Moshi,
                    private val matchInvitationMapper: MatchInvitationMapper)
    : NotificationCacheToDomainMapper<MatchInvitationApprovalNotification> {

    private val adapter = moshi.adapter(MatchInvitationApprovalNotificationData::class.java)

    override fun mapCacheToDomain(notificationCache: NotificationCache): MatchInvitationApprovalNotification {
        val data = adapter.fromJson(notificationCache.data)!!
        return notificationCache.run {
            MatchInvitationApprovalNotification(
                    id, createDate,
                    markedAsRead,
                    matchInvitationMapper.mapApiResponseToDomain(data.matchInvitation))
        }
    }

    private data class MatchInvitationApprovalNotificationData(
            @Json(name = "invitation") val matchInvitation: MatchInvitationResponse)
}