package com.tomaszstankowski.demcouch.main.web.matchinvitation

import com.tomaszstankowski.demcouch.main.cache.matchinvitation.MatchInvitationCache
import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.domain.MatchInvitation
import com.tomaszstankowski.demcouch.main.domain.MatchInvitationStatus
import com.tomaszstankowski.demcouch.main.repository.team.TeamMapper
import javax.inject.Inject

class MatchInvitationMapper @Inject constructor(private val teamMapper: TeamMapper,
                                                private val teamDao: TeamDao) {

    fun mapApiResponseToCache(apiResponse: MatchInvitationResponse): MatchInvitationCache {
        return apiResponse.run {
            MatchInvitationCache(id, createDate, status.toString(), invitingTeam.id, invitedTeam.id,
                    location, date)
        }
    }

    fun mapApiResponseToDomain(apiResponse: MatchInvitationResponse): MatchInvitation {
        return apiResponse.run {
            val invitingTeam = teamMapper.mapApiResponseToDomain(apiResponse.invitingTeam)
            val invitedTeam = teamMapper.mapApiResponseToDomain(apiResponse.invitedTeam)
            MatchInvitation(id, createDate, status, invitingTeam, invitedTeam, location, date)
        }
    }

    fun mapCacheToDomain(cache: MatchInvitationCache): MatchInvitation {
        return cache.run {
            val invitingTeam = teamMapper.mapCacheToDomain(teamDao.findByIdBlocking(invitingTeamId)!!)
            val invitedTeam = teamMapper.mapCacheToDomain(teamDao.findByIdBlocking(invitedTeamId)!!)
            MatchInvitation(id, createDate, MatchInvitationStatus.valueOf(status),
                    invitingTeam, invitedTeam, location, matchDate)
        }
    }
}