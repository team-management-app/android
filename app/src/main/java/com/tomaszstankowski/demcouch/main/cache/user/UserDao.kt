package com.tomaszstankowski.demcouch.main.cache.user

import android.arch.persistence.room.*
import io.reactivex.Flowable

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: UserCache)

    @Query("SELECT * FROM user WHERE id = :id")
    fun findById(id: Long): Flowable<UserCache>

    @Query("SELECT * FROM user WHERE id = :id")
    fun findByIdBlocking(id: Long): UserCache?

    @Delete
    fun delete(vararg user: UserCache)

    @Query("DELETE FROM user WHERE id= :id")
    fun delete(id: Long)
}