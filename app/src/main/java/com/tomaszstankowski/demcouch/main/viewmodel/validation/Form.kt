package com.tomaszstankowski.demcouch.main.viewmodel.validation

import com.tomaszstankowski.demcouch.main.ui.Schedulers
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

class Form(private vararg val fields: Field) {
    private val disposable = CompositeDisposable()
    private val _valid = BehaviorSubject.create<Boolean>()

    private fun update() {
        val valid = fields.all { formField ->
            if (formField.required) {
                formField.field.currentStatus?.isValid == true
            } else {
                formField.field.currentStatus?.isValid != false
            }
        }
        _valid.onNext(valid)
    }

    val valid: Flowable<Boolean>
        get() = _valid.toFlowable(BackpressureStrategy.LATEST)

    init {
        fields.forEach { pair ->
            val dis = pair.field.status
                    .subscribeOn(Schedulers.backgroundThreadScheduler())
                    .subscribe {
                        update()
                    }
            disposable.add(dis)
        }
    }

    fun release() {
        fields.forEach { field ->
            field.field.release()
        }
        disposable.dispose()
    }

    class Field(val field: ValidatedField<*>, val required: Boolean = false)
}

