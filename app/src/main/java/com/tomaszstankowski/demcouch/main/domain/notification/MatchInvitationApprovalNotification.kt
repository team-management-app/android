package com.tomaszstankowski.demcouch.main.domain.notification

import com.tomaszstankowski.demcouch.main.domain.MatchInvitation
import org.threeten.bp.ZonedDateTime

data class MatchInvitationApprovalNotification(override val id: Long,
                                               override val createDate: ZonedDateTime,
                                               override val markedAsRead: Boolean,
                                               val matchInvitation: MatchInvitation) : Notification