package com.tomaszstankowski.demcouch.main.di

import com.tomaszstankowski.demcouch.main.features.account.AccountFragment
import com.tomaszstankowski.demcouch.main.features.createteam.CreateTeamActivity
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosedescription.ChooseDescriptionFragment
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosediscipline.ChooseDisciplineFragment
import com.tomaszstankowski.demcouch.main.features.createteam.steps.chooselocation.ChooseLocationFragment
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosename.ChooseNameFragment
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosethumbnail.ChooseThumbnailFragment
import com.tomaszstankowski.demcouch.main.features.editteam.EditTeamFragment
import com.tomaszstankowski.demcouch.main.features.invite.InviteActivity
import com.tomaszstankowski.demcouch.main.features.invite.steps.chooseinvitingteam.ChooseInvitingTeamFragment
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchdate.ChooseMatchDateFragment
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation.ChooseMatchLocationFragment
import com.tomaszstankowski.demcouch.main.features.joinrequestlist.JoinRequestListFragment
import com.tomaszstankowski.demcouch.main.features.main.MainActivity
import com.tomaszstankowski.demcouch.main.features.managememberships.ManageMembershipsFragment
import com.tomaszstankowski.demcouch.main.features.match.MatchFragment
import com.tomaszstankowski.demcouch.main.features.matchinvitation.MatchInvitationFragment
import com.tomaszstankowski.demcouch.main.features.matchinvitation.list.MatchInvitationListFragment
import com.tomaszstankowski.demcouch.main.features.matchlist.MatchListFragment
import com.tomaszstankowski.demcouch.main.features.myteams.MyTeamsFragment
import com.tomaszstankowski.demcouch.main.features.notifications.NotificationsFragment
import com.tomaszstankowski.demcouch.main.features.profile.ProfileFragment
import com.tomaszstankowski.demcouch.main.features.profile.profilesettings.ProfileSettingsFragment
import com.tomaszstankowski.demcouch.main.features.profile.profilesettings.changepassword.ChangePasswordFragment
import com.tomaszstankowski.demcouch.main.features.register.RegisterFragment
import com.tomaszstankowski.demcouch.main.features.searchteams.SearchTeamsFragment
import com.tomaszstankowski.demcouch.main.features.selectthumbnail.SelectThumbnailActivity
import com.tomaszstankowski.demcouch.main.features.signin.SignInActivity
import com.tomaszstankowski.demcouch.main.features.signin.SignInFragment
import com.tomaszstankowski.demcouch.main.features.teamdetails.TeamDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ViewBinderModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindCreateTeamActivity(): CreateTeamActivity

    @ContributesAndroidInjector
    abstract fun bindMyTeamsFragment(): MyTeamsFragment

    @ContributesAndroidInjector
    abstract fun bindChooseNameFragment(): ChooseNameFragment

    @ContributesAndroidInjector
    abstract fun bindChooseDescriptionFragment(): ChooseDescriptionFragment

    @ContributesAndroidInjector
    abstract fun bindChooseDisciplineFragment(): ChooseDisciplineFragment

    @ContributesAndroidInjector
    abstract fun bindChooseThumbnailFragment(): ChooseThumbnailFragment

    @ContributesAndroidInjector
    abstract fun bindChooseLocationFragment(): ChooseLocationFragment

    @ContributesAndroidInjector
    abstract fun bindTeamDetailsFragment(): TeamDetailsFragment

    @ContributesAndroidInjector
    abstract fun bindSearchTeamsFragment(): SearchTeamsFragment

    @ContributesAndroidInjector
    abstract fun bindEditTeamFragment(): EditTeamFragment

    @ContributesAndroidInjector
    abstract fun bindSignInActivity(): SignInActivity

    @ContributesAndroidInjector
    abstract fun bindSignInFragment(): SignInFragment

    @ContributesAndroidInjector
    abstract fun bindRegisterFragment(): RegisterFragment

    @ContributesAndroidInjector
    abstract fun bindProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun bindProfileSettingsFragment(): ProfileSettingsFragment

    @ContributesAndroidInjector
    abstract fun bindChangePasswordFragment(): ChangePasswordFragment

    @ContributesAndroidInjector
    abstract fun bindChangeThumbnailActivity(): SelectThumbnailActivity

    @ContributesAndroidInjector
    abstract fun bindNotificationsFragment(): NotificationsFragment

    @ContributesAndroidInjector
    abstract fun bindManageMembershipsFragment(): ManageMembershipsFragment

    @ContributesAndroidInjector
    abstract fun bindJoinRequestListFragment(): JoinRequestListFragment

    @ContributesAndroidInjector
    abstract fun bindMatchInvitationFragment(): MatchInvitationFragment

    @ContributesAndroidInjector
    abstract fun bindMatchInvitationListFragment(): MatchInvitationListFragment

    @ContributesAndroidInjector
    abstract fun bindInviteActivity(): InviteActivity

    @ContributesAndroidInjector
    abstract fun bindChooseInvitingTeamFragment(): ChooseInvitingTeamFragment

    @ContributesAndroidInjector
    abstract fun bindChooseMatchLocationFragment(): ChooseMatchLocationFragment

    @ContributesAndroidInjector
    abstract fun bindChooseMatchDateFragment(): ChooseMatchDateFragment

    @ContributesAndroidInjector
    abstract fun bindMatchListFragment(): MatchListFragment

    @ContributesAndroidInjector
    abstract fun bindMatchFragment(): MatchFragment

    @ContributesAndroidInjector
    abstract fun bindAccountFragment(): AccountFragment

    //Add activities and fragments here

}