package com.tomaszstankowski.demcouch.main.features.invite.steps.chooseinvitingteam

import com.tomaszstankowski.demcouch.main.domain.Team

interface OnTeamSelectedListener {
    fun onTeamSelected(team: Team)
}