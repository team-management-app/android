package com.tomaszstankowski.demcouch.main.features.invite

import android.os.Bundle
import android.support.v4.app.Fragment
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Discipline
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.features.invite.steps.chooseinvitingteam.ChooseInvitingTeamFragment
import com.tomaszstankowski.demcouch.main.features.invite.steps.chooseinvitingteam.OnTeamSelectedListener
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchdate.ChooseMatchDateFragment
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchdate.OnDateSelectedListener
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation.ChooseMatchLocationFragment
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation.Coordinates
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation.OnLocationSelectedListener
import com.tomaszstankowski.demcouch.main.ui.activities.DemCouchActivity
import kotlinx.android.synthetic.main.activity_invite.*
import org.threeten.bp.ZonedDateTime

class InviteActivity : DemCouchActivity(),
        OnTeamSelectedListener,
        OnLocationSelectedListener,
        OnDateSelectedListener {

    companion object {
        const val INVITED_TEAM_ID_KEY = "invitedTeamId"
        const val INVITED_TEAM_DISCIPLINE_KEY = "invitedTeamDiscipline"
    }

    private lateinit var viewModel: InviteViewModel

    private var invitedTeamId: Long = 0

    private lateinit var invitedTeamDiscipline: Discipline

    private fun nextStep() {
        val newFragment: Fragment =
                when (viewModel.currentStep + 1) {
                    1 -> ChooseMatchLocationFragment.instance
                    2 -> ChooseMatchDateFragment.instance
                    3 -> {
                        viewModel.onInviteClicked()
                        return
                    }
                    else -> throw IllegalStateException(String.format("Step number (%d) is not valid", viewModel.currentStep + 1))
                }
        viewModel.currentStep++

        supportFragmentManager.beginTransaction()
                .replace(R.id.activityInviteStepContainer, newFragment)
                .commit()
        activityInviteNextStepButton.isEnabled = false
        activityInviteStepView.go(viewModel.currentStep, true)
    }

    private fun previousStep() {
        val fragment: Fragment =
                when (viewModel.currentStep - 1) {
                    -1 -> {
                        onBackPressed()
                        return
                    }
                    0 -> ChooseInvitingTeamFragment.instance(invitedTeamId, invitedTeamDiscipline)
                    1 -> ChooseMatchLocationFragment.instance
                    else -> throw IllegalStateException(String.format("Step number (%d) is not valid", viewModel.currentStep - 1))
                }
        viewModel.currentStep--

        supportFragmentManager.beginTransaction()
                .replace(R.id.activityInviteStepContainer, fragment)
                .commit()
        activityInviteNextStepButton.isEnabled = false
        activityInviteStepView.go(viewModel.currentStep, true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite)

        viewModel = getViewModel()
        invitedTeamId = intent.extras?.getLong(INVITED_TEAM_ID_KEY)
                ?: throw IllegalStateException("No '$INVITED_TEAM_ID_KEY' param passed to activity")
        invitedTeamDiscipline = Discipline.valueOf(intent.extras?.getString(INVITED_TEAM_DISCIPLINE_KEY)
                ?: throw java.lang.IllegalStateException("no '$INVITED_TEAM_DISCIPLINE_KEY' param passed to activity"))
        viewModel.invitedTeamId = invitedTeamId

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(
                            R.id.activityInviteStepContainer,
                            ChooseInvitingTeamFragment.instance(
                                    invitedTeamId,
                                    invitedTeamDiscipline))
                    .commit()
        }

        activityInviteStepView.go(viewModel.currentStep, false)

        activityInviteNextStepButton.setOnClickListener { nextStep() }
        activityInvitePrevStepButton.setOnClickListener { previousStep() }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.ready, {
            val text = if (it) R.string.invite_activity_invite_button else R.string.next
            activityInviteNextStepButton.setText(text)
        })
        subscribe(viewModel.invitation.success, {
            showMessage(R.string.invite_activity_success)
            finish()
        })
        subscribe(viewModel.invitation.failure, {
            showMessage(R.string.error)
        })
        subscribe(viewModel.invitation.loading, {
            activityInviteNextStepButton.isEnabled = !it
        })
    }

    override fun onTeamSelected(team: Team) {
        activityInviteNextStepButton.isEnabled = true
        viewModel.onTeamSelected(team)
    }

    override fun onLocationSelected(coordinates: Coordinates) {
        activityInviteNextStepButton.isEnabled = true
        viewModel.onLocationSelected(coordinates)
    }

    override fun onDateSelected(date: ZonedDateTime) {
        activityInviteNextStepButton.isEnabled = true
        viewModel.onDateSelected(date)
    }
}