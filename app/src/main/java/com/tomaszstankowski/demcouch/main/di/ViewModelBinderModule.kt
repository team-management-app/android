package com.tomaszstankowski.demcouch.main.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.tomaszstankowski.demcouch.main.features.account.AccountViewModel
import com.tomaszstankowski.demcouch.main.features.createteam.CreateTeamViewModel
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosedescription.ChooseDescriptionViewModel
import com.tomaszstankowski.demcouch.main.features.createteam.steps.chooselocation.ChooseLocationViewModel
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosename.ChooseNameViewModel
import com.tomaszstankowski.demcouch.main.features.createteam.steps.choosethumbnail.ChooseThumbnailViewModel
import com.tomaszstankowski.demcouch.main.features.editteam.EditTeamViewModel
import com.tomaszstankowski.demcouch.main.features.invite.InviteViewModel
import com.tomaszstankowski.demcouch.main.features.invite.steps.chooseinvitingteam.ChooseInvitingTeamViewModel
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchdate.ChooseMatchDateViewModel
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation.ChooseMatchLocationViewModel
import com.tomaszstankowski.demcouch.main.features.joinrequestlist.JoinRequestListViewModel
import com.tomaszstankowski.demcouch.main.features.main.MainViewModel
import com.tomaszstankowski.demcouch.main.features.managememberships.ManageMembershipsViewModel
import com.tomaszstankowski.demcouch.main.features.match.MatchViewModel
import com.tomaszstankowski.demcouch.main.features.matchinvitation.MatchInvitationViewModel
import com.tomaszstankowski.demcouch.main.features.matchinvitation.list.MatchInvitationListViewModel
import com.tomaszstankowski.demcouch.main.features.matchlist.MatchListViewModel
import com.tomaszstankowski.demcouch.main.features.myteams.MyTeamsViewModel
import com.tomaszstankowski.demcouch.main.features.notifications.NotificationsViewModel
import com.tomaszstankowski.demcouch.main.features.profile.ProfileViewModel
import com.tomaszstankowski.demcouch.main.features.profile.profilesettings.ProfileSettingsViewModel
import com.tomaszstankowski.demcouch.main.features.profile.profilesettings.changepassword.ChangePasswordViewModel
import com.tomaszstankowski.demcouch.main.features.register.RegisterViewModel
import com.tomaszstankowski.demcouch.main.features.searchteams.SearchTeamsViewModel
import com.tomaszstankowski.demcouch.main.features.selectthumbnail.SelectThumbnailViewModel
import com.tomaszstankowski.demcouch.main.features.signin.SignInViewModel
import com.tomaszstankowski.demcouch.main.features.teamdetails.TeamDetailsViewModel
import com.tomaszstankowski.demcouch.main.viewmodel.ViewModelFactory
import com.tomaszstankowski.demcouch.main.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelBinderModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun mainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyTeamsViewModel::class)
    internal abstract fun myTeamsViewModel(viewModel: MyTeamsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateTeamViewModel::class)
    internal abstract fun createTeamViewModel(viewModel: CreateTeamViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseNameViewModel::class)
    internal abstract fun chooseNameViewModel(viewModel: ChooseNameViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseDescriptionViewModel::class)
    internal abstract fun chooseDescriptionViewModel(viewModel: ChooseDescriptionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseThumbnailViewModel::class)
    internal abstract fun chooseThumbnailViewModel(viewModel: ChooseThumbnailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseLocationViewModel::class)
    internal abstract fun chooseLocationViewModel(viewModel: ChooseLocationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TeamDetailsViewModel::class)
    internal abstract fun teamDetailsViewModel(viewModel: TeamDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchTeamsViewModel::class)
    internal abstract fun searchTeamsViewModel(viewModel: SearchTeamsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditTeamViewModel::class)
    internal abstract fun editTeamViewModel(viewModel: EditTeamViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignInViewModel::class)
    internal abstract fun signInViewModel(viewModel: SignInViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    internal abstract fun registerViewModel(viewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    internal abstract fun profileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileSettingsViewModel::class)
    internal abstract fun profileSettingsViewModel(viewModel: ProfileSettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangePasswordViewModel::class)
    internal abstract fun changePasswordViewModel(viewModel: ChangePasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelectThumbnailViewModel::class)
    internal abstract fun changeThubmnailViewModel(viewModel: SelectThumbnailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotificationsViewModel::class)
    internal abstract fun notificationsViewModel(viewModel: NotificationsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManageMembershipsViewModel::class)
    internal abstract fun manageMembershipsViewModel(viewModel: ManageMembershipsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(JoinRequestListViewModel::class)
    internal abstract fun joinRequestListViewModel(viewModel: JoinRequestListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MatchInvitationViewModel::class)
    internal abstract fun matchInvitationViewModel(viewModel: MatchInvitationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MatchInvitationListViewModel::class)
    internal abstract fun matchInvitationListViewModel(viewModel: MatchInvitationListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InviteViewModel::class)
    internal abstract fun inviteViewModel(viewModel: InviteViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseInvitingTeamViewModel::class)
    internal abstract fun chooseInvitingTeamViewModel(viewModel: ChooseInvitingTeamViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseMatchLocationViewModel::class)
    internal abstract fun chooseMatchLocationViewModel(viewModel: ChooseMatchLocationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseMatchDateViewModel::class)
    internal abstract fun chooseMatchDateViewModel(viewModel: ChooseMatchDateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MatchListViewModel::class)
    internal abstract fun matchListViewModel(viewModel: MatchListViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(MatchViewModel::class)
    internal abstract fun matchViewModel(viewModel: MatchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountViewModel::class)
    internal abstract fun accountViewModel(viewModel: AccountViewModel): ViewModel

    //Add ViewModels here

}