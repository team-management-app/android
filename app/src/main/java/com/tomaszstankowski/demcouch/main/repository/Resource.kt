package com.tomaszstankowski.demcouch.main.repository

data class Resource<T>(val status: FetchStatus, val data: T?)