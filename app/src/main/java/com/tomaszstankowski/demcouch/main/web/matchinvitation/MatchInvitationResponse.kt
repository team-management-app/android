package com.tomaszstankowski.demcouch.main.web.matchinvitation

import com.squareup.moshi.Json
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.MatchInvitationStatus
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import org.threeten.bp.ZonedDateTime

data class MatchInvitationResponse(val id: Long,
                                   @Json(name = "create_date") val createDate: ZonedDateTime,
                                   val status: MatchInvitationStatus,
                                   @Json(name = "inviting_team") val invitingTeam: TeamResponse,
                                   @Json(name = "invited_team") val invitedTeam: TeamResponse,
                                   val location: Location,
                                   val date: ZonedDateTime)