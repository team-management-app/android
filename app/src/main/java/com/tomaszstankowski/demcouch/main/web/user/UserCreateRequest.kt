package com.tomaszstankowski.demcouch.main.web.user

import com.squareup.moshi.Json

data class UserCreateRequest(val email: String,
                             @Json(name = "first_name") val firstName: String,
                             @Json(name = "last_name") val lastName: String,
                             val password: String)