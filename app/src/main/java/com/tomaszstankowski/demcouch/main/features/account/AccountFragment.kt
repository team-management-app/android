package com.tomaszstankowski.demcouch.main.features.account

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.setVisibleOrElseGone
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : DemCouchFragment() {

    private lateinit var viewModel: AccountViewModel

    private fun displayUser(user: User) {
        user.apply {
            fragmentAccountThumbnail.loadThumbnailFromUrl(thumbnail)
            fragmentAccountFirstName.text = firstName
            fragmentAccountLastName.text = lastName
            fragmentAccountEmail.text = email
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()
        fragmentAccountChangePasswordButton.setOnClickListener(
                Navigation.createNavigateOnClickListener(R.id.action_accountFragment_to_changePasswordFragment))
        fragmentAccountEditProfileButton.setOnClickListener(
                Navigation.createNavigateOnClickListener(R.id.action_accountFragment_to_profileSettingsFragment))
        fragmentAccountLogOutButton.setOnClickListener {
            viewModel.onLogOutClicked()
        }
        fragmentAccountDeleteAccountButton.setOnClickListener {
            AlertDialog.Builder(context!!)
                    .setTitle(R.string.account_fragment_delete_account_dialog_title)
                    .setPositiveButton(R.string.yes) { _, _ -> viewModel.onDeleteAccountClicked() }
                    .setNegativeButton(R.string.cancel) { dialog: DialogInterface, _ -> dialog.dismiss() }
                    .create()
                    .show()
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.user, {
            if (it.data != null) {
                displayUser(it.data)
            } else if (it.status == FetchStatus.FAIL) {
                showMessage(R.string.error)
            }
        })
        fun setAllButtonsEnabled(enabled: Boolean) {
            arrayOf(fragmentAccountEditProfileButton,
                    fragmentAccountChangePasswordButton,
                    fragmentAccountDeleteAccountButton,
                    fragmentAccountLogOutButton)
                    .forEach { it.isEnabled = enabled }
        }

        subscribe(viewModel.deleteAccount.loading, {
            fragmentAccountDeleteAccountProgress.setVisibleOrElseGone(it)
            setAllButtonsEnabled(!it)
        })
        subscribe(viewModel.logOut.loading, {
            fragmentAccountLogOutProgress.setVisibleOrElseGone(it)
            setAllButtonsEnabled(!it)
        })
        subscribe(viewModel.deleteAccount.failure, {
            showMessage(R.string.account_fragment_delete_account_failure)
        })
        subscribe(viewModel.logOut.failure, {
            showMessage(R.string.account_fragment_log_out_failure)
        })
    }
}