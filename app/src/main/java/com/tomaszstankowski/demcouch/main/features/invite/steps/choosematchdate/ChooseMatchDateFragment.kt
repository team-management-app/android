package com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchdate

import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CalendarView
import android.widget.TimePicker
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_choose_match_date.*
import org.threeten.bp.format.DateTimeFormatter

class ChooseMatchDateFragment : DemCouchFragment(),
        TimePickerDialog.OnTimeSetListener,
        CalendarView.OnDateChangeListener {

    private lateinit var viewModel: ChooseMatchDateViewModel

    companion object {
        val instance: ChooseMatchDateFragment
            get() = ChooseMatchDateFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_choose_match_date, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()
        fragmentChooseMatchDateCalendar.setOnDateChangeListener(this)
        fragmentChooseMatchDateCalendar.minDate = viewModel.minDate.toInstant().toEpochMilli()
        fragmentChooseMatchDateCalendar.maxDate = viewModel.maxDate.toInstant().toEpochMilli()
        fragmentChooseMatchDateTime.setOnClickListener {
            TimePickerDialog(context!!, this, viewModel.dateSnapshot.hour, viewModel.dateSnapshot.minute, true).show()
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.date, {
            fragmentChooseMatchDateCalendar.date = it.toInstant().toEpochMilli()
            fragmentChooseMatchDateTime.text = it.format(DateTimeFormatter.ofPattern("HH:mm"))
            (activity as OnDateSelectedListener?)?.onDateSelected(it)
        })
        subscribe(viewModel.invalidDate, {
            showMessage(R.string.choose_match_date_fragment_invalid_date)
        })
    }

    override fun onSelectedDayChange(view: CalendarView, year: Int, month: Int, dayOfMonth: Int) {
        viewModel.onDateSelected(year, month + 1, dayOfMonth)
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        viewModel.onTimeSelected(hourOfDay, minute)
    }
}