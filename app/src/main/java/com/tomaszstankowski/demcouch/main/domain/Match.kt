package com.tomaszstankowski.demcouch.main.domain

import org.threeten.bp.ZonedDateTime

class Match(val id: Long,
            val date: ZonedDateTime,
            val location: Location,
            val hostTeam: Team,
            val guestTeam: Team)