package com.tomaszstankowski.demcouch.main.cache.user

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.threeten.bp.LocalDate

@Entity(tableName = "user")
data class UserCache(@PrimaryKey val id: Long,
                     @ColumnInfo(name = "first_name") val firstName: String,
                     @ColumnInfo(name = "last_name") val lastName: String,
                     val email: String? = null,
                     @ColumnInfo(name = "thumbnail") val thumbnailUrl: String? = null,
                     @ColumnInfo(name = "born_date") val bornDate: LocalDate? = null)