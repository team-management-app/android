package com.tomaszstankowski.demcouch.main.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tomaszstankowski.demcouch.main.web.core.adapters.LocalDateAdapter
import com.tomaszstankowski.demcouch.main.web.core.adapters.ZonedDateTimeAdapter
import com.tomaszstankowski.demcouch.main.web.core.interceptors.TokenInterceptor
import com.tomaszstankowski.demcouch.main.web.login.LoginService
import com.tomaszstankowski.demcouch.main.web.match.MatchService
import com.tomaszstankowski.demcouch.main.web.matchinvitation.MatchInvitationService
import com.tomaszstankowski.demcouch.main.web.notifications.NotificationService
import com.tomaszstankowski.demcouch.main.web.team.TeamService
import com.tomaszstankowski.demcouch.main.web.teammembership.TeamMembershipService
import com.tomaszstankowski.demcouch.main.web.user.UserService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import javax.inject.Named
import javax.inject.Singleton

@Module
class WebModule {

    @Provides
    @Named("apiUrl")
    fun provideApiUrl() = "http://18.130.184.78:5000"

    @Provides
    @Singleton
    fun provideMoshi(): Moshi =
            Moshi.Builder()
                    .add(KotlinJsonAdapterFactory())
                    .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
                    .add(LocalDateAdapter())
                    .add(ZonedDateTimeAdapter())
                    .build()

    @Provides
    @Singleton
    fun provideHttpClient(tokenInterceptor: TokenInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(tokenInterceptor)
                    .addInterceptor(
                            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build()

    @Provides
    @Singleton
    fun provideRetrofit(@Named("apiUrl") apiUrl: String,
                        moshi: Moshi,
                        okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
            .baseUrl(apiUrl)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideTeamService(retrofit: Retrofit): TeamService =
            retrofit.create(TeamService::class.java)

    @Provides
    @Singleton
    fun provideLoginService(retrofit: Retrofit): LoginService =
            retrofit.create(LoginService::class.java)

    @Provides
    @Singleton
    fun provideUserService(retrofit: Retrofit): UserService =
            retrofit.create(UserService::class.java)

    @Provides
    @Singleton
    fun provideNotificationService(retrofit: Retrofit): NotificationService =
            retrofit.create(NotificationService::class.java)

    @Provides
    @Singleton
    fun provideTeamMembershipService(retrofit: Retrofit): TeamMembershipService =
            retrofit.create(TeamMembershipService::class.java)

    @Provides
    @Singleton
    fun provideMatchInvitationService(retrofit: Retrofit): MatchInvitationService =
            retrofit.create(MatchInvitationService::class.java)

    @Provides
    @Singleton
    fun provideMatchService(retrofit: Retrofit): MatchService =
            retrofit.create(MatchService::class.java)
}