package com.tomaszstankowski.demcouch.main.cache

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.tomaszstankowski.demcouch.main.cache.converters.LocalDateConverter
import com.tomaszstankowski.demcouch.main.cache.converters.ZonedDateTimeConverter
import com.tomaszstankowski.demcouch.main.cache.match.MatchCache
import com.tomaszstankowski.demcouch.main.cache.match.MatchDao
import com.tomaszstankowski.demcouch.main.cache.matchinvitation.MatchInvitationCache
import com.tomaszstankowski.demcouch.main.cache.matchinvitation.MatchInvitationDao
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationDao
import com.tomaszstankowski.demcouch.main.cache.team.TeamCache
import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.cache.teammembership.TeamMembershipCache
import com.tomaszstankowski.demcouch.main.cache.teammembership.TeamMembershipDao
import com.tomaszstankowski.demcouch.main.cache.user.UserCache
import com.tomaszstankowski.demcouch.main.cache.user.UserDao

@Database(entities = [
    TeamCache::class,
    UserCache::class,
    NotificationCache::class,
    TeamMembershipCache::class,
    MatchInvitationCache::class,
    MatchCache::class],
        version = 1,
        exportSchema = false)
@TypeConverters(ZonedDateTimeConverter::class, LocalDateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun teamDao(): TeamDao

    abstract fun userDao(): UserDao

    abstract fun notificationDao(): NotificationDao

    abstract fun teamMembershipDao(): TeamMembershipDao

    abstract fun matchInvitationDao(): MatchInvitationDao

    abstract fun matchDao(): MatchDao
}