package com.tomaszstankowski.demcouch.main.cache.team

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface TeamDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(team: TeamCache)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(teams: List<TeamCache>)

    @Query("DELETE FROM team WHERE id= :id")
    fun delete(id: Long)

    @Query("SELECT * FROM team WHERE id = :id")
    fun findById(id: Long): Flowable<TeamCache>

    @Query("SELECT * FROM team WHERE id = :id")
    fun findByIdBlocking(id: Long): TeamCache?
}