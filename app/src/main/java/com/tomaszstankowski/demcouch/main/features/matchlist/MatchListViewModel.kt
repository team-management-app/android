package com.tomaszstankowski.demcouch.main.features.matchlist

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.Match
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.match.MatchRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class MatchListViewModel
@Inject constructor(private val matchRepository: MatchRepository,
                    authService: AuthenticationService) : DemCouchViewModel() {
    private val userId = authService.userId

    private val _matches = BehaviorSubject.create<Resource<List<Match>>>()
    val matches: Flowable<Resource<List<Match>>>
        get() = _matches.toFlowable(BackpressureStrategy.LATEST)

    private val _refreshing = BehaviorSubject.create<Boolean>()
    val refreshing: Flowable<Boolean>
        get() = _refreshing.toFlowable(BackpressureStrategy.LATEST)

    init {
        val disposable = matchRepository.getMatchesForUser(userId)
                .subscribeOn(backgroundThreadScheduler())
                .subscribe(_matches::onNext)
        compositeDisposable.add(disposable)
    }

    fun refreshMatchList() {
        val disposable = matchRepository.refreshMatchesForUser(userId)
                .subscribeOn(backgroundThreadScheduler())
                .doOnSubscribe { _refreshing.onNext(true) }
                .doFinally { _refreshing.onNext(false) }
                .subscribe({}, {
                    Timber.e(it, "Failed to refresh match list")
                })
    }
}