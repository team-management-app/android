package com.tomaszstankowski.demcouch.main.repository.notification.model

import com.squareup.moshi.Moshi
import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.domain.notification.TeamMembershipApprovalNotification
import com.tomaszstankowski.demcouch.main.repository.team.TeamMapper
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import javax.inject.Inject

class TeamMembershipApprovalNotificationMapper
@Inject constructor(moshi: Moshi,
                    private val teamMapper: TeamMapper)
    : NotificationCacheToDomainMapper<TeamMembershipApprovalNotification> {
    private val adapter = moshi.adapter(TeamMembershipApprovalNotificationData::class.java)

    override fun mapCacheToDomain(notificationCache: NotificationCache): TeamMembershipApprovalNotification {
        val data = adapter.fromJson(notificationCache.data)!!
        val team = teamMapper.mapApiResponseToDomain(data.sender)
        return notificationCache.run {
            TeamMembershipApprovalNotification(
                    id, createDate,
                    markedAsRead,
                    team)
        }
    }

    private data class TeamMembershipApprovalNotificationData(val sender: TeamResponse)
}