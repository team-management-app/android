package com.tomaszstankowski.demcouch.main.domain

data class Location(val name: String, val lat: Double, val lng: Double)