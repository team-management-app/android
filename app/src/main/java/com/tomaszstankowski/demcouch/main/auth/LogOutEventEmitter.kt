package com.tomaszstankowski.demcouch.main.auth

import com.tomaszstankowski.demcouch.main.misc.ConsumableEvent
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LogOutEventEmitter @Inject constructor() {
    private val _logOutEvent = BehaviorSubject.create<ConsumableEvent<Any>>()
    val logOutEvent: Flowable<ConsumableEvent<Any>>
        get() = _logOutEvent.toFlowable(BackpressureStrategy.LATEST)

    fun emit() {
        _logOutEvent.onNext(ConsumableEvent())
    }
}