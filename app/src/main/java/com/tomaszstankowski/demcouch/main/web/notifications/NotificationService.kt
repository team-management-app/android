package com.tomaszstankowski.demcouch.main.web.notifications

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Path

interface NotificationService {

    @GET("/users/{id}/notifications")
    fun getUserNotifications(@Path("id") id: Long): Single<List<NotificationResponse>>

    @DELETE("notifications/{id}")
    fun delete(@Path("id") id: Long): Completable
}