package com.tomaszstankowski.demcouch.main.repository.notification.model

import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.domain.notification.Notification

interface NotificationCacheToDomainMapper<T : Notification> {
    fun mapCacheToDomain(notificationCache: NotificationCache): T
}