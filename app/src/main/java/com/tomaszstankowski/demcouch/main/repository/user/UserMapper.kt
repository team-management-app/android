package com.tomaszstankowski.demcouch.main.repository.user

import com.tomaszstankowski.demcouch.main.cache.user.UserCache
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.misc.ThumbnailService
import com.tomaszstankowski.demcouch.main.web.user.UserResponse
import javax.inject.Inject

class UserMapper @Inject constructor(private val thumbnailService: ThumbnailService) {

    fun mapApiResponseToCache(userResponse: UserResponse): UserCache {
        return userResponse.run {
            UserCache(id, firstName, lastName, email, thumbnailService.normalizeThumbnailUrl(thumbnail), bornDate)
        }
    }

    fun mapCacheToDomain(userCache: UserCache): User {
        return userCache.run {
            User(id, firstName, lastName, email, thumbnailUrl, bornDate)
        }
    }
}