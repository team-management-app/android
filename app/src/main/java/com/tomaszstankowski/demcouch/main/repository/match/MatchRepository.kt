package com.tomaszstankowski.demcouch.main.repository.match

import com.tomaszstankowski.demcouch.main.cache.match.MatchDao
import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.cache.teammembership.TeamMembershipCache
import com.tomaszstankowski.demcouch.main.cache.teammembership.TeamMembershipDao
import com.tomaszstankowski.demcouch.main.domain.Match
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.ResourceFetchMemory
import com.tomaszstankowski.demcouch.main.repository.combineCacheWithApi
import com.tomaszstankowski.demcouch.main.repository.team.TeamMapper
import com.tomaszstankowski.demcouch.main.web.match.MatchResponse
import com.tomaszstankowski.demcouch.main.web.match.MatchService
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import com.tomaszstankowski.demcouch.main.web.teammembership.TeamMembershipResponse
import com.tomaszstankowski.demcouch.main.web.teammembership.TeamMembershipService
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class MatchRepository @Inject constructor(private val matchDao: MatchDao,
                                          private val matchService: MatchService,
                                          private val matchMapper: MatchMapper,
                                          private val teamMapper: TeamMapper,
                                          private val teamDao: TeamDao,
                                          private val teamMembershipDao: TeamMembershipDao,
                                          private val teamMembershipService: TeamMembershipService,
                                          private val fetchMemory: ResourceFetchMemory) {

    private fun saveMatchInCache(matchResponse: MatchResponse) {
        val hostTeamCache = teamMapper.mapApiResponseToCache(matchResponse.hostTeam)
        val guestTeamCache = teamMapper.mapApiResponseToCache(matchResponse.guestTeam)
        teamDao.insertAll(listOf(hostTeamCache, guestTeamCache))
        val matchCache = matchMapper.mapApiResponseToCache(matchResponse)
        matchDao.insert(matchCache)
    }

    fun getMatch(id: Long): Flowable<Resource<Match>> {
        return combineCacheWithApi(
                cacheSource = matchDao.findById(id),
                apiSource = matchService.getMatch(id.toString()),
                saveInCache = this::saveMatchInCache,
                mapCacheToDomain = matchMapper::mapCacheToDomain,
                shouldFetch = { fetchMemory.shouldFetch("matches/$id") },
                onFetchCompleted = { fetchMemory.onResourceFetched("matches/$id") })
    }

    fun getMatchesForUser(id: Long): Flowable<Resource<List<Match>>> {
        // Try to get user's teams from API, if call to API fails get it from cache
        // Then get all matches for these teams from cache (optionally fetch it from API too)
        return combineCacheWithApi(
                cacheSource = teamMembershipService.getMembershipsForUser(id.toString())
                        .map { list ->
                            list.filter(TeamMembershipResponse::confirmed)
                                    .map(TeamMembershipResponse::team)
                                    .map(TeamResponse::id)
                        }
                        .toFlowable()
                        .onErrorResumeNext(
                                teamMembershipDao.findMembershipsForUser(id)
                                        .map { list ->
                                            list.filter(TeamMembershipCache::confirmed)
                                                    .map(TeamMembershipCache::teamId)
                                        })

                        .flatMap(matchDao::findForTeams),
                apiSource = matchService.getMatchesForUsers(id.toString()),
                saveInCache = { list -> list.forEach(this::saveMatchInCache) },
                mapCacheToDomain = { list -> list.map(matchMapper::mapCacheToDomain) },
                shouldFetch = { fetchMemory.shouldFetch("users/$id/matches") },
                onFetchCompleted = { fetchMemory.onResourceFetched("users/$id/matches") })
    }

    fun refreshMatchesForUser(id: Long): Completable {
        return matchService.getMatchesForUsers(id.toString())
                .doOnSuccess { list -> list.forEach(this::saveMatchInCache) }
                .flatMapCompletable { Completable.complete() }
    }

    fun setMatchesForUserNotFresh(userId: Long) {
        fetchMemory.onResourceNeedsFetch("users/$userId/matches")
    }
}