package com.tomaszstankowski.demcouch.main.features.joinrequestlist

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.ui.adapters.EmptyListViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.ErrorViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.FixedLengthAdapter
import com.tomaszstankowski.demcouch.main.ui.adapters.PlaceholderViewHolder
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl

class JoinRequestListAdapter(private val context: Context) : FixedLengthAdapter<TeamMembership>() {

    var onMembershipItemClickListener: OnMembershipItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return when (viewType) {
            ITEM_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.join_request_list_item, parent, false)
                JoinRequestListItemViewHolder(view)
            }
            PLACE_HOLDER_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.join_request_list_item_placeholder, parent, false)
                PlaceholderViewHolder(view)
            }
            EMPTY_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_empty_layout, parent, false)
                EmptyListViewHolder(view)
            }
            ERROR_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_error_layout, parent, false)
                ErrorViewHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, pos: Int) {
        if (holder is JoinRequestListItemViewHolder) {
            val item = items.data!![pos]
            holder.confirmButton.setOnClickListener {
                onMembershipItemClickListener?.onMembershipConfirmClicked(item)
            }
            holder.rejectButton.setOnClickListener {
                onMembershipItemClickListener?.onMembershipRejectClicked(item)
            }
            holder.thumbnail.setOnClickListener {
                onMembershipItemClickListener?.onMembershipClicked(item)
            }
            holder.name.setOnClickListener {
                onMembershipItemClickListener?.onMembershipClicked(item)
            }
            item.apply {
                holder.thumbnail.loadThumbnailFromUrl(user.thumbnail)
                @SuppressLint("SetTextI18n")
                holder.name.text = "${user.firstName} ${user.lastName}"
            }
        }
    }

    class JoinRequestListItemViewHolder
    (view: View) : RecyclerView.ViewHolder(view) {
        val thumbnail: ImageView = view.findViewById(R.id.joinRequestListItemThumbnail)
        val name: TextView = view.findViewById(R.id.joinRequestListItemName)
        val confirmButton: ImageView = view.findViewById(R.id.joinRequestListItemAcceptButton)
        val rejectButton: ImageView = view.findViewById(R.id.joinRequestListItemRejectButton)
    }

    interface OnMembershipItemClickListener {
        fun onMembershipClicked(membership: TeamMembership)
        fun onMembershipConfirmClicked(membership: TeamMembership)
        fun onMembershipRejectClicked(membership: TeamMembership)
    }
}