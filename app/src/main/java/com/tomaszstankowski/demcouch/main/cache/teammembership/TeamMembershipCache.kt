package com.tomaszstankowski.demcouch.main.cache.teammembership

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.threeten.bp.ZonedDateTime

@Entity(tableName = "team_membership")
data class TeamMembershipCache(@PrimaryKey val id: Long,
                               @ColumnInfo(name = "start_date") val startDate: ZonedDateTime?,
                               val confirmed: Boolean,
                               val role: String,
                               @ColumnInfo(name = "user_id") val userId: Long,
                               @ColumnInfo(name = "team_id") val teamId: Long)