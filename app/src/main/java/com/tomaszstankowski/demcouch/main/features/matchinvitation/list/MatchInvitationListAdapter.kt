package com.tomaszstankowski.demcouch.main.features.matchinvitation.list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.MatchInvitation
import com.tomaszstankowski.demcouch.main.ui.adapters.EmptyListViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.ErrorViewHolder
import com.tomaszstankowski.demcouch.main.ui.adapters.FixedLengthAdapter
import com.tomaszstankowski.demcouch.main.ui.adapters.PlaceholderViewHolder
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.toMillis

class MatchInvitationListAdapter(private val context: Context) : FixedLengthAdapter<MatchInvitation>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return when (viewType) {
            ITEM_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.match_invitation_list_item, parent, false)
                MatchInvitationViewHolder(view)
            }
            PLACE_HOLDER_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.match_invitation_list_item_placeholder, parent, false)
                PlaceholderViewHolder(view)
            }
            EMPTY_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_empty_layout, parent, false)
                EmptyListViewHolder(view)
            }
            ERROR_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.recycler_view_error_layout, parent, false)
                ErrorViewHolder(view)
            }
            else -> throw IllegalArgumentException("Unknown view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, pos: Int) {
        if (holder is MatchInvitationViewHolder) {
            val item = items.data!![pos]
            item.apply {
                holder.invitingTeamThumbnail.loadThumbnailFromUrl(invitingTeam.thumbnailUrl)
                holder.invitingTeamName.text = invitingTeam.name
                holder.invitedTeamThumbnail.loadThumbnailFromUrl(invitedTeam.thumbnailUrl)
                holder.invitedTeamName.text = invitedTeam.name
                holder.matchDate.text = DateUtils.getRelativeTimeSpanString(context, matchDate.toMillis())
                holder.matchLocation.text = matchLocation.name
            }
            holder.itemView.setOnClickListener {
                onItemClickListener?.invoke(item, pos)
            }
        }
    }

    private class MatchInvitationViewHolder
    (view: View) : RecyclerView.ViewHolder(view) {
        val invitingTeamThumbnail: ImageView = view.findViewById(R.id.matchInvitationListItemInvitingTeamThumbnail)
        val invitingTeamName: TextView = view.findViewById(R.id.matchInvitationListItemInvitingTeamName)
        val invitedTeamThumbnail: ImageView = view.findViewById(R.id.matchInvitationListItemInvitedTeamThumbnail)
        val invitedTeamName: TextView = view.findViewById(R.id.matchInvitationListItemInvitedTeamName)
        val matchDate: TextView = view.findViewById(R.id.matchInvitationListItemMatchDate)
        val matchLocation: TextView = view.findViewById(R.id.matchInvitationListItemMatchLocation)
    }
}