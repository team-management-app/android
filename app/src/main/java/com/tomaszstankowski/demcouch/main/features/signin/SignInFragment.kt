package com.tomaszstankowski.demcouch.main.features.signin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.ui.addOnTextChangeListener
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.hideKeyboard
import kotlinx.android.synthetic.main.fragment_sign_in.*
import retrofit2.HttpException

class SignInFragment : DemCouchFragment() {

    private lateinit var viewModel: SignInViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel()

        fragmentSignInEmailTIL.addOnTextChangeListener { text, _ ->
            viewModel.email = text
        }
        fragmentSignInPasswordTIL.addOnTextChangeListener { text, _ ->
            viewModel.password = text
        }

        fragmentSignInButton.setOnClickListener {
            hideKeyboard()
            viewModel.onSignInClicked()
        }

        fragmentSignInRegisterButton.setOnClickListener(
                Navigation.createNavigateOnClickListener(R.id.action_to_register_fragment))
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.signIn.success, {
            activity?.finish()
        })
        subscribe(viewModel.signIn.failure, {
            if (it is HttpException && (it.code() == 404 || it.code() == 409 || it.code() == 422)) {
                fragmentSignInErrorTV.visibility = View.VISIBLE
                fragmentSignInErrorTV.text = getText(R.string.sign_in_fragment_bad_credentials)
            } else if (it is HttpException && it.code() == 403) {
                fragmentSignInErrorTV.visibility = View.VISIBLE
                fragmentSignInErrorTV.text = getText(R.string.sign_in_fragment_account_not_activated)
            } else {
                fragmentSignInErrorTV.visibility = View.INVISIBLE
                showMessage(R.string.error)
            }
        })
        subscribe(viewModel.signIn.loading, {
            fragmentSignInProgressBar.visibility = if (it) View.VISIBLE else View.GONE
            fragmentSignInButton.isEnabled = !it
            fragmentSignInRegisterButton.isEnabled = !it
        })
    }
}