package com.tomaszstankowski.demcouch.main

import com.jakewharton.threetenabp.AndroidThreeTen
import com.tomaszstankowski.demcouch.main.di.DaggerDemCouchAppComponent
import dagger.android.support.DaggerApplication
import timber.log.Timber

class DemCouchApp : DaggerApplication() {

    override fun applicationInjector() = DaggerDemCouchAppComponent.builder()
            .application(this)
            .build()

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        AndroidThreeTen.init(this)
    }
}