package com.tomaszstankowski.demcouch.main.features.searchteams

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.misc.LocationService
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.team.TeamRepository
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.ui.adapters.PagedListAdapter
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchTeamsViewModel
@Inject constructor(private val teamRepository: TeamRepository,
                    teamMembershipRepository: TeamMembershipRepository,
                    private val locationService: LocationService,
                    authService: AuthenticationService) : DemCouchViewModel() {

    val pageSize = 25

    private var lastRequestedPage = 1

    private val _nameQuery = BehaviorSubject.create<String>()

    private val _pageRequest = BehaviorSubject.create<PagedListAdapter.PageLoadRequest<Team>>()
    val pageRequest: Flowable<PagedListAdapter.PageLoadRequest<Team>>
        get() = _pageRequest.toFlowable(BackpressureStrategy.LATEST)

    val isLoading
        get() = _pageRequest.value?.page?.status == FetchStatus.LOADING

    private val _locationEntries = BehaviorSubject.create<List<LocationEntry>>()
    val locationEntries: Flowable<List<LocationEntry>>
        get() = _locationEntries.toFlowable(BackpressureStrategy.LATEST)

    private val _radiusEntries = BehaviorSubject.create<List<Int>>()
    val radiusEntries: Flowable<List<Int>>
        get() = _radiusEntries.toFlowable(BackpressureStrategy.LATEST)

    private val _selectedLocationEntryIndex = BehaviorSubject.create<Int>()
    val selectedLocationEntryIndex: Flowable<Int>
        get() = _selectedLocationEntryIndex.toFlowable(BackpressureStrategy.LATEST)

    private val _selectedRadiusEntryIndex = BehaviorSubject.create<Int>()
    val selectedRadiusEntryIndex: Flowable<Int>
        get() = _selectedLocationEntryIndex.toFlowable(BackpressureStrategy.LATEST)

    private val _locationQuerySelection = BehaviorSubject.create<LocationQuerySelection>()

    private val currentPageRequestQueryHash: Int
        get() = (_nameQuery.value?.hashCode()
                ?: 0) * 31 + (_locationQuerySelection.value?.locationQuery?.hashCode() ?: 0)

    init {
        _locationEntries.onNext(
                listOf(LocationEntry.anyLocation()))
        _radiusEntries.onNext(listOf(1, 2, 5, 10, 20, 50, 100))
        _selectedLocationEntryIndex.onNext(0)
        _selectedRadiusEntryIndex.onNext(0)

        compositeDisposable.addAll(
                _nameQuery.debounce(300, TimeUnit.MILLISECONDS)
                        .distinctUntilChanged()
                        .toFlowable(BackpressureStrategy.LATEST)
                        .switchMap {
                            teamRepository.findTeams(pageSize = pageSize,
                                    name = it, locationQuery = _locationQuerySelection.value?.locationQuery)
                        }
                        .subscribeOn(Schedulers.backgroundThreadScheduler())
                        .subscribe {
                            val request = PagedListAdapter.PageLoadRequest(
                                    currentPageRequestQueryHash, 1, it)
                            _pageRequest.onNext(request)
                            lastRequestedPage = 1
                        },
                _locationQuerySelection.debounce(300, TimeUnit.MILLISECONDS)
                        .distinctUntilChanged()
                        .toFlowable(BackpressureStrategy.LATEST)
                        .switchMap {
                            teamRepository.findTeams(pageSize = pageSize,
                                    name = _nameQuery.value, locationQuery = it.locationQuery)
                        }
                        .subscribeOn(Schedulers.backgroundThreadScheduler())
                        .subscribe {
                            val request = PagedListAdapter.PageLoadRequest(
                                    currentPageRequestQueryHash, 1, it)
                            _pageRequest.onNext(request)
                            lastRequestedPage = 1
                        },
                teamMembershipRepository.getUserMemberships(authService.userId)
                        .map {
                            Resource(it.status, it.data?.map(TeamMembership::team))
                        }
                        .subscribeOn(backgroundThreadScheduler())
                        .subscribe {
                            if (it.data != null) {
                                val entries = it.data.map { team ->
                                    LocationEntry.locationOfATeam(team)
                                }
                                val joinEntries = _locationEntries.value
                                        ?.filter { entry ->
                                            entry.team == null
                                        } ?: emptyList()
                                val finalEntries = (entries + joinEntries).sorted()
                                _locationEntries.onNext(finalEntries)
                            }
                        }
        )
    }

    fun onLocationAccessPermissionGranted() {
        val disposable = locationService.getCurrentLocation()
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .subscribe({ location ->
                    val entry = LocationEntry.currentLocation(location)
                    val joinEntries = _locationEntries.value
                            ?.filter { e ->
                                e.location == null || e.team != null
                            } ?: emptyList()
                    val finalEntries = (joinEntries + entry).sorted()
                    _locationEntries.onNext(finalEntries)
                }, {
                    Timber.e(it, "Failed to get current location")
                })
        compositeDisposable.add(disposable)
    }

    fun loadNextPage() {
        val disposable = teamRepository.findTeams(
                pageNumber = lastRequestedPage + 1,
                pageSize = pageSize,
                name = _nameQuery.value,
                locationQuery = _locationQuerySelection.value?.locationQuery)
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .subscribe {
                    val request = PagedListAdapter.PageLoadRequest(
                            currentPageRequestQueryHash, lastRequestedPage + 1, it)
                    _pageRequest.onNext(request)
                    if (it.status == FetchStatus.SUCCESS) {
                        lastRequestedPage++
                    }
                }
        compositeDisposable.add(disposable)
    }

    fun onQueryChanged(query: String) {
        _nameQuery.onNext(query)
    }

    fun onLocationSelected(pos: Int) {
        val entry = _locationEntries.value!![pos]
        val location = entry.location
        if (location == null) {
            _locationQuerySelection.onNext(
                    LocationQuerySelection.cleared())
        } else {
            val radiusEntry = _radiusEntries.value!![_selectedRadiusEntryIndex.value!!]
            _locationQuerySelection.onNext(
                    LocationQuerySelection.selected(
                            location.lat.toFloat(), location.lng.toFloat(), radiusEntry.toFloat()))
        }
        _selectedLocationEntryIndex.onNext(pos)
    }

    fun onRadiusSelected(pos: Int) {
        val radiusEntry = _radiusEntries.value!![pos]
        val locationEntry = _locationEntries.value!![_selectedLocationEntryIndex.value!!]
        _locationQuerySelection.onNext(
                LocationQuerySelection.selected(
                        locationEntry.location!!.lat.toFloat(),
                        locationEntry.location.lng.toFloat(),
                        radiusEntry.toFloat())
        )
        _selectedRadiusEntryIndex.onNext(pos)
    }

    class LocationEntry private constructor(val location: Location?, val team: Team?) : Comparable<LocationEntry> {
        companion object {
            fun locationOfATeam(team: Team) = LocationEntry(team.location, team)
            fun currentLocation(location: Location) = LocationEntry(location, null)
            fun anyLocation() = LocationEntry(null, null)
        }

        override fun compareTo(other: LocationEntry): Int {
            if (location == null && other.location != null)
                return -1
            if (other.location == null)
                return 1
            if (team == null && other.team != null)
                return -1
            if (other.team == null)
                return 1
            if (location == null)
                return 0
            return location.name.compareTo(other.location.name)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as LocationEntry

            if (location != other.location) return false
            if (team != other.team) return false

            return true
        }

        override fun hashCode(): Int {
            var result = location?.hashCode() ?: 0
            result = 31 * result + (team?.hashCode() ?: 0)
            return result
        }

    }

    class LocationQuerySelection
    private constructor(val locationQuery: TeamRepository.LocationQuery?) {
        companion object {
            fun selected(lat: Float, lng: Float, radius: Float) =
                    LocationQuerySelection(
                            TeamRepository.LocationQuery(lat, lng, radius))

            fun cleared() =
                    LocationQuerySelection(null)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as LocationQuerySelection

            if (locationQuery != other.locationQuery) return false

            return true
        }

        override fun hashCode(): Int {
            return locationQuery?.hashCode() ?: 0
        }


    }

}