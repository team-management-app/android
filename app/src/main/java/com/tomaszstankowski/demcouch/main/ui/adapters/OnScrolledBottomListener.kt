package com.tomaszstankowski.demcouch.main.ui.adapters

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import timber.log.Timber

class OnScrolledBottomListener(private val lm: LinearLayoutManager,
                               private val onScrolledDown: () -> Unit) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy > 0) {
            val pastVisibleItems = lm.findFirstVisibleItemPosition()
            val visibleItemCount = lm.childCount
            val totalItemCount = lm.itemCount
            val isBottom = pastVisibleItems + visibleItemCount >= totalItemCount
            Timber.i("$pastVisibleItems $visibleItemCount $totalItemCount")
            if (isBottom) {
                onScrolledDown.invoke()
            }
        }
    }
}