package com.tomaszstankowski.demcouch.main.features.selectthumbnail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.ui.activities.DemCouchActivity
import com.tomaszstankowski.demcouch.main.ui.getMessage
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUri
import kotlinx.android.synthetic.main.activity_select_thumbnail.*

class SelectThumbnailActivity : DemCouchActivity() {

    private lateinit var viewModel: SelectThumbnailViewModel

    private fun openFileChooser() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        startActivityForResult(intent, CHOOSE_FILE_REQUEST_CODE)
    }

    companion object {
        private const val CHOOSE_FILE_REQUEST_CODE = 6483
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_thumbnail)

        viewModel = getViewModel()

        setSupportActionBar(activitySelectThumbnailToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            openFileChooser()
        }

        activitySelectThumbnailTryAnother.setOnClickListener {
            openFileChooser()
        }
        activitySelectThumbnailSaveButton.setOnClickListener {
            val intent = Intent()
            intent.data = viewModel.thumbnail.value
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.thumbnail.status, {
            activitySelectThumbnailSaveButton.isEnabled = it.isValid
            if (it.isValid) {
                activitySelectThumbnailError.visibility = View.GONE
                activitySelectThumbnailImage.visibility = View.VISIBLE
                activitySelectThumbnailImage.loadThumbnailFromUri(viewModel.thumbnail.value)
                activitySelectThumbnailSaveButton.isEnabled = true
            } else {
                activitySelectThumbnailError.visibility = View.VISIBLE
                activitySelectThumbnailError.text = it.error?.getMessage(this, it.data)
                activitySelectThumbnailImage.visibility = View.GONE
                activitySelectThumbnailSaveButton.isEnabled = false
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CHOOSE_FILE_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val uri = data?.data
                    if (uri != null)
                        viewModel.thumbnail.postValue(uri)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}