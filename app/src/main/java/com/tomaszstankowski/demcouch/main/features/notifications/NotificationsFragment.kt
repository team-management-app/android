package com.tomaszstankowski.demcouch.main.features.notifications

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.notification.MatchInvitationApprovalNotification
import com.tomaszstankowski.demcouch.main.domain.notification.MatchInvitationNotification
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.domain.notification.TeamJoinRequestNotification
import com.tomaszstankowski.demcouch.main.features.joinrequestlist.JoinRequestListFragment
import com.tomaszstankowski.demcouch.main.features.matchinvitation.list.MatchInvitationListFragment
import com.tomaszstankowski.demcouch.main.features.notifications.list.NotificationListAdapter
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_notifications.*

class NotificationsFragment : DemCouchFragment(), NotificationListAdapter.OnNotificationClickListener {

    private lateinit var viewModel: NotificationsViewModel
    private lateinit var adapter: NotificationListAdapter

    override fun onNotificationClicked(notification: Notification, position: Int) {
        if (!notification.markedAsRead) {
            viewModel.markAsRead(notification)
        }
        when (notification) {
            is TeamJoinRequestNotification -> {
                val args = Bundle()
                args.putLong(JoinRequestListFragment.TEAM_ID_KEY, notification.team.id)
                Navigation.findNavController(activity!!, R.id.navHostFragment)
                        .navigate(R.id.action_notificationsFragment_to_joinRequestListFragment, args)
            }
            is MatchInvitationNotification -> {
                val args = Bundle()
                args.putLong(MatchInvitationListFragment.TEAM_ID_KEY, notification.matchInvitation.invitedTeam.id)
                Navigation.findNavController(activity!!, R.id.navHostFragment)
                        .navigate(R.id.action_notificationsFragment_to_matchInvitationListFragment, args)
            }
            is MatchInvitationApprovalNotification -> {
                Navigation.findNavController(activity!!, R.id.navHostFragment)
                        .navigate(R.id.action_to_matchListFragment)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel()
        adapter = NotificationListAdapter(activity!!)
        val layoutManager = LinearLayoutManager(activity!!)
        fragmentNotificationsRecyclerView.adapter = adapter
        fragmentNotificationsRecyclerView.layoutManager = layoutManager
        adapter.onNotificationClickListener = this
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.notifications, adapter::items::set)
    }
}