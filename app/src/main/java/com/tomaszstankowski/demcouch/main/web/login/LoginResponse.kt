package com.tomaszstankowski.demcouch.main.web.login

import com.squareup.moshi.Json

data class LoginResponse(@Json(name = "auth_token") val token: String,
                         @Json(name = "user_id") val userId: Long)