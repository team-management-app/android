package com.tomaszstankowski.demcouch.main.domain

import org.threeten.bp.ZonedDateTime

data class TeamMembership(val id: Long,
                          val startDate: ZonedDateTime?,
                          val confirmed: Boolean,
                          val role: TeamMembershipRole,
                          val user: User,
                          val team: Team)