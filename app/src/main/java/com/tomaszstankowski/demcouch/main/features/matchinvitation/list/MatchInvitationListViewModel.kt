package com.tomaszstankowski.demcouch.main.features.matchinvitation.list

import com.tomaszstankowski.demcouch.main.domain.MatchInvitation
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.matchinvitation.MatchInvitationRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MatchInvitationListViewModel
@Inject constructor(private val repository: MatchInvitationRepository) : DemCouchViewModel() {

    private val _invitations = BehaviorSubject.create<Resource<List<MatchInvitation>>>()
    val invitations: Flowable<Resource<List<MatchInvitation>>>
        get() = _invitations.toFlowable(BackpressureStrategy.LATEST)

    private var init = false

    fun init(teamId: Long) {
        if (init)
            return
        val disposable = repository.getReceivedMatchInvitationsForTeam(teamId)
                .subscribeOn(backgroundThreadScheduler())
                .subscribe { _invitations.onNext(it) }
        compositeDisposable.add(disposable)
    }

    init {

    }
}