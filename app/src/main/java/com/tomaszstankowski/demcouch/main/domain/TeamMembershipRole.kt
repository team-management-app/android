package com.tomaszstankowski.demcouch.main.domain

import com.squareup.moshi.Json

enum class TeamMembershipRole {
    @Json(name = "player")
    PLAYER,
    @Json(name = "leader")
    LEADER
}