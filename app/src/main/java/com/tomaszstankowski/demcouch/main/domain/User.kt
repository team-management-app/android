package com.tomaszstankowski.demcouch.main.domain

import org.threeten.bp.LocalDate

data class User(val id: Long,
                val firstName: String,
                val lastName: String,
                val email: String? = null,
                val thumbnail: String? = null,
                val bornDate: LocalDate? = null)