package com.tomaszstankowski.demcouch.main.cache.matchinvitation

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.tomaszstankowski.demcouch.main.domain.Location
import org.threeten.bp.ZonedDateTime

@Entity(tableName = "match_invitation")
class MatchInvitationCache(@PrimaryKey val id: Long,
                           @ColumnInfo(name = "create_date") val createDate: ZonedDateTime,
                           val status: String,
                           @ColumnInfo(name = "inviting_team_id") val invitingTeamId: Long,
                           @ColumnInfo(name = "invited_team_id") val invitedTeamId: Long,
                           @Embedded(prefix = "location") val location: Location,
                           @ColumnInfo(name = "match_date") val matchDate: ZonedDateTime)