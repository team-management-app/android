package com.tomaszstankowski.demcouch.main.viewmodel.validation

import com.tomaszstankowski.demcouch.main.ui.Schedulers
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit

class ValidatedField<T>(private val validator: (T) -> Single<FieldStatus>,
                        debounceTime: Long = 300,
                        unit: TimeUnit = TimeUnit.MILLISECONDS) {

    private val _value = BehaviorSubject.create<T>()
    private val _status = BehaviorSubject.create<FieldStatus>()

    private val mainDisposable = _value
            .debounce(debounceTime, unit)
            .distinctUntilChanged()
            .switchMapSingle(validator)
            .toFlowable(BackpressureStrategy.LATEST)
            .subscribeOn(Schedulers.backgroundThreadScheduler())
            .onErrorReturnItem(FieldStatus.correct())
            .subscribe {
                _status.onNext(it)
            }
    private var revalidateDisposable: Disposable? = null


    val status: Flowable<FieldStatus>
        get() = _status.toFlowable(BackpressureStrategy.LATEST)

    val currentStatus: FieldStatus?
        get() = _status.value

    val value: T?
        get() = _value.value

    fun postValue(value: T) {
        if (value != _value.value) {
            _value.onNext(value)
        }
    }

    fun revalidate() {
        val snapshot = value ?: return
        revalidateDisposable?.dispose()
        revalidateDisposable = validator.invoke(snapshot)
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .subscribe { status -> _status.onNext(status) }
    }

    /**
     * This method should be called inside ViewModel onCleared() method.
     */
    fun release() {
        mainDisposable.dispose()
        revalidateDisposable?.dispose()
    }
}