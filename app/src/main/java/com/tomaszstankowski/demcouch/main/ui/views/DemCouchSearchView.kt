package com.tomaszstankowski.demcouch.main.ui.views

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import com.tomaszstankowski.demcouch.R

class DemCouchSearchView(context: Context, attributeSet: AttributeSet) : FrameLayout(context, attributeSet) {

    private var editText: EditText
    private val clearIcon: ImageView

    var text: CharSequence
        get() = editText.text
        set(value) {
            editText.setText(value)
        }

    var hint: CharSequence
        get() = editText.hint
        set(value) {
            editText.hint = value
        }


    init {
        View.inflate(context, R.layout.demcouch_search_view, this)

        editText = findViewById(R.id.searchTextEditText)
        editText.requestFocus()
        clearIcon = findViewById(R.id.clearImageView)
        clearIcon.setOnClickListener {
            editText.text.clear()
        }
        context.theme.obtainStyledAttributes(
                attributeSet,
                R.styleable.DemCouchSearchView,
                0, 0).apply {
            try {
                hint = if (hasValue(R.styleable.DemCouchSearchView_text))
                    getText(R.styleable.DemCouchSearchView_hint)
                else
                    ""
                text = if (hasValue(R.styleable.DemCouchSearchView_text))
                    getText(R.styleable.DemCouchSearchView_text)
                else
                    ""
            } finally {
                recycle()
            }

        }
    }

    fun addTextChangedListener(listener: (String) -> Unit) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                listener.invoke(p0?.toString() ?: "")
            }
        })
    }
}