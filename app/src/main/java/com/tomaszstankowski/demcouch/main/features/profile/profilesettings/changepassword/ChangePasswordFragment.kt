package com.tomaszstankowski.demcouch.main.features.profile.profilesettings.changepassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.ui.addOnTextChangeListener
import com.tomaszstankowski.demcouch.main.ui.changeStatus
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.hideKeyboard
import com.tomaszstankowski.demcouch.main.ui.resetStatus
import kotlinx.android.synthetic.main.fragment_change_password.*

class ChangePasswordFragment : DemCouchFragment() {

    private lateinit var viewModel: ChangePasswordViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_change_password, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel()
        fragmentChangePasswordSaveButton.setOnClickListener {
            hideKeyboard()
            viewModel.onSavePasswordClicked()
        }
        fragmentChangePasswordTIL.addOnTextChangeListener { value, view ->
            viewModel.password.postValue(value)
            view.resetStatus()
        }
        fragmentChangePasswordRepeatTIL.addOnTextChangeListener { value, view ->
            viewModel.passwordRepeat.postValue(value)
            view.resetStatus()
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.password.status, fragmentChangePasswordTIL::changeStatus)
        subscribe(viewModel.passwordRepeat.status, fragmentChangePasswordRepeatTIL::changeStatus)
        subscribe(viewModel.changePassword.success, {
            showMessage(R.string.change_password_fragment_success)
            Navigation.findNavController(activity!!, R.id.navHostFragment).navigateUp()
        })
        subscribe(viewModel.changePassword.failure, {
            showMessage(R.string.change_password_fragment_failure)
        })
        subscribe(viewModel.changePassword.loading, {
            fragmentChangePasswordProgressBar.visibility = if (it) View.VISIBLE else View.GONE
            fragmentChangePasswordSaveButton.isEnabled = !it
        })
        subscribe(viewModel.form.valid, {
            fragmentChangePasswordSaveButton.isEnabled = it
        })
    }
}