package com.tomaszstankowski.demcouch.main.features.notifications.list

import android.content.Context
import android.view.View
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.notification.Notification
import com.tomaszstankowski.demcouch.main.domain.notification.TeamMembershipRejectionNotification
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl

class TeamMembershipRejectionViewHolder(private val context: Context, view: View)
    : NotificationViewHolder(view) {

    override fun bind(item: Notification) {
        val notification = item as TeamMembershipRejectionNotification
        notification.team.apply {
            this@TeamMembershipRejectionViewHolder.thumbnail.loadThumbnailFromUrl(thumbnail)
            text.text = context.getString(R.string.notification_membership_rejection, name)
        }
        super.bind(item)
    }
}