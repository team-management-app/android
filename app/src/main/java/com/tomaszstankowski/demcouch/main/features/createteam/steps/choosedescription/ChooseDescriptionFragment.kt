package com.tomaszstankowski.demcouch.main.features.createteam.steps.choosedescription

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.features.createteam.steps.StepFragment
import com.tomaszstankowski.demcouch.main.ui.addOnTextChangeListener
import com.tomaszstankowski.demcouch.main.ui.changeStatus
import com.tomaszstankowski.demcouch.main.ui.resetStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import kotlinx.android.synthetic.main.fragment_choose_description.*

class ChooseDescriptionFragment : StepFragment() {

    private lateinit var viewModel: ChooseDescriptionViewModel

    companion object {
        fun getInstance(description: String?): ChooseDescriptionFragment {
            val args = Bundle()
            args.putString(DESCRIPTION_KEY, description)
            val fragment = ChooseDescriptionFragment()
            fragment.arguments = args
            return fragment
        }

        private const val DESCRIPTION_KEY = "description"
    }

    val description: String?
        get() = fragmentChooseDescriptionTIL.editText?.text?.toString()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_choose_description, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState == null) {
            val desc = arguments?.getString(DESCRIPTION_KEY)
            if (desc != null) {
                fragmentChooseDescriptionTIL.changeStatus(FieldStatus.correct())
            }
        }
        setValid(true)

        viewModel = getViewModel()

        fragmentChooseDescriptionTIL.addOnTextChangeListener { text, view ->
            view.resetStatus()
            viewModel.description.postValue(text)
            setValid(true)
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.description.status, {
            setValid(it.isValid)
            fragmentChooseDescriptionTIL.changeStatus(it)
        })
    }
}