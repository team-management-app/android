package com.tomaszstankowski.demcouch.main.features.managememberships

import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.Action
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class ManageMembershipsViewModel
@Inject constructor(private val repository: TeamMembershipRepository)
    : DemCouchViewModel() {

    private val _memberships = BehaviorSubject.create<Resource<List<TeamMembership>>>()
    val memberships: Flowable<Resource<List<TeamMembership>>>
        get() = _memberships.toFlowable(BackpressureStrategy.LATEST)

    val deleteMembership = Action()

    private var teamId = 0L

    fun init(teamId: Long) {
        if (this.teamId > 0)
            return
        this.teamId = teamId
        val disposable = repository.getTeamMemberships(teamId)
                .map {
                    Resource(it.status,
                            it.data?.filter(TeamMembership::confirmed)
                    )
                }
                .subscribeOn(backgroundThreadScheduler())
                .subscribe { _memberships.onNext(it) }
        compositeDisposable.add(disposable)
    }

    fun onDeleteMembershipClicked(membership: TeamMembership) {
        val disposable = deleteMembership.invoke(repository.deleteMembership(membership.id))
                .subscribe({
                }, {
                    Timber.e(it, "Failed to delete membership")
                })
        compositeDisposable.add(disposable)
    }
}