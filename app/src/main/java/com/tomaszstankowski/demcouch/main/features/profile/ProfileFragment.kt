package com.tomaszstankowski.demcouch.main.features.profile

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.features.teamdetails.TeamDetailsFragment
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.ui.adapters.TeamHorizontalListAdapter
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.yearsAgo
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : DemCouchFragment() {

    companion object {
        const val ID_KEY = "id"
    }

    private lateinit var viewModel: ProfileViewModel

    private lateinit var adapter: TeamHorizontalListAdapter

    private fun showShimmerLayout() {
        fragmentProfileErrorLayout.visibility = View.GONE
        fragmentProfileContentLayout.visibility = View.GONE
        fragmentProfileShimmerFrameLayout.visibility = View.VISIBLE
        fragmentProfileShimmerFrameLayout.startShimmer()
    }

    private fun showContentLayout() {
        fragmentProfileErrorLayout.visibility = View.GONE
        fragmentProfileContentLayout.visibility = View.VISIBLE
        fragmentProfileShimmerFrameLayout.visibility = View.GONE
        fragmentProfileShimmerFrameLayout.stopShimmer()
    }

    private fun showErrorLayout() {
        fragmentProfileErrorLayout.visibility = View.VISIBLE
        fragmentProfileContentLayout.visibility = View.GONE
        fragmentProfileShimmerFrameLayout.visibility = View.GONE
        fragmentProfileShimmerFrameLayout.stopShimmer()
    }

    private fun displayUser(user: User) {
        fragmentProfileFirstNameTV.text = user.firstName
        fragmentProfileLastNameTV.text = user.lastName
        fragmentProfileAgeTV.text = if (user.bornDate == null) null
        else getString(R.string.profile_fragment_age, user.bornDate.yearsAgo)
        fragmentProfileThumbnail.loadThumbnailFromUrl(user.thumbnail)
    }

    private fun onTeamClicked(team: Team) {
        val args = Bundle()
        args.putLong(TeamDetailsFragment.ID_KEY, team.id)
        Navigation.findNavController(activity!!, R.id.navHostFragment)
                .navigate(R.id.action_profileFragment_to_teamDetailsFragment, args)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()

        val id = arguments?.getLong(ID_KEY) ?: return
        viewModel.init(id)
        adapter = TeamHorizontalListAdapter(context!!)
        adapter.onItemClickListener = { team, _ -> onTeamClicked(team) }
        val layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        fragmentProfileRecyclerView.adapter = adapter
        fragmentProfileRecyclerView.layoutManager = layoutManager

    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.user, {
            if (it.data == null) {
                if (it.status == FetchStatus.LOADING) {
                    showShimmerLayout()
                } else {
                    showErrorLayout()
                }
            } else {
                showContentLayout()
                displayUser(it.data)
            }
        })
        subscribe(viewModel.teams, adapter::items::set)
    }
}