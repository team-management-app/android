package com.tomaszstankowski.demcouch.main.web.teammembership

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface TeamMembershipService {

    @GET("/users/{userId}/team-memberships")
    fun getMembershipsForUser(@Path("userId") userId: String): Single<List<TeamMembershipResponse>>

    @GET("/teams/{teamId}/team-memberships")
    fun getMembershipsForTeam(@Path("teamId") teamId: String): Single<List<TeamMembershipResponse>>

    @POST("/team-memberships")
    fun addMembershipRequest(@Body body: TeamMembershipCreateRequestBody): Single<TeamMembershipResponse>

    @POST("/team-memberships/{id}/confirm")
    fun confirmMembership(@Path("id") id: String): Single<TeamMembershipResponse>

    @DELETE("/team-memberships/{id}")
    fun deleteMembership(@Path("id") id: String): Completable
}