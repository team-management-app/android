package com.tomaszstankowski.demcouch.main.misc

import android.annotation.SuppressLint
import android.location.Geocoder
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.tasks.Tasks
import com.tomaszstankowski.demcouch.main.domain.Location
import io.reactivex.Single
import javax.inject.Inject

class LocationService
@Inject constructor(private val fusedLocationProviderClient: FusedLocationProviderClient,
                    private val geocoder: Geocoder) {

    @SuppressLint("MissingPermission")
    fun getCurrentLocation(): Single<Location> {
        return Single.create<Location> {
            val fusedLocation = Tasks.await(fusedLocationProviderClient.lastLocation)
            val addresses = geocoder.getFromLocation(fusedLocation.latitude, fusedLocation.longitude, 1)
            val location = addresses[0].run {
                Location("$locality, $countryName", latitude, longitude)
            }
            it.onSuccess(location)
        }
    }
}