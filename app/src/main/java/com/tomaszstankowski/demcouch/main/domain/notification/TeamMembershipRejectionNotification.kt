package com.tomaszstankowski.demcouch.main.domain.notification

import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import org.threeten.bp.ZonedDateTime

data class TeamMembershipRejectionNotification(override val id: Long,
                                               override val createDate: ZonedDateTime,
                                               override val markedAsRead: Boolean,
                                               val team: TeamResponse) : Notification