package com.tomaszstankowski.demcouch.main.misc

class ConsumableEvent<T>(val data: T? = null) {
    var isConsumed = false
        private set

    fun consume(): T? {
        if (isConsumed)
            throw IllegalStateException("Event was already consumed")
        isConsumed = true
        return data
    }
}