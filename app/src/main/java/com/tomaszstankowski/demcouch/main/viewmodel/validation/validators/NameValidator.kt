package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import io.reactivex.Single
import javax.inject.Inject

class NameValidator @Inject constructor() {

    companion object {
        const val MAX_LENGTH = 60
    }

    fun validate(name: String): Single<FieldStatus> = Single.create {
        when {
            name.isEmpty() ->
                it.onSuccess(FieldStatus.error(ValidationError.MIN_LENGTH, 1))
            name.length > MAX_LENGTH ->
                it.onSuccess(FieldStatus.error(ValidationError.MAX_LENGTH, MAX_LENGTH))
            else -> it.onSuccess(FieldStatus.correct())
        }
    }
}