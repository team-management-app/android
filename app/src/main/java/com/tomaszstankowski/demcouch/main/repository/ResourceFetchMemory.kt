package com.tomaszstankowski.demcouch.main.repository

import android.content.SharedPreferences
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ResourceFetchMemory @Inject constructor(private val sharedPreferences: SharedPreferences,
                                              private val config: Config,
                                              private val clock: Clock) {

    fun shouldFetch(resourceId: String): Boolean {
        val resourceTimestamp = sharedPreferences.getLong(config.preferencesKeyPrefix + resourceId, 0)
        val currentTimestamp = clock.currentTimeMs
        return currentTimestamp - resourceTimestamp > config.freshDurationMs
    }

    fun onResourceFetched(resourceId: String) {
        val timestamp = clock.currentTimeMs
        sharedPreferences.edit()
                .putLong(config.preferencesKeyPrefix + resourceId, timestamp)
                .apply()
    }

    fun onResourceNeedsFetch(resourceId: String) {
        sharedPreferences.edit()
                .remove(config.preferencesKeyPrefix + resourceId)
                .apply()
    }

    class Config(val freshDurationMs: Long = TimeUnit.HOURS.toMillis(1),
                 val preferencesKeyPrefix: String = "timestamp_")
}

interface Clock {
    val currentTimeMs: Long
}