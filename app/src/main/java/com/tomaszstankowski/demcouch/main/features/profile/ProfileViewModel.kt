package com.tomaszstankowski.demcouch.main.features.profile

import com.tomaszstankowski.demcouch.main.auth.AuthenticationService
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.domain.User
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.repository.teammembership.TeamMembershipRepository
import com.tomaszstankowski.demcouch.main.repository.user.UserRepository
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class ProfileViewModel
@Inject constructor(private val userRepo: UserRepository,
                    private val teamMembershipRepo: TeamMembershipRepository,
                    private val authService: AuthenticationService) : DemCouchViewModel() {

    private val _user = BehaviorSubject.create<Resource<User>>()
    val user: Flowable<Resource<User>>
        get() = _user.toFlowable(BackpressureStrategy.LATEST)

    private val _teams = BehaviorSubject.create<Resource<List<Team>>>()
    val teams: Flowable<Resource<List<Team>>>
        get() = _teams.toFlowable(BackpressureStrategy.LATEST)

    private var userId: Long? = null

    fun init(id: Long) {
        if (userId == null) {
            userId = id
            compositeDisposable.addAll(
                    userRepo.getUser(id)
                            .subscribeOn(backgroundThreadScheduler())
                            .subscribe {
                                _user.onNext(it)
                            },
                    teamMembershipRepo.getUserMemberships(id)
                            .map {
                                Resource(it.status, it.data
                                        ?.filter(TeamMembership::confirmed)
                                        ?.map(TeamMembership::team))
                            }
                            .subscribeOn(backgroundThreadScheduler())
                            .subscribe {
                                _teams.onNext(it)
                            })
        }
    }
}