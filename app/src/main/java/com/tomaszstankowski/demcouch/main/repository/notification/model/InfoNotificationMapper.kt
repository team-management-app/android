package com.tomaszstankowski.demcouch.main.repository.notification.model

import com.tomaszstankowski.demcouch.main.cache.notifications.NotificationCache
import com.tomaszstankowski.demcouch.main.domain.notification.InfoNotification
import javax.inject.Inject

class InfoNotificationMapper @Inject constructor() : NotificationCacheToDomainMapper<InfoNotification> {

    override fun mapCacheToDomain(notificationCache: NotificationCache): InfoNotification {
        return notificationCache.run {
            InfoNotification(id, createDate, markedAsRead, message = notificationCache.data)
        }
    }
}