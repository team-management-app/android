package com.tomaszstankowski.demcouch.main.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.web.core.model.PageResponse
import kotlin.math.min

abstract class PagedListAdapter<T>(val pageSize: Int, private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val ITEM_VIEW_TYPE = 0
        const val PLACE_HOLDER_VIEW_TYPE = 1
        const val EMPTY_VIEW_TYPE = 2
        const val ERROR_VIEW_TYPE = 3
    }

    protected class Item<I> private constructor(val type: Int, val data: I?) {
        companion object {
            fun <I> placeholder() = Item<I>(PLACE_HOLDER_VIEW_TYPE, null)
            fun <I> loaded(data: I) = Item(ITEM_VIEW_TYPE, data)
            fun <I> error() = Item<I>(ERROR_VIEW_TYPE, null)
            fun <I> empty() = Item<I>(EMPTY_VIEW_TYPE, null)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Item<*>

            if (type != other.type) return false
            if (data != other.data) return false

            return true
        }

        override fun hashCode(): Int {
            var result = type.hashCode()
            result = 31 * result + (data?.hashCode() ?: 0)
            return result
        }
    }

    protected val items: MutableList<Item<T>> = ArrayList()

    private val totalItemCount: Int?
        get() = lastRequest?.page?.data?.pagination?.totalItemCount

    private fun clearPlaceholdersWithoutNotifyingChanges(): Int {
        val firstPlaceholderItemIndex = items.indexOfFirst { it.type == PLACE_HOLDER_VIEW_TYPE }
        if (firstPlaceholderItemIndex == -1) {
            return 0
        }
        val placeHolderCount = items.size - firstPlaceholderItemIndex
        items.subList(firstPlaceholderItemIndex, items.size).clear()
        return placeHolderCount
    }

    private var lastRequest: PageLoadRequest<T>? = null

    val endReached: Boolean
        get() = lastRequest?.page?.data?.pagination?.page != null
                && lastRequest?.page?.data?.pagination?.page == lastRequest?.page?.data?.pagination?.pageCount

    var onItemClickListener: ((item: T, position: Int) -> Unit)? = null

    private fun insertPageOfPlaceHolders() {
        val placeHoldersCount = if (totalItemCount == null) pageSize
        else min(totalItemCount!! - items.size, pageSize)
        repeat(placeHoldersCount) { items.add(Item.placeholder()) }
        notifyItemRangeInserted(items.size - placeHoldersCount, placeHoldersCount)
    }

    private fun insertItems(newItems: List<Item<T>>) {
        val offset = items.size
        items.addAll(newItems)
        notifyItemRangeInserted(offset, newItems.size)
    }

    private fun removePreviousItemsAndInsertPageOfPlaceHolders() {
        if (items.size > pageSize) {
            val changedItemsCount = pageSize
            val removedItemsCount = items.size - changedItemsCount
            items.clear()
            repeat(pageSize) { items.add(Item.placeholder()) }
            notifyItemRangeChanged(0, pageSize)
            notifyItemRangeRemoved(pageSize, removedItemsCount)
        } else {
            val changedItemsCount = items.size
            val insertedItemsCount = pageSize - changedItemsCount
            items.clear()
            repeat(pageSize) { items.add(Item.placeholder()) }
            notifyItemRangeChanged(0, changedItemsCount)
            notifyItemRangeInserted(changedItemsCount, insertedItemsCount)
        }
    }

    private fun removePlaceholdersAndAddItems(newItems: List<Item<T>>) {
        val placeHoldersCount = clearPlaceholdersWithoutNotifyingChanges()
        items.addAll(newItems)

        val changedItemsCount = newItems.size
        val itemsRemovedCount = placeHoldersCount - changedItemsCount
        notifyItemRangeChanged(items.size - changedItemsCount, changedItemsCount)
        notifyItemRangeRemoved(items.size, itemsRemovedCount)
    }

    private fun removePreviousItemsIfAnyAndAddItem(item: Item<T>) {
        val prevItemsCount = items.size
        items.clear()
        items.add(item)
        if (prevItemsCount > 0) {
            notifyItemRangeChanged(0, 1)
            notifyItemRangeRemoved(1, prevItemsCount - 1)
        } else {
            notifyItemRangeInserted(0, 1)
        }
    }

    private fun removePlaceholders() {
        val offset = items.indexOfFirst { it.type == PLACE_HOLDER_VIEW_TYPE }
        if (offset != -1) {
            val removedItemsCount = items.size - offset
            items.subList(offset, items.size).clear()
            notifyItemRangeRemoved(offset, removedItemsCount)
        }
    }

    private fun handleRequestForTheSameQuery(lastRequest: PageLoadRequest<T>, request: PageLoadRequest<T>) {
        when (lastRequest.page.status) {
            FetchStatus.LOADING -> {
                when (request.page.status) {
                    FetchStatus.SUCCESS -> {
                        if (request.page.data?.data?.isEmpty() != false) {
                            check(request.pageNumber == 1) { "Wrong page ${request.pageNumber}" }
                            removePreviousItemsIfAnyAndAddItem(Item.empty())
                        } else {
                            removePlaceholdersAndAddItems(request.page.data.data.map { Item.loaded(it) })
                        }
                    }
                    FetchStatus.FAIL -> {
                        if (lastRequest.pageNumber == 1) {
                            removePreviousItemsIfAnyAndAddItem(Item.error())
                        } else {
                            removePlaceholders()
                            Toast.makeText(context, R.string.page_load_error, Toast.LENGTH_SHORT)
                                    .show()
                        }
                    }
                    FetchStatus.LOADING -> removePreviousItemsAndInsertPageOfPlaceHolders()
                }
            }
            FetchStatus.FAIL -> {
                check(request.page.status == FetchStatus.LOADING) { "Page should be loading instead" }
                if (lastRequest.pageNumber == 1) {
                    removePreviousItemsAndInsertPageOfPlaceHolders()
                } else {
                    insertPageOfPlaceHolders()
                }
            }
            FetchStatus.SUCCESS -> {
                check(request.page.status == FetchStatus.LOADING) {
                    "Page should be loading intead"
                }
                check(request.pageNumber == 1 || request.pageNumber == lastRequest.pageNumber + 1) {
                    "Wrong page number ${request.pageNumber}"
                }
                if (request.pageNumber == lastRequest.pageNumber + 1) {
                    insertPageOfPlaceHolders()
                } else {
                    removePreviousItemsAndInsertPageOfPlaceHolders()
                }
            }
        }
    }

    private fun handleRequestForNewQuery(request: PageLoadRequest<T>) {
        if (lastRequest == null) {
            when (request.page.status) {
                FetchStatus.LOADING -> insertPageOfPlaceHolders()
                FetchStatus.FAIL -> removePreviousItemsIfAnyAndAddItem(Item.error())
                FetchStatus.SUCCESS -> {
                    if (request.page.data?.data?.isEmpty() == false) {
                        insertItems(request.page.data.data.map { Item.loaded(it) })
                    } else {
                        removePreviousItemsIfAnyAndAddItem(Item.empty())
                    }
                }
            }
        } else {
            check(request.page.status == FetchStatus.LOADING)
            removePreviousItemsAndInsertPageOfPlaceHolders()
        }
    }

    fun submitPage(request: PageLoadRequest<T>) {
        if (request.requestQueryHash == lastRequest?.requestQueryHash) {
            handleRequestForTheSameQuery(lastRequest!!, request)
        } else {
            handleRequestForNewQuery(request)
        }
        lastRequest = request
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        return items[position].type
    }

    data class PageLoadRequest<T>(val requestQueryHash: Int,
                                  val pageNumber: Int,
                                  val page: Resource<PageResponse<T>>)

}