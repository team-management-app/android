package com.tomaszstankowski.demcouch.main.web.teammembership

import com.squareup.moshi.Json
import com.tomaszstankowski.demcouch.main.domain.TeamMembershipRole
import com.tomaszstankowski.demcouch.main.web.team.TeamResponse
import com.tomaszstankowski.demcouch.main.web.user.UserResponse
import org.threeten.bp.ZonedDateTime

data class TeamMembershipResponse(val id: Long,
                                  @Json(name = "start_date") val startDate: ZonedDateTime?,
                                  val confirmed: Boolean,
                                  val role: TeamMembershipRole,
                                  @Json(name = "team") val team: TeamResponse,
                                  @Json(name = "user") val user: UserResponse)