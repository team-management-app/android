package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import io.reactivex.Single
import javax.inject.Inject

class DescriptionValidator @Inject constructor() {

    companion object {
        internal const val MAX_LENGTH = 100
    }

    fun validate(description: String): Single<FieldStatus> {
        return Single.create {
            if (description.length > MAX_LENGTH) {
                it.onSuccess(FieldStatus.error(ValidationError.MAX_LENGTH, MAX_LENGTH))
            } else {
                it.onSuccess(FieldStatus.correct())
            }
        }
    }
}