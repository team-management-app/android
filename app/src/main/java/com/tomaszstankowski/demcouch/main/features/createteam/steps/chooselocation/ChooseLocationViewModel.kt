package com.tomaszstankowski.demcouch.main.features.createteam.steps.chooselocation

import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.misc.LocationService
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.repository.Resource
import com.tomaszstankowski.demcouch.main.ui.Schedulers
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class ChooseLocationViewModel
@Inject constructor(private val locationService: LocationService) : DemCouchViewModel() {

    private val _location = BehaviorSubject.create<Resource<Location>>()
    val location: Flowable<Resource<Location>>
        get() = _location.toFlowable(BackpressureStrategy.LATEST)

    val locationSnapshot: Location?
        get() = _location.value?.data

    fun onLocationFromAutocompleteSelected(location: Location) {
        _location.onNext(Resource(FetchStatus.SUCCESS, location))
    }

    fun pickCurrentLocation() {
        val disposable = locationService.getCurrentLocation()
                .subscribeOn(Schedulers.backgroundThreadScheduler())
                .doOnSubscribe { _location.onNext(Resource(FetchStatus.LOADING, null)) }
                .subscribe({
                    _location.onNext(Resource(FetchStatus.SUCCESS, it))
                }, {
                    _location.onNext(Resource(FetchStatus.FAIL, null))
                    Timber.e(it, "Could not get current location")
                })
        compositeDisposable.add(disposable)
    }
}