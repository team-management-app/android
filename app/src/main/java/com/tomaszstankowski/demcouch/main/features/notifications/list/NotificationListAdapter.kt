package com.tomaszstankowski.demcouch.main.features.notifications.list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.notification.*
import com.tomaszstankowski.demcouch.main.ui.adapters.EmptyListViewHolder

class NotificationListAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_MATCH_INVITATION = 0
        const val VIEW_TYPE_MATCH_INVITATION_APPROVAL = 1
        const val VIEW_TYPE_MATCH_INVITATION_REJECTION = 2
        const val VIEW_TYPE_JOIN_REQUEST = 3
        const val VIEW_TYPE_INFO = 4
        const val VIEW_TYPE_MEMBERSHIP_APPROVAL = 5
        const val VIEW_TYPE_MEMBERSHIP_REJECTION = 6
        const val VIEW_TYPE_EMPTY = 7
    }

    var onNotificationClickListener: OnNotificationClickListener? = null

    var items: List<Notification> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.notification_list_item, parent, false)
        return when (viewType) {
            VIEW_TYPE_MATCH_INVITATION -> MatchInvitationViewHolder(context, view)
            VIEW_TYPE_MATCH_INVITATION_APPROVAL -> MatchInvitationApprovalViewHolder(context, view)
            VIEW_TYPE_MATCH_INVITATION_REJECTION -> MatchInvitationRejectionViewHolder(context, view)
            VIEW_TYPE_JOIN_REQUEST -> TeamJoinRequestViewHolder(context, view)
            VIEW_TYPE_INFO -> InfoViewHolder(context, view)
            VIEW_TYPE_MEMBERSHIP_APPROVAL -> TeamMembershipApprovalViewHolder(context, view)
            VIEW_TYPE_MEMBERSHIP_REJECTION -> TeamMembershipRejectionViewHolder(context, view)
            VIEW_TYPE_EMPTY -> {
                val emptyView = inflater.inflate(R.layout.recycler_view_empty_layout, parent, false)
                EmptyListViewHolder(emptyView)
            }
            else -> throw IllegalArgumentException("Unknown view type")
        }
    }

    override fun getItemCount(): Int {
        return if (items.isEmpty()) 1
        else items.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (items.isEmpty() && position == 0) VIEW_TYPE_EMPTY
        else when (items[position]) {
            is MatchInvitationNotification -> VIEW_TYPE_MATCH_INVITATION
            is MatchInvitationApprovalNotification -> VIEW_TYPE_MATCH_INVITATION_APPROVAL
            is MatchInvitationRejectionNotification -> VIEW_TYPE_MATCH_INVITATION_REJECTION
            is TeamJoinRequestNotification -> VIEW_TYPE_JOIN_REQUEST
            is InfoNotification -> VIEW_TYPE_INFO
            is TeamMembershipApprovalNotification -> VIEW_TYPE_MEMBERSHIP_APPROVAL
            is TeamMembershipRejectionNotification -> VIEW_TYPE_MEMBERSHIP_REJECTION
            else -> throw IllegalStateException("Unknown type of notification")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, pos: Int) {
        when (holder) {
            is NotificationViewHolder -> {
                val item = items[pos]
                holder.bind(item)
                holder.itemView.setOnClickListener {
                    onNotificationClickListener?.onNotificationClicked(item, pos)
                }
            }
            is EmptyListViewHolder -> holder.text.text = context.getString(R.string.notification_list_empty)
        }
    }

    interface OnNotificationClickListener {
        fun onNotificationClicked(notification: Notification, position: Int)
    }

}