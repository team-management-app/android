package com.tomaszstankowski.demcouch.main.web.user

import com.squareup.moshi.Json
import org.threeten.bp.LocalDate

data class UserResponse(val id: Long,
                        @Json(name = "first_name") val firstName: String,
                        @Json(name = "last_name") val lastName: String,
                        val email: String? = null,
                        val thumbnail: String? = null,
                        @Json(name = "born_date") val bornDate: LocalDate? = null)