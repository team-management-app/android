package com.tomaszstankowski.demcouch.main.features.match

import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.domain.Match
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.loadThumbnailFromUrl
import com.tomaszstankowski.demcouch.main.ui.toDisplayString
import com.tomaszstankowski.demcouch.main.ui.toMillis
import kotlinx.android.synthetic.main.fragment_match.*

class MatchFragment : DemCouchFragment(), OnMapReadyCallback {

    private lateinit var viewModel: MatchViewModel

    private var marker: Marker? = null
    private var map: GoogleMap? = null

    private fun updateMarkerAndMapPositionIfPresent(matchLocation: Location) {
        matchLocation.apply {
            val latLng = LatLng(lat, lng)
            marker?.position = latLng
            marker?.title = name
            marker?.showInfoWindow()
            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM))
        }
    }

    private fun displayMatch(match: Match) {
        match.apply {
            fragmentMatchHostTeamName.text = hostTeam.name
            fragmentMatchGuestTeamName.text = guestTeam.name
            fragmentMatchDiscipline.text = hostTeam.discipline.toDisplayString(activity!!)
            fragmentMatchDate.text = DateFormat.getLongDateFormat(activity!!).format(date.toMillis())
            fragmentMatchTime.text = DateFormat.getTimeFormat(activity!!).format(date.toMillis())
            fragmentMatchHostTeamThumbnail.loadThumbnailFromUrl(hostTeam.thumbnailUrl)
            fragmentMatchGuestTeamThumbnail.loadThumbnailFromUrl(guestTeam.thumbnailUrl)
        }
    }

    companion object {
        const val MATCH_ID_KEY = "matchId"
        private const val MAP_ZOOM = 15f
        private const val DEFAULT_MARKER_LAT = 54.3742012
        private const val DEFAULT_MARKER_LNG = 18.6073264
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_match, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()
        if (savedInstanceState == null) {
            val matchId = arguments?.getLong(MATCH_ID_KEY)
                    ?: throw IllegalStateException("No '$MATCH_ID_KEY' param passed to fragment")
            viewModel.init(matchId)
        }
        val mapFragment = childFragmentManager.findFragmentById(R.id.fragmentMatchMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.match, {
            if (it.data == null && it.status == FetchStatus.FAIL) {
                showMessage(R.string.error)
                Navigation.findNavController(activity!!, R.id.navHostFragment)
                        .navigateUp()
            } else if (it.data != null) {
                displayMatch(it.data)
                updateMarkerAndMapPositionIfPresent(it.data.location)
            }
        })
    }

    override fun onMapReady(map: GoogleMap?) {
        this.map = map
        val matchLocation = viewModel.matchSnapshot?.data?.location
        val latLng = LatLng(
                matchLocation?.lng ?: DEFAULT_MARKER_LAT,
                matchLocation?.lng ?: DEFAULT_MARKER_LNG)
        val markerOptions = MarkerOptions()
                .position(latLng)
                .title(matchLocation?.name)
        marker = map?.addMarker(markerOptions)
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM))

        if (matchLocation != null) {
            updateMarkerAndMapPositionIfPresent(matchLocation)
        }
    }
}