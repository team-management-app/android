package com.tomaszstankowski.demcouch.main.web.login

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {

    @POST("/session")
    fun login(@Body body: LoginRequest): Single<LoginResponse>
}