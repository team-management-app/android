package com.tomaszstankowski.demcouch.main.features.createteam.steps.chooselocation

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Location
import com.tomaszstankowski.demcouch.main.features.createteam.steps.StepFragment
import com.tomaszstankowski.demcouch.main.repository.FetchStatus
import com.tomaszstankowski.demcouch.main.ui.toBundle
import com.tomaszstankowski.demcouch.main.ui.toLocation
import kotlinx.android.synthetic.main.fragment_choose_location.*
import timber.log.Timber


class ChooseLocationFragment : StepFragment() {

    private lateinit var viewModel: ChooseLocationViewModel

    companion object {
        fun getInstance(location: Location?): ChooseLocationFragment {
            val args = Bundle()
            args.putBundle(LOCATION_KEY, location?.toBundle())
            val fragment = ChooseLocationFragment()
            fragment.arguments = args
            return fragment
        }

        private const val LOCATION_KEY = "location"
        private const val PLACE_AUTOCOMPLETE_REQUEST_CODE = 987
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 456
    }

    val location: Location?
        get() = viewModel.locationSnapshot

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_choose_location, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = getViewModel()

        if (savedInstanceState == null) {
            val location = arguments?.getBundle(LOCATION_KEY)?.toLocation()
            if (location != null) {
                viewModel.onLocationFromAutocompleteSelected(location)
                setValid(true)
            }
        }

        fragmentChooseLocationFindLocationButton.setOnClickListener {
            try {
                val filter = AutocompleteFilter.Builder()
                        .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                        .build()
                val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                        .setFilter(filter)
                        .build(activity!!)
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
            } catch (e: Exception) {
                Timber.e(e, "Could not open places autocomplete")
            }
        }

        fragmentChooseLocationCurrentLocationButton.setOnClickListener {
            if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                viewModel.pickCurrentLocation()
            } else {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_PERMISSIONS_REQUEST_CODE)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.location, {
            when (it.status) {
                FetchStatus.LOADING -> {
                    fragmentChooseLocationSuccessTV.visibility = View.INVISIBLE
                    fragmentChooseLocationErrorTV.visibility = View.INVISIBLE
                    fragmentChooseLocationProgressBar.visibility = View.VISIBLE
                }
                FetchStatus.FAIL -> {
                    fragmentChooseLocationSuccessTV.visibility = View.INVISIBLE
                    fragmentChooseLocationErrorTV.visibility = View.VISIBLE
                    fragmentChooseLocationProgressBar.visibility = View.INVISIBLE
                }
                FetchStatus.SUCCESS -> {
                    fragmentChooseLocationSuccessTV.visibility = View.VISIBLE
                    fragmentChooseLocationErrorTV.visibility = View.INVISIBLE
                    fragmentChooseLocationProgressBar.visibility = View.INVISIBLE
                    fragmentChooseLocationSuccessTV.text = it.data?.name
                    setValid(true)
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PLACE_AUTOCOMPLETE_REQUEST_CODE -> {
                if (resultCode == RESULT_OK) {
                    val place = PlaceAutocomplete.getPlace(activity!!, data)
                    viewModel.onLocationFromAutocompleteSelected(place.toLocation())
                    setValid(true)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSIONS_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    viewModel.pickCurrentLocation()
                }
            }
        }
    }

}