package com.tomaszstankowski.demcouch.main.repository.teammembership

import com.tomaszstankowski.demcouch.main.cache.team.TeamDao
import com.tomaszstankowski.demcouch.main.cache.teammembership.TeamMembershipCache
import com.tomaszstankowski.demcouch.main.cache.user.UserDao
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.domain.TeamMembershipRole
import com.tomaszstankowski.demcouch.main.repository.team.TeamMapper
import com.tomaszstankowski.demcouch.main.repository.user.UserMapper
import com.tomaszstankowski.demcouch.main.web.teammembership.TeamMembershipResponse
import javax.inject.Inject

class TeamMembershipMapper @Inject constructor(private val teamMapper: TeamMapper,
                                               private val userMapper: UserMapper,
                                               private val teamDao: TeamDao,
                                               private val userDao: UserDao) {

    fun mapApiResponseToCache(apiResponse: TeamMembershipResponse): TeamMembershipCache {
        return apiResponse.run {
            TeamMembershipCache(id, startDate, confirmed, role.toString(), user.id, team.id)
        }
    }

    fun mapCacheToDomain(cache: TeamMembershipCache): TeamMembership {
        return cache.run {
            val userCache = userDao.findByIdBlocking(cache.userId)!!
            val teamCache = teamDao.findByIdBlocking(cache.teamId)!!
            TeamMembership(id, startDate, confirmed, TeamMembershipRole.valueOf(role),
                    userMapper.mapCacheToDomain(userCache),
                    teamMapper.mapCacheToDomain(teamCache))
        }
    }
}