package com.tomaszstankowski.demcouch.main.features.searchteams

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.Team
import com.tomaszstankowski.demcouch.main.features.teamdetails.TeamDetailsFragment
import com.tomaszstankowski.demcouch.main.ui.adapters.OnScrolledBottomListener
import com.tomaszstankowski.demcouch.main.ui.adapters.TeamPagedListAdapter
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import kotlinx.android.synthetic.main.fragment_search_teams.*
import timber.log.Timber

class SearchTeamsFragment : DemCouchFragment() {
    private lateinit var adapter: TeamPagedListAdapter
    private lateinit var viewModel: SearchTeamsViewModel

    private fun onItemClicked(item: Team) {
        val args = Bundle()
        args.putLong(TeamDetailsFragment.ID_KEY, item.id)
        Navigation.findNavController(activity!!, R.id.navHostFragment)
                .navigate(R.id.action_searchTeamsFragment_to_teamDetailsFragment, args)
    }

    private fun setUpRecyclerView() {
        adapter = TeamPagedListAdapter(context!!, viewModel.pageSize)
        adapter.onItemClickListener = { item, _ -> onItemClicked(item) }
        fragmentSearchTeamsRecyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(context)
        fragmentSearchTeamsRecyclerView.layoutManager = layoutManager
        fragmentSearchTeamsRecyclerView.addOnScrollListener(
                OnScrolledBottomListener(layoutManager) {
                    if (!viewModel.isLoading && !adapter.endReached) {
                        viewModel.loadNextPage()
                    }
                }
        )
    }

    companion object {
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 444
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_teams, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = getViewModel()

        setUpRecyclerView()

        fragmentSearchTeamsSearchView.addTextChangedListener(viewModel::onQueryChanged)

        fragmentSearchTeamsLocationSpinner.setOnItemSelectedListener { _, position, _, _ ->
            viewModel.onLocationSelected(position)
        }
        fragmentSearchTeamsDistanceSpinner.setOnItemSelectedListener { _, position, _, _ ->
            viewModel.onRadiusSelected(position)
        }
        fragmentSearchTeamsSearchView.clearFocus()
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.pageRequest, {
            try {
                adapter.submitPage(it)
            } catch (e: Exception) {
                Timber.e(e, "Error displaying page")
            }
        })
        subscribe(viewModel.locationEntries, {
            val items = it.map { entry ->
                when {
                    entry.location == null -> getString(R.string.search_teams_fragment_any_location)
                    entry.team == null -> "${entry.location.name} (${getString(R.string.search_teams_Fragment_current_location)})"
                    else -> "${entry.location.name} (${entry.team.name})"
                }
            }
            fragmentSearchTeamsLocationSpinner.setItems(items)
        })
        subscribe(viewModel.radiusEntries, {
            val items = it.map { radius ->
                getString(R.string.search_teams_fragment_radius_entry, radius)
            }
            fragmentSearchTeamsDistanceSpinner.setItems(items)
        })
        subscribe(viewModel.selectedLocationEntryIndex, {
            fragmentSearchTeamsLocationSpinner.selectedIndex = it
            fragmentSearchTeamsDistanceSpinner.isEnabled = it != 0
            fragmentSearchTeamsDistanceSpinner.visibility = if (it == 0) View.INVISIBLE else View.VISIBLE
            fragmentSearchTeamsRadiusHintTV.visibility = if (it == 0) View.INVISIBLE else View.VISIBLE
        })
        subscribe(viewModel.selectedRadiusEntryIndex, {
            fragmentSearchTeamsDistanceSpinner.selectedIndex = it
        })
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            viewModel.onLocationAccessPermissionGranted()
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSIONS_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    viewModel.onLocationAccessPermissionGranted()
                }
            }
        }
    }

}