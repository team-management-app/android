package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import io.reactivex.Single
import javax.inject.Inject

class PasswordValidator @Inject constructor() {

    fun validate(password: String): Single<FieldStatus> {
        return Single.create {
            when {
                password.length < 6 ->
                    it.onSuccess(FieldStatus.error(ValidationError.MIN_LENGTH, 6))
                password.length > 30 ->
                    it.onSuccess(FieldStatus.error(ValidationError.MAX_LENGTH, 30))
                else ->
                    it.onSuccess(FieldStatus.correct())
            }
        }
    }
}