package com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation

import com.tomaszstankowski.demcouch.main.misc.LocationService
import com.tomaszstankowski.demcouch.main.ui.Schedulers.Companion.backgroundThreadScheduler
import com.tomaszstankowski.demcouch.main.viewmodel.DemCouchViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class ChooseMatchLocationViewModel
@Inject constructor(private val locationService: LocationService) : DemCouchViewModel() {

    private val _location = BehaviorSubject.create<Coordinates>()
    val location: Flowable<Coordinates>
        get() = _location.toFlowable(BackpressureStrategy.LATEST)

    fun onGetCurrentLocationClicked() {
        val disposable = locationService.getCurrentLocation()
                .subscribeOn(backgroundThreadScheduler())
                .subscribe({
                    val coordinates = Coordinates(it.lat, it.lng)
                    _location.onNext(coordinates)
                }, {
                    Timber.e(it, "Failed getting current location")
                })
        compositeDisposable.add(disposable)
    }

    fun onLocationChangedByDraggingMarker(location: Coordinates) {
        if (location != _location.value) {
            _location.onNext(location)
        }
    }
}