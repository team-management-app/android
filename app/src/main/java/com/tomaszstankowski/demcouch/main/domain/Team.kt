package com.tomaszstankowski.demcouch.main.domain

data class Team(val id: Long,
                val name: String,
                val desc: String?,
                val location: Location,
                val discipline: Discipline,
                val thumbnailUrl: String?)