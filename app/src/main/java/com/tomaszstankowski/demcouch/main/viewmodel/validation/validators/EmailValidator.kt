package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import android.text.TextUtils
import android.util.Patterns
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import io.reactivex.Single
import javax.inject.Inject

class EmailValidator @Inject constructor() {

    fun validate(email: String): Single<FieldStatus> {
        return Single.create {
            if (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                it.onSuccess(FieldStatus.correct())
            } else {
                it.onSuccess(FieldStatus.error(ValidationError.PATTERN, "email"))
            }
        }
    }
}