package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import com.tomaszstankowski.demcouch.main.web.team.TeamService
import io.reactivex.Single
import javax.inject.Inject

class TeamNameValidator @Inject constructor(private val teamService: TeamService) {
    companion object {
        internal const val MIN_LENGTH = 6
        internal const val MAX_LENGTH = 30
    }

    fun validate(name: String, vararg validNames: String): Single<FieldStatus> {
        if (name in validNames) {
            return Single.just(FieldStatus.correct())
        }
        if (name.length < MIN_LENGTH) {
            return Single.just(FieldStatus.error(ValidationError.MIN_LENGTH, MIN_LENGTH))
        }
        if (name.length > MAX_LENGTH) {
            return Single.just(FieldStatus.error(ValidationError.MAX_LENGTH, MAX_LENGTH))
        }
        return teamService.getTeams(name = name)
                .map { page ->
                    if (page.data.isEmpty()) {
                        FieldStatus.correct()
                    } else {
                        FieldStatus.error(ValidationError.CONFLICT, name)
                    }
                }
    }
}