package com.tomaszstankowski.demcouch.main.repository

import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import timber.log.Timber
import java.util.concurrent.TimeUnit

fun <ResultType, ApiResponseType, CacheType> combineCacheWithApi(cacheSource: Flowable<CacheType>,
                                                                 apiSource: Single<ApiResponseType>,
                                                                 saveInCache: (ApiResponseType) -> Unit,
                                                                 mapCacheToDomain: (CacheType) -> ResultType,
                                                                 onNotFoundInApi: () -> Unit = { },
                                                                 shouldFetch: () -> Boolean = { true },
                                                                 onFetchCompleted: () -> Unit = { }): Flowable<Resource<ResultType>> {
    val fetchStatusSource = if (shouldFetch())
        apiSource
                .subscribeOn(Schedulers.newThread())
                .doOnSuccess(saveInCache)
                .doOnSuccess { onFetchCompleted() }
                .doOnError { Timber.e(it) }
                .doOnError { error ->
                    if (error is HttpException && error.code() == 404)
                        onNotFoundInApi()
                }
                .map { FetchStatus.SUCCESS }
                .onErrorReturnItem(FetchStatus.FAIL)
                .toFlowable()
                .startWith(FetchStatus.LOADING)
    else
        Flowable.just(FetchStatus.SUCCESS)

    return Flowable
            .combineLatest(
                    cacheSource,
                    fetchStatusSource,
                    BiFunction { cached: CacheType, status: FetchStatus ->
                        Resource(status, mapCacheToDomain(cached))
                    })
            .debounce(300, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .doOnError { Timber.e(it) }
            .onErrorReturnItem(Resource(FetchStatus.FAIL, null))
}