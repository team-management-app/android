package com.tomaszstankowski.demcouch.main.viewmodel.validation.validators

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import com.tomaszstankowski.demcouch.main.viewmodel.validation.FieldStatus
import com.tomaszstankowski.demcouch.main.viewmodel.validation.ValidationError
import io.reactivex.Single
import javax.inject.Inject


class ThumbnailValidator @Inject constructor(private val context: Context) {
    companion object {
        const val MAX_FILE_SIZE_IN_BYTES = 1024 * 1024
    }

    fun validate(uri: Uri): Single<FieldStatus> {
        val cursor = context.contentResolver
                .query(uri, null, null, null, null, null)
        cursor?.use {
            it.moveToFirst()
            val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
            if (!it.isNull(sizeIndex)) {
                val size: Int = cursor.getString(sizeIndex).toInt()
                if (size > MAX_FILE_SIZE_IN_BYTES) {
                    return Single.just(
                            FieldStatus.error(
                                    ValidationError.SIZE_EXCEEDED,
                                    MAX_FILE_SIZE_IN_BYTES / (1024 * 1024)))
                }
            }
        }

        return Single.just(FieldStatus.correct())
    }

}