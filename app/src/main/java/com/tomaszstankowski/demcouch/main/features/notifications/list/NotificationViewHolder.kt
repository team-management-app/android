package com.tomaszstankowski.demcouch.main.features.notifications.list

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.notification.Notification

abstract class NotificationViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    protected val thumbnail: ImageView = view.findViewById(R.id.notificationListItemThumbnail)
    protected val text: TextView = view.findViewById(R.id.notificationListItemText)

    open fun bind(item: Notification) {
        if (item.markedAsRead) {
            text.typeface = Typeface.create(text.typeface, Typeface.NORMAL)
        } else {
            text.typeface = Typeface.create(text.typeface, Typeface.BOLD)
        }
    }
}