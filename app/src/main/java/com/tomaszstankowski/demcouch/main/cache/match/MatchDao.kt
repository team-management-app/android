package com.tomaszstankowski.demcouch.main.cache.match

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface MatchDao {

    @Query("SELECT * FROM `match` WHERE guest_team_id IN (:teamsIds) OR host_team_id IN (:teamsIds) ORDER BY date DESC")
    fun findForTeams(teamsIds: List<Long>): Flowable<List<MatchCache>>

    @Query("SELECT * FROM `match` WHERE id = :id")
    fun findById(id: Long): Flowable<MatchCache>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(matches: List<MatchCache>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(match: MatchCache)
}