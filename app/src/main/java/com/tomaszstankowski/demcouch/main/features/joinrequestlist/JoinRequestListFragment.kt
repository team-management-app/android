package com.tomaszstankowski.demcouch.main.features.joinrequestlist

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.tomaszstankowski.demcouch.R
import com.tomaszstankowski.demcouch.main.domain.TeamMembership
import com.tomaszstankowski.demcouch.main.features.profile.ProfileFragment
import com.tomaszstankowski.demcouch.main.ui.fragments.DemCouchFragment
import com.tomaszstankowski.demcouch.main.ui.setVisibleOrElseGone
import kotlinx.android.synthetic.main.fragment_join_request_list.*
import javax.inject.Inject

class JoinRequestListFragment : DemCouchFragment(),
        JoinRequestListAdapter.OnMembershipItemClickListener {

    @Inject
    lateinit var viewModel: JoinRequestListViewModel

    private lateinit var adapter: JoinRequestListAdapter

    companion object {
        const val TEAM_ID_KEY = "teamId"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_join_request_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val teamId = arguments?.getLong(TEAM_ID_KEY)
                ?: throw IllegalStateException("No '$TEAM_ID_KEY' param passed to fragment")
        viewModel = getViewModel()
        viewModel.init(teamId)

        adapter = JoinRequestListAdapter(activity!!)
        adapter.onMembershipItemClickListener = this
        fragmentJoinRequestListRecyclerView.adapter = adapter
        fragmentJoinRequestListRecyclerView.layoutManager = LinearLayoutManager(activity!!)
    }

    override fun onStart() {
        super.onStart()
        subscribe(viewModel.requestedMemberships, adapter::items::set)
        subscribe(viewModel.membershipConfirmation.loading,
                fragmentJoinRequestListProgressBar::setVisibleOrElseGone)
        subscribe(viewModel.membershipRejection.loading,
                fragmentJoinRequestListProgressBar::setVisibleOrElseGone)
        subscribe(viewModel.membershipConfirmation.failure, { showMessage(R.string.error) })
        subscribe(viewModel.membershipRejection.failure, { showMessage(R.string.error) })
    }

    override fun onMembershipClicked(membership: TeamMembership) {
        val args = Bundle()
        args.putLong(ProfileFragment.ID_KEY, membership.user.id)
        Navigation.findNavController(activity!!, R.id.navHostFragment)
                .navigate(R.id.action_joinRequestListFragment_to_profileFragment, args)
    }

    override fun onMembershipConfirmClicked(membership: TeamMembership) {
        viewModel.onConfirmMembershipClicked(membership.id)
    }

    override fun onMembershipRejectClicked(membership: TeamMembership) {
        viewModel.onRejectMembershipClicked(membership.id)
    }
}