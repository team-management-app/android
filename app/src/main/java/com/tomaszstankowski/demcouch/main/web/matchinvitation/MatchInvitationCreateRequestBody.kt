package com.tomaszstankowski.demcouch.main.web.matchinvitation

import com.squareup.moshi.Json
import com.tomaszstankowski.demcouch.main.features.invite.steps.choosematchlocation.Coordinates
import org.threeten.bp.ZonedDateTime

data class MatchInvitationCreateRequestBody(@Json(name = "inviting_team_id") val invitingTeamId: Long,
                                            @Json(name = "invited_team_id") val invitedTeamId: Long,
                                            val location: Coordinates,
                                            val date: ZonedDateTime)