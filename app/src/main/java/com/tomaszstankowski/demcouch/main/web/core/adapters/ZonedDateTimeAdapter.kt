package com.tomaszstankowski.demcouch.main.web.core.adapters

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter

class ZonedDateTimeAdapter {
    private val inFormat = DateTimeFormatter.ISO_DATE_TIME
    private val outFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX")

    @ToJson
    fun toJson(dateTime: ZonedDateTime): String =
            dateTime.format(outFormat)

    @FromJson
    @Synchronized
    fun fromJson(json: String): ZonedDateTime = ZonedDateTime.parse(json, inFormat)
}